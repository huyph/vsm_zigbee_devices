/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_buttons.c
 *
 * DESCRIPTION:        Button events
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "drv_buttons.h"
#include "my_define.h"
#include "hw_isr.h"
#include "Ztimer.h"
#include "AppHardwareApi_JN516x.h"
#include "dbg.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_DRV_BUTTON
    #define TRACE_DRV_BUTTON            FALSE
#else
    #define TRACE_DRV_BUTTON            TRUE
#endif

/* Defines the number of bits sampled for a button debounce event. One sample taken for each bit set */
#define APP_BUTTON_SAMPLE_MASK          (0x1f)
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef struct
{
	uint8_t u8ButtonIndex;
	uint8_t u8TimerIndex;
	uint8_t	u8HoldTime;		//second
}DRV_BUTTONS_tsHoldParams;
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void DRV_BUTTONS_vIsr(void* pvParams);

PRIVATE void vCallbackTimerButtonScan(void* pvParams);

PRIVATE void vCallbackTimerHoldButton(void* pvParams);

PRIVATE void vProcessButtonUp(uint8_t u8ButtonNumber);
PRIVATE void vProcessButtonDown(uint8_t	u8ButtonNumber);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE uint8_t		u8ArrPinButton[NUMBER_OF_BUTTONS] = {
		PIN_IN_FUNC_BUTTON,
#if (defined TEST_SCENE_SWITCH) | (defined TEST_NORMAL_SWITCH)
		PIN_IN_BUTTON_TEST_1,
		PIN_IN_BUTTON_TEST_2,
		PIN_IN_BUTTON_TEST_3,
		PIN_IN_BUTTON_TEST_4
#endif
};
PRIVATE uint32_t	u32ButtonsMask = 0UL;
PRIVATE DRV_BUTTONS_tsHoldParams sHoldParams[NUMBER_OF_BUTTONS];
PRIVATE drvButtonCb			s_fnButtonCallback = NULL;

PRIVATE uint32_t	u32DioInterrupts = 0UL;
PRIVATE uint32 		u32PreviousDioState;
PRIVATE uint8 		s_u8ButtonDebounce[NUMBER_OF_BUTTONS] = { 0xff };
PRIVATE uint32 		s_u32ButtonDIOLine[NUMBER_OF_BUTTONS];
PRIVATE uint8_t		u8TimerDebounceButton;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC bool_t DRV_BUTTONS_bInitialize(
		drvButtonCb		fptr
)
{
	uint8_t i;
	for(i = 0; i < NUMBER_OF_BUTTONS; i++)
	{
		u32ButtonsMask 			|= 1UL << u8ArrPinButton[i];
		s_u32ButtonDIOLine[i] 	= 1UL << u8ArrPinButton[i];
	}

	u32PreviousDioState = u32ButtonsMask;
	/* Set DIO lines to inputs with buttons connected */
	vAHI_DioSetDirection(u32ButtonsMask, 0);

	/* Turn on pull-ups for DIO lines with buttons connected */
	vAHI_DioSetPullup(u32ButtonsMask, 0);

	/* Set the edge detection for falling edges */
	vAHI_DioInterruptEdge(0, u32ButtonsMask);

	/* Enable interrupts to occur on selected edge */
	vAHI_DioInterruptEnable(u32ButtonsMask, 0);

	uint32 u32Buttons = u32AHI_DioReadInput() & u32ButtonsMask;
	//
	uint8_t 	u8ButtonIsr;
	HW_ISR_vRegisterCallback(&u8ButtonIsr,
			u32ButtonsMask,
			DRV_BUTTONS_vIsr,
			NULL);
	//
	(void) ZTIMER_eOpen(&u8TimerDebounceButton,
					vCallbackTimerButtonScan, NULL,
					ZTIMER_FLAG_PREVENT_SLEEP);
	// Create timer button hold
	for(i = 0; i < NUMBER_OF_BUTTONS; i++)
	{
		sHoldParams[i].u8ButtonIndex = i;
		(void) ZTIMER_eOpen(&sHoldParams[i].u8TimerIndex,
				vCallbackTimerHoldButton, &sHoldParams[i].u8ButtonIndex,
				ZTIMER_FLAG_PREVENT_SLEEP);
	}

	/*Register callback function*/
	if (NULL != fptr)
	{
		s_fnButtonCallback = fptr;
	}

	/* If we came out of deep sleep; perform appropriate action as well based
	       on button press.*/
	vCallbackTimerButtonScan(NULL);

	if (u32Buttons != u32ButtonsMask)
	{
		return TRUE;
	}
	return FALSE;
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void DRV_BUTTONS_vIsr(void* pvParams)
{
    /* Disable edge detection until scan complete */
    vAHI_DioInterruptEnable(0, u32ButtonsMask);
    /* Begin debouncing the button press */
    ZTIMER_eStop(u8TimerDebounceButton);
    ZTIMER_eStart(u8TimerDebounceButton, 2);
}

PRIVATE void vCallbackTimerButtonScan(void* pvParams)
{
	uint8 u8Button;
	uint32 u32DioInput = 0;
	bool_t bButtonDebounceComplete = TRUE;

	/* Clear any existing interrupt pending flags */
	(void)u32AHI_DioInterruptStatus();

	if(u32DioInterrupts != 0)
	{
		u32DioInput = (u32PreviousDioState ^ u32DioInterrupts) & u32ButtonsMask;
	}
	else
	{
		u32DioInput = u32AHI_DioReadInput();
	}

	DBG_vPrintf(TRACE_DRV_BUTTON,
			"\nAPP Button: APP_ButtonsScanTask, Buttons = %08x, Interrupts = %08x, Previous = %08x",
			u32ButtonsMask & u32DioInput, (u32DioInterrupts & u32ButtonsMask),
			u32PreviousDioState);

	u32DioInterrupts = 0;

	/* Loop over all buttons to check their Dio states*/
	for (u8Button = 0; u8Button < NUMBER_OF_BUTTONS; u8Button++)
	{

		/* Shift the previous debounce checks and add the new debounce reading*/
		s_u8ButtonDebounce[u8Button] <<= 1;
		s_u8ButtonDebounce[u8Button] |= (u32DioInput & s_u32ButtonDIOLine[u8Button]) ? TRUE : FALSE;
		s_u8ButtonDebounce[u8Button] &= APP_BUTTON_SAMPLE_MASK;

		DBG_vPrintf(TRACE_DRV_BUTTON, "\nAPP Button: Button %d, Debounce = %02x, Dio State = %08x", u8Button, s_u8ButtonDebounce[u8Button], u32PreviousDioState);

		/* If previously the button was down but now it is up, post an event to the queue */
		if (((u32PreviousDioState & s_u32ButtonDIOLine[u8Button]) == 0) && (s_u8ButtonDebounce[u8Button] == APP_BUTTON_SAMPLE_MASK))
		{
			/* Save the new state */
			u32PreviousDioState |= s_u32ButtonDIOLine[u8Button];
			DBG_vPrintf(TRACE_DRV_BUTTON, "\nAPP Button: Button UP=%d, Dio State = %08x", u8Button, u32PreviousDioState);
			vProcessButtonUp(u8Button);

		}
		/* If previously the button was up but now it is down, post an event to the queue */
		else if (((u32PreviousDioState & s_u32ButtonDIOLine[u8Button]) != 0) && (s_u8ButtonDebounce[u8Button] == 0x0))
		{
			/* Save the new state */
			u32PreviousDioState &= ~s_u32ButtonDIOLine[u8Button];
			DBG_vPrintf(TRACE_DRV_BUTTON, "\nAPP Button: Button DN=%d, Dio State = %08x", u8Button, u32PreviousDioState);
			vProcessButtonDown(u8Button);
		}

		/* Still debouncing this button, clear flag to indicate more samples are required */
		else if(((s_u8ButtonDebounce[u8Button] != 0) && (s_u8ButtonDebounce[u8Button] != APP_BUTTON_SAMPLE_MASK)))
		{
			bButtonDebounceComplete &= FALSE;
		}

	}

	/* If all buttons are in a stable state, stop the scan timer and set the new interrupt edge requirements */
	if(bButtonDebounceComplete == TRUE)
	{
		/* Set the new interrupt edge requirements */
		vAHI_DioWakeEdge((u32ButtonsMask & ~u32PreviousDioState), (u32ButtonsMask & u32PreviousDioState));

		/* Re enable DIO wake interrupts on all buttons */
		vAHI_DioInterruptEnable(u32ButtonsMask, 0);

		DBG_vPrintf(TRACE_DRV_BUTTON, "\nAPP Button: Debounce complete, timer stopped, interrupts re-enabled, previous state %08x", u32PreviousDioState);
		DBG_vPrintf(TRACE_DRV_BUTTON, "\nAPP Button: Wake edges: Rising=%08x Falling=%08x", (u32ButtonsMask & ~u32PreviousDioState), (u32ButtonsMask & u32PreviousDioState));

	}
	else
	{
		DBG_vPrintf(TRACE_DRV_BUTTON, "\nAPP Button: Debounce in progress, timer continued");
		ZTIMER_eStart(u8TimerDebounceButton, 2);
	}
}

PRIVATE void vCallbackTimerHoldButton(void* pvParams)
{
	if (NULL == pvParams) return;

	uint8_t u8ButtonNumber = *((uint8_t*)pvParams);
	(void) ZTIMER_eStop(sHoldParams[u8ButtonNumber].u8TimerIndex);

	sHoldParams[u8ButtonNumber].u8HoldTime++;

	DRV_BUTTONS_tsEvent_t sEvent;
	sEvent.eEventType 	= DRV_E_BUTTON_EVENT_HOLD;
	sEvent.u8Button		= u8ArrPinButton[u8ButtonNumber];
	sEvent.u8TimeHold	= sHoldParams[u8ButtonNumber].u8HoldTime;
	if(s_fnButtonCallback != NULL)
	{
		s_fnButtonCallback(sEvent);
	}
	DBG_vPrintf(TRACE_DRV_BUTTON, "DRV_BUTTONS: Button[%d] hold [%d] second(s) \n", sEvent.u8Button, sEvent.u8TimeHold);
	(void) ZTIMER_eStart(sHoldParams[u8ButtonNumber].u8TimerIndex, ZTIMER_TIME_SEC(1));
}

PRIVATE void vProcessButtonUp(uint8_t u8ButtonNumber)
{
	DBG_vPrintf(TRACE_DRV_BUTTON, "DRV_BUTTON: Button[%d] Up \n", u8ArrPinButton[u8ButtonNumber]);

	(void) ZTIMER_eStop(sHoldParams[u8ButtonNumber].u8TimerIndex);

	DRV_BUTTONS_tsEvent_t sEvent;
	sEvent.eEventType 	= DRV_E_BUTTON_EVENT_UP;
	sEvent.u8Button		= u8ArrPinButton[u8ButtonNumber];
	sEvent.u8TimeHold	= sHoldParams[u8ButtonNumber].u8HoldTime;

	if(s_fnButtonCallback != NULL)
	{
		s_fnButtonCallback(sEvent);
	}

	sHoldParams[u8ButtonNumber].u8HoldTime = 0;
}

PRIVATE void vProcessButtonDown(uint8_t	u8ButtonNumber)
{
	DBG_vPrintf(TRACE_DRV_BUTTON, "DRV_BUTTON: Button[%d] Down \n", u8ArrPinButton[u8ButtonNumber]);

	(void) ZTIMER_eStop(sHoldParams[u8ButtonNumber].u8TimerIndex);

	DRV_BUTTONS_tsEvent_t sEvent;
	sEvent.eEventType 	= DRV_E_BUTTON_EVENT_DOWN;
	sEvent.u8Button		= u8ArrPinButton[u8ButtonNumber];
	sEvent.u8TimeHold	= 0;
	if(s_fnButtonCallback != NULL)
	{
		s_fnButtonCallback(sEvent);
	}

	sHoldParams[u8ButtonNumber].u8HoldTime = 0;
	(void) ZTIMER_eStart(sHoldParams[u8ButtonNumber].u8TimerIndex, ZTIMER_TIME_SEC(1));
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/

