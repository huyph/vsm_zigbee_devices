/*****************************************************************************
 *
 * MODULE:             JN-AN-1220 ZLO Sensor Demo
 *
 * COMPONENT:          app_device_config.c
 *
 * DESCRIPTION:        DK4 (DR1175) Button Press detection (Implementation)
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5168, JN5179].
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2017. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include <jendefs.h>
#include "dbg.h"
#include "PDM_IDs.h"
#include "app_device_config.h"
//#include "app_sensor_node.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_APP_DEVICE_CONFIG
    #define TRACE_APP_DEVICE_CONFIG               FALSE
#else
    #define TRACE_APP_DEVICE_CONFIG               TRUE
#endif

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE tsDeviceConfig sDeviceConfig;

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
/****************************************************************************
 *
 * NAME: bDeviceConfig_UpdateConfig
 *
 * DESCRIPTION:
 * Update device config when initialization or receiving new config
 *
 * PARAMETER:
 *
 * RETURNS:
 *
 ****************************************************************************/
PUBLIC BOOL_T bDeviceConfig_UpdateConfig(uint8 u8ParamId,
										 void* pParam)
{
	uint8 u8UpdateMask = 0;

	/* Store new config to flash */
	switch (u8ParamId)
	{
#ifdef DEVICE_CONFIG_LED_INTENSITY_BIT
		case DEVICE_CONFIG_LED_INTENSITY_BIT:
			sDeviceConfig.sDeviceConfigParams.u8LedIntensity = *((uint8*) pParam);
			u8UpdateMask = DEVICE_CONFIG_LED_INTENSITY_BIT;
			break;
#endif

#ifdef DEVICE_CONFIG_VIBRATION_INTENSITY_BIT
		case DEVICE_CONFIG_VIBRATION_INTENSITY_BIT:
			sDeviceConfig.sDeviceConfigParams.u8VibrationIntensity = *((uint8*) pParam);
			u8UpdateMask = DEVICE_CONFIG_VIBRATION_INTENSITY_BIT;
		break;
#endif

		default:
			return FALSE;
	}

	PDM_eSaveRecordData(PDM_ID_DEVICE_CONFIG,
						&sDeviceConfig.sDeviceConfigParams,
						sizeof(sDeviceConfig.sDeviceConfigParams));


	/* Call callback function to apply config */
	if (sDeviceConfig.pCallbackFunction == NULL)
		return FALSE;

	sDeviceConfig.pCallbackFunction(u8UpdateMask, &sDeviceConfig.sDeviceConfigParams);

	return TRUE;
}

/****************************************************************************
 *
 * NAME: vDeviceConfig_UpdateConfig
 *
 * DESCRIPTION:
 * An Sensor specific Debounce task. In the real world this maybe
 * removed as the digital input will be driven from a sensor.
 *
 * PARAMETER:
 *
 * RETURNS:
 *
 ****************************************************************************/
PUBLIC BOOL_T bDeviceConfig_LoadConfig(tsCLD_Basic* psBasicServerCluster)
{
    uint16 u16BytesRead = 0;
    uint8  u8UpdateMask = 0;

    if (psBasicServerCluster == NULL)
    	return FALSE;

    /* Load device config from memory */
	PDM_eReadDataFromRecord(PDM_ID_DEVICE_CONFIG,
							&sDeviceConfig.sDeviceConfigParams,
							sizeof(sDeviceConfig.sDeviceConfigParams),
							&u16BytesRead);

	/* Update value to basic cluster */
#ifdef CLD_BAS_ATTR_MAN_SPEC_LED_INTENSITY
	psBasicServerCluster->u8LedIntensity = sDeviceConfig.sDeviceConfigParams.u8LedIntensity;
	u8UpdateMask |= DEVICE_CONFIG_LED_INTENSITY_BIT;

	DBG_vPrintf(TRUE, "\nLED Intensity: %d", psBasicServerCluster->u8LedIntensity);
#endif

#ifdef CLD_BAS_ATTR_MAN_SPEC_VIBRATION_INTENSITY
	psBasicServerCluster->u8VibrationIntensity     = sDeviceConfig.sDeviceConfigParams.u8VibrationIntensity;
	u8UpdateMask |= DEVICE_CONFIG_VIBRATION_INTENSITY_BIT;

	DBG_vPrintf(TRUE, "\nVibration Intensity: %d", psBasicServerCluster->u8VibrationIntensity);
#endif

	/* Call callback function to apply config */
	if (sDeviceConfig.pCallbackFunction == NULL)
		return FALSE;

	sDeviceConfig.pCallbackFunction(u8UpdateMask, &sDeviceConfig.sDeviceConfigParams);

	return TRUE;
}

/****************************************************************************
 *
 * NAME: bDeviceConfig_RegisterCallback
 *
 * DESCRIPTION:
 * Register callback for device config purpose
 *
 * PARAMETER:
 *
 * RETURNS:
 *
 ****************************************************************************/
PUBLIC BOOL_T bDeviceConfig_RegisterCallback(tfpDeviceConfigCb pfCallBack)
{
	if (pfCallBack == NULL)
		return FALSE;

	sDeviceConfig.pCallbackFunction = pfCallBack;
	return TRUE;
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
