/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_led.c
 *
 * DESCRIPTION:        Base Device application - reporting functionality
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "app_led.h"
#include "dbg.h"
#include "my_define.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_APP_LED
    #define TRACE_APP_LED              (TRUE)
#else   /* DEBUG_APP_LED */
    #define TRACE_APP_LED              (FALSE)
#endif  /* DEBUG_APP_LED */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
#define LED_STATUS_MASK		(1UL << PIN_OUT_LED_STATUS)
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_LED_vInitialize(void)
{
	if(!DRV_LEDS_bInitialize(LED_STATUS_MASK, 100))
	{
		DBG_vPrintf(TRACE_APP_LED,"APP-LED: Initialize failed! \n");
	}

	APP_LED_vOff();
}

PUBLIC void APP_LED_vSet(bool_t bOnOff)
{
	if(bOnOff)
	{
		APP_LED_vOn(-1);
	}
	else
	{
		APP_LED_vOff();
	}
}

PUBLIC void APP_LED_vOn(
		int16_t	i16TimeOn
)
{
	if(i16TimeOn < 0)
	{
		DRV_LEDS_vOn(LED_STATUS_MASK);
	}
	else
	{
		DRV_LEDS_vBlinkLedsAlternately(LED_STATUS_MASK, 0UL, 1,
				-1, i16TimeOn, i16TimeOn,
				NULL, NULL);
	}
}

PUBLIC void APP_LED_vOff(void)
{
	DRV_LEDS_vOff(LED_STATUS_MASK);
}

PUBLIC void APP_LED_vStartBlink(
		int16_t 				i16BlinkDuration,
		uint16_t				u16Cycle,
		fnCbAfterBlink			fnCallback,
		void                	*pParams
)
{
	DRV_LEDS_vBlinkLedsAlternately(LED_STATUS_MASK, 0UL, -1,
			i16BlinkDuration, u16Cycle / 2, u16Cycle / 2,
			fnCallback, pParams);
}
PUBLIC void APP_LED_vStopBlink(void)
{
	DRV_LEDS_vStopBlinkLeds(TRUE, 0);
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/

