/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_light_sensor.c
 *
 * DESCRIPTION:
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "pdum_gen.h"
#include "zps_gen.h"
#include "zcl_options.h"
#include "app_light_sensor.h"
#include "drv_light_sensor.h"
#include "dbg.h"
#include "app_reporting.h"
#include "app_main.h"
#include "app_events.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_APP_LIGHT_SENSOR
	#define TRACE_APP_LIGHT_SENSOR                     (FALSE)
#else /* DEBUG_APP_LIGHT_SENSOR */
	#define TRACE_APP_LIGHT_SENSOR                     (TRUE)
#endif /* DEBUG_APP_LIGHT_SENSOR */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vCallbackDrvLightSensor(uint16_t u16LightLevel);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
PUBLIC tsZLO_LightSensorDevice s_LightSensor;
extern PUBLIC tszQueue APP_msgAppEvents;
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_LIGHT_SENSOR_vInitialize(void)
{
	DRV_LIGHT_SENSOR_vInitialise(vCallbackDrvLightSensor);
}

PUBLIC teZCL_Status APP_LIGHT_SENSOR_eZCLRegisterEndpoint(
    tfpZCL_ZCLCallBackFunction fptr
)
{
    return eZLO_RegisterLightSensorEndPoint(
                                PIR_LIGHTSENSOR_ENDPOINT,
                                fptr,
                                &s_LightSensor);

}

PUBLIC void APP_LIGHT_SENSOR_vZCLDeviceSpecificInit(void)
{
    /* Init Light Sensor Endpoint */
    memcpy(s_LightSensor.sBasicServerCluster.au8ManufacturerName, BAS_MANUF_NAME_STRING, CLD_BAS_MANUF_NAME_SIZE);
    memcpy(s_LightSensor.sBasicServerCluster.au8ModelIdentifier,  BAS_MODEL_ID_STRING,   CLD_BAS_MODEL_ID_SIZE);
    memcpy(s_LightSensor.sBasicServerCluster.au8DateCode,         BAS_DATE_STRING,       CLD_BAS_DATE_SIZE);
    memcpy(s_LightSensor.sBasicServerCluster.au8SWBuildID,        BAS_SW_BUILD_STRING,   CLD_BAS_SW_BUILD_SIZE);
    s_LightSensor.sBasicServerCluster.eGenericDeviceType = E_CLD_BAS_GENERIC_DEVICE_TYPE_MOTION_OR_LIGHT_SENSOR;

    /* Initialise the attribute in illuminance Measurement */
    s_LightSensor.sIlluminanceMeasurementServerCluster.u16MeasuredValue = 0;
    s_LightSensor.sIlluminanceMeasurementServerCluster.eLightSensorType = E_CLD_ILLMEAS_LST_CMOS;
    s_LightSensor.sIlluminanceMeasurementServerCluster.u16MinMeasuredValue = LIGHT_SENSOR_MINIMUM_MEASURED_VALUE;
    s_LightSensor.sIlluminanceMeasurementServerCluster.u16MaxMeasuredValue = LIGHT_SENSOR_MAXIMUM_MEASURED_VALUE;
}

PUBLIC void APP_LIGHT_SENSOR_vReportState(void)
{
	APP_REPORT_eReportAttribute(PIR_LIGHTSENSOR_ENDPOINT,
			MEASUREMENT_AND_SENSING_CLUSTER_ID_ILLUMINANCE_MEASUREMENT,
			E_CLD_ILLMEAS_ATTR_ID_MEASURED_VALUE);
}

PUBLIC bool_t APP_LIGHT_SENSOR_bIsDarkState(void)
{
	return (E_LIGHT_LEVEL_DARK == DRV_LIGHT_SENSOR_eGetState());
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vCallbackDrvLightSensor(uint16_t u16LightLevel)
{
	DBG_vPrintf(TRACE_APP_LIGHT_SENSOR, "APP_LIGHT_SENSOR: Light level %d \n", u16LightLevel);
	s_LightSensor.sIlluminanceMeasurementServerCluster.u16MeasuredValue = u16LightLevel;
	// Post message to main program to report
	APP_tsEvent sAppEvent;
	sAppEvent.eType = APP_E_EVENT_LIGHT_SENSOR_REPORT_STATUS;
	if(ZQ_bQueueSend(&APP_msgAppEvents, &sAppEvent) == FALSE)
	{
		DBG_vPrintf(TRACE_APP_LIGHT_SENSOR, "APP_LIGHT_SENSOR: Failed to post Event %d \n", sAppEvent.eType);
	}
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
