/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          function_button.c
 *
 * DESCRIPTION:        Function button using ADC
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "function_button.h"
#include "AppHardwareApi_JN516x.h"
#include "dbg.h"
#include "ZTimer.h"
#include "app_events.h"
#include "ZQueue.h"
#include "my_define.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_FUNCTION_BUTTON
    #define TRACE_FUNCTION_BUTTON          (TRUE)
#else   /* DEBUG_FUNCTION_BUTTON */
    #define TRACE_FUNCTION_BUTTON          (FALSE)
#endif  /* DEBUG_FUNCTION_BUTTON */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vScanFunctionButton(void *pvParam);
PRIVATE void vCallbackTimerButtonHold(void *pvParam);

PRIVATE bool_t bGetFuncButtonState(void);
PRIVATE void vProcessButtonPress(void);
PRIVATE void vProcessButtonRelease(void);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
extern PUBLIC tszQueue APP_msgAppEvents;
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE uint8_t		u8TimerScanFuncButton;
PRIVATE uint8_t		u8TimerButtonHold;
PRIVATE uint8_t 	u8TimeButtonHold_Sec;
PRIVATE bool_t		bPreButtonState = FALSE;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void FUNC_BUTTON_vInitialize(void)
{
	// Setting ADC for button
	if (!bAHI_APRegulatorEnabled()){

		vAHI_ApConfigure(E_AHI_AP_REGULATOR_ENABLE,
				E_AHI_AP_INT_DISABLE,       // Disable interrupt when conversion
				E_AHI_AP_SAMPLE_2,
				E_AHI_AP_CLOCKDIV_500KHZ, // clock
				E_AHI_AP_EXTREF);          // Vref = Internal = 1.235VDC (Refer JN5169 Datasheet page 67)

		while(!bAHI_APRegulatorEnabled());
	}

	vAHI_AdcEnable( E_AHI_ADC_SINGLE_SHOT,  // Perform a single conversion and then stop
			E_AHI_AP_INPUT_RANGE_1, // range 0-Vref = 0-> 2.47VDC
			ADC_FUNC_BUTTON);   // ADC1 or PIN15

	bPreButtonState = bGetFuncButtonState();

	// Open timer count hold time of button
	(void) ZTIMER_eOpen(&u8TimerButtonHold,   vCallbackTimerButtonHold, NULL, ZTIMER_FLAG_PREVENT_SLEEP);
	// Start timer to reading ADC frequency
	(void) ZTIMER_eOpen(&u8TimerScanFuncButton,   vScanFunctionButton, NULL, ZTIMER_FLAG_PREVENT_SLEEP);
	ZTIMER_eStart(u8TimerScanFuncButton, FUNC_BUTTON_ADC_SCAN_CYCLE);
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vScanFunctionButton(void *pvParam)
{
	ZTIMER_eStop(u8TimerScanFuncButton);
	// check button state
	bool_t bCurButtonStatus = bGetFuncButtonState();
	if(!bCurButtonStatus && bPreButtonState) // Button press (falling edge)
	{
		DBG_vPrintf(TRACE_FUNCTION_BUTTON, "FUNC_BUTTON: Down \n");
		bPreButtonState = bCurButtonStatus;
		vProcessButtonPress();
	}
	else if(bCurButtonStatus && !bPreButtonState) // Button release (rising edge)
	{
		DBG_vPrintf(TRACE_FUNCTION_BUTTON, "FUNC_BUTTON: Up \n");
		bPreButtonState = bCurButtonStatus;
		vProcessButtonRelease();
	}
	// Start timer again
	ZTIMER_eStart(u8TimerScanFuncButton, FUNC_BUTTON_ADC_SCAN_CYCLE);
}

PRIVATE bool_t bGetFuncButtonState(void)
{
	// reading ADC value
	vAHI_AdcStartSample();
	while(bAHI_AdcPoll());             // Poll for read completion
	uint16_t u16AdcReading = u16AHI_AdcRead();

	if(u16AdcReading < FUNC_BUTTON_ADC_THRESHOLD)
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

PRIVATE void vProcessButtonPress(void)
{
	u8TimeButtonHold_Sec = 0;
	// start timer count hold time of button
	ZTIMER_eStart(u8TimerButtonHold, 1000);
}

PRIVATE void vProcessButtonRelease(void)
{
	// stop timer count hold time of button
	ZTIMER_eStop(u8TimerButtonHold);

	if(u8TimeButtonHold_Sec < 5)
	{
		APP_tsEvent sAppEvent;
		sAppEvent.eType = APP_E_EVENT_FUNC_BUTTON_UP_SHORTER_5S;
		if(ZQ_bQueueSend(&APP_msgAppEvents, &sAppEvent) == FALSE)
		{
			DBG_vPrintf(TRACE_FUNCTION_BUTTON, "FUNC_BUTTON: Function button Failed to post Event %d \n", sAppEvent.eType);
		}
	}
}

PRIVATE void vCallbackTimerButtonHold(void *pvParam)
{
	ZTIMER_eStop(u8TimerButtonHold);

	if(u8TimeButtonHold_Sec < 10)
	{
		u8TimeButtonHold_Sec ++;
		DBG_vPrintf(TRACE_FUNCTION_BUTTON, "FUNC_BUTTON: button hold = %d s \n", u8TimeButtonHold_Sec);
		if(u8TimeButtonHold_Sec == 5)
		{
			APP_tsEvent sAppEvent;
			sAppEvent.eType = APP_E_EVENT_FUNC_BUTTON_HOLD_5S;
			if(ZQ_bQueueSend(&APP_msgAppEvents, &sAppEvent) == FALSE)
			{
				DBG_vPrintf(TRACE_FUNCTION_BUTTON, "FUNC_BUTTON: Function button Failed to post Event %d \n", sAppEvent.eType);
			}
		}
		ZTIMER_eStart(u8TimerButtonHold, 1000);
	}
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
