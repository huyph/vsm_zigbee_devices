/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          hw_timer.c
 *
 * DESCRIPTION:        Manage hardware and software timers
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "hw_timer.h"
#include "dbg.h"
#include "math.h"
#include "app_common.h"
#include "my_define.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_HW_TIMER
    #define TRACE_HW_TIMER              (TRUE)
#else   /* DEBUG_HW_TIMER */
    #define TRACE_HW_TIMER              (FALSE)
#endif  /* DEBUG_HW_TIMER */

#define SOURCE_CLOCK_FREQ						(16000000)	//16Mhz


/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
/* common */
typedef enum
{
	E_TIMER_STATE_FREE,
	E_TIMER_STATE_OCCUPIED_PERMIT_OVERRIDE,
	E_TIMER_STATE_OCCUPIED_NOT_PERMIT_OVERRIDE,
}HW_TIMER_teTimerStates_t;

typedef struct
{
	uint8 	u8Prescale;
	bool_t 	bIntRiseEnable;
	bool_t	bIntPeriodEnable;
	bool_t	bOutputEnable;

	bool_t 	bLocation;
	bool_t 	bLocationOverridePWM3andPWM2;

	bool_t	bDIOEnable;

}HW_TIMER_tsConfig_t;
typedef struct
{
	HW_TIMER_teTimerStates_t		eState;

	uint32_t						u32ClokFreq;
	bool_t 							bIntRiseEnable;
	bool_t 							bIntPeriodEnable;
	fbCallbackTimerInt				fnCallback;
	void*							pvParams;

	bool_t							bEnableDefaultOutput;
	bool_t 							bEnableUserOutput;
	uint32_t						u32TimerOutputMask;

}HW_TIMER_tsParams_t;
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vHWTimer_ProcessInterrupt(
		HW_TIMER_teTimerNumber_t	eHWTimer
);
PRIVATE void vHWTimer_Enable(
		HW_TIMER_teTimerNumber_t	eHWTimer
);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE HW_TIMER_tsParams_t	sHWTimerParams[NUMBER_HW_TIMER];
PRIVATE HW_TIMER_tsConfig_t	sHWTimerConfig[NUMBER_HW_TIMER];

PRIVATE uint8_t		u8ArrHWTimer[NUMBER_HW_TIMER] = {
		E_AHI_TIMER_0,
		E_AHI_TIMER_1,
		E_AHI_TIMER_2,
		E_AHI_TIMER_3,
		E_AHI_TIMER_4,
};
PRIVATE uint8_t		u8ArrHWTimerPrescale[NUMBER_HW_TIMER_CYCLE] = {
		4,
		10,
		14
};
PRIVATE uint32_t	u32ArrHWTimerClkFreq[NUMBER_HW_TIMER_CYCLE];		//Hz
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void HW_TIMER_vInitialize(void)
{
	uint8_t	i;
	// Initialize for hardware timer
	vAHI_Timer0RegisterCallback(HW_TIMER_vISRTimer0);
	vAHI_Timer1RegisterCallback(HW_TIMER_vISRTimer1);
	vAHI_Timer2RegisterCallback(HW_TIMER_vISRTimer2);
	vAHI_Timer3RegisterCallback(HW_TIMER_vISRTimer3);
	vAHI_Timer4RegisterCallback(HW_TIMER_vISRTimer4);

	for(i = 0; i < NUMBER_HW_TIMER_CYCLE; i++)
	{
		u32ArrHWTimerClkFreq[i] = SOURCE_CLOCK_FREQ/pow(2,u8ArrHWTimerPrescale[i]);
		DBG_vPrintf(TRACE_HW_TIMER, "HW_TIMER: u16ArrHWTimerFrequency[%d] = %d Hz \n", i, u32ArrHWTimerClkFreq[i]);
	}
	for(i = 0; i < NUMBER_HW_TIMER; i++)
	{
		sHWTimerParams[i].eState = E_TIMER_STATE_FREE;


		sHWTimerConfig[i].bLocation 					= FALSE;
		sHWTimerConfig[i].bLocationOverridePWM3andPWM2 	= FALSE;
	}
}

PUBLIC bool_t HW_TIMER_bEnable(
		HW_TIMER_teTimerNumber_t		eHWTimer,
		bool_t								bAllowOtherAppUseThisTimer,
		HW_TIMER_teTimerCycle_t		eTimerCycle,
		bool_t 								bIntRiseEnable,
		bool_t 								bIntPeriodEnable,
		fbCallbackTimerInt					fnCallback,
		void*								pvParams,

		HW_TIMER_teOutput_t	eTimerOutput,
		uint32_t							u32UserOutputMask
)
{
	if(sHWTimerParams[eHWTimer].eState == E_TIMER_STATE_OCCUPIED_NOT_PERMIT_OVERRIDE)
	{
		DBG_vPrintf(TRACE_HW_TIMER,
				"HW_TIMER: Enable Timer %d Error: it was already occupied by other application without permission to override \n",
				eHWTimer);
		return FALSE;
	}
	else if(sHWTimerParams[eHWTimer].eState == E_TIMER_STATE_OCCUPIED_PERMIT_OVERRIDE)
	{
		HW_TIMER_vDisable(eHWTimer);
	}
	//
	sHWTimerParams[eHWTimer].bEnableUserOutput =(eTimerOutput == E_TIMER_OUTPUT_BY_USER)
												&&(u32UserOutputMask != 0UL);
	//
	sHWTimerConfig[eHWTimer].u8Prescale 		= u8ArrHWTimerPrescale[eTimerCycle];
	sHWTimerConfig[eHWTimer].bIntRiseEnable 	= 	(bIntRiseEnable && (fnCallback != NULL))
													||(sHWTimerParams[eHWTimer].bEnableUserOutput);

	sHWTimerConfig[eHWTimer].bIntPeriodEnable 	= 	(bIntPeriodEnable && (fnCallback != NULL))
													||(sHWTimerParams[eHWTimer].bEnableUserOutput);
	sHWTimerConfig[eHWTimer].bOutputEnable		=
			((eHWTimer == E_HW_TIMER_0) &&	(
					(eTimerOutput == E_TIMER_0_DEFAULT_OUTPUT_DIO_4)
					||(eTimerOutput == E_TIMER_0_DEFAULT_OUTPUT_DIO_10))
			)
			||(((eHWTimer == E_HW_TIMER_1) || (eHWTimer == E_HW_TIMER_2) || (eHWTimer == E_HW_TIMER_3) || (eHWTimer == E_HW_TIMER_4)) &&	(
					(eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_5_6_7_8)
					||(eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_11_12_13_17))
			)
			||(((eHWTimer == E_HW_TIMER_2) || (eHWTimer == E_HW_TIMER_3)) &&	(
					(eTimerOutput == E_TIMER_2_3_DEFAULT_OUTPUT_DO_0_1))
			);
	//
	sHWTimerConfig[eHWTimer].bLocation =
			((eHWTimer == E_HW_TIMER_0) &&	(eTimerOutput == E_TIMER_0_DEFAULT_OUTPUT_DIO_4))
			||(((eHWTimer == E_HW_TIMER_1) || (eHWTimer == E_HW_TIMER_2) || (eHWTimer == E_HW_TIMER_3) || (eHWTimer == E_HW_TIMER_4)) &&	(
					(eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_5_6_7_8)));

	sHWTimerConfig[eHWTimer].bLocationOverridePWM3andPWM2 =
			(((eHWTimer == E_HW_TIMER_2) || (eHWTimer == E_HW_TIMER_3)) &&	(eTimerOutput == E_TIMER_2_3_DEFAULT_OUTPUT_DO_0_1));
	//
	sHWTimerConfig[eHWTimer].bDIOEnable = sHWTimerConfig[eHWTimer].bOutputEnable;
	//
	if(bAllowOtherAppUseThisTimer)
	{
		sHWTimerParams[eHWTimer].eState				= E_TIMER_STATE_OCCUPIED_PERMIT_OVERRIDE;
	}
	else
	{
		sHWTimerParams[eHWTimer].eState				= E_TIMER_STATE_OCCUPIED_NOT_PERMIT_OVERRIDE;
	}
	sHWTimerParams[eHWTimer].u32ClokFreq 		= u32ArrHWTimerClkFreq[eTimerCycle];
	sHWTimerParams[eHWTimer].bIntRiseEnable 	= bIntRiseEnable;
	sHWTimerParams[eHWTimer].bIntPeriodEnable 	= bIntPeriodEnable;
	sHWTimerParams[eHWTimer].fnCallback 		= fnCallback;
	sHWTimerParams[eHWTimer].pvParams 			= pvParams;
	sHWTimerParams[eHWTimer].bEnableDefaultOutput = sHWTimerConfig[eHWTimer].bOutputEnable;

	sHWTimerParams[eHWTimer].u32TimerOutputMask = 0;
	if(sHWTimerParams[eHWTimer].bEnableDefaultOutput)
	{
		uint8_t u8TimerOutputPin = 100;
		if(eHWTimer == E_HW_TIMER_0)
		{
			if(eTimerOutput == E_TIMER_0_DEFAULT_OUTPUT_DIO_4)
			{
				u8TimerOutputPin = 4;
			}
			else if (eTimerOutput == E_TIMER_0_DEFAULT_OUTPUT_DIO_10)
			{
				u8TimerOutputPin = 10;
			}
		}
		else if(eHWTimer == E_HW_TIMER_1)
		{
			if(eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_5_6_7_8)
			{
				u8TimerOutputPin = 5;
			}
			else if (eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_11_12_13_17)
			{
				u8TimerOutputPin = 11;
			}

		}
		else if(eHWTimer == E_HW_TIMER_2)
		{
			if(eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_5_6_7_8)
			{
				u8TimerOutputPin = 6;
			}
			else if (eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_11_12_13_17)
			{
				u8TimerOutputPin = 12;
			}
			else if (eTimerOutput == E_TIMER_2_3_DEFAULT_OUTPUT_DO_0_1)
			{
				u8TimerOutputPin = DO_0;
			}
		}
		else if(eHWTimer == E_HW_TIMER_3)
		{
			if(eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_5_6_7_8)
			{
				u8TimerOutputPin = 7;
			}
			else if (eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_11_12_13_17)
			{
				u8TimerOutputPin = 13;
			}
			else if (eTimerOutput == E_TIMER_2_3_DEFAULT_OUTPUT_DO_0_1)
			{
				u8TimerOutputPin = DO_1;
			}
		}
		else if(eHWTimer == E_HW_TIMER_4)
		{
			if(eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_5_6_7_8)
			{
				u8TimerOutputPin = 8;
			}
			else if (eTimerOutput == E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_11_12_13_17)
			{
				u8TimerOutputPin = 17;
			}
		}
		sHWTimerParams[eHWTimer].u32TimerOutputMask = 1UL << u8TimerOutputPin;
	}
	else if(sHWTimerParams[eHWTimer].bEnableUserOutput)
	{
		sHWTimerParams[eHWTimer].u32TimerOutputMask = u32UserOutputMask;
		vAHI_DioSetDirection(0, u32UserOutputMask);
		if(u32UserOutputMask >> DO_0)
		{
			bAHI_DoEnableOutputs(TRUE);
		}
	}

	vHWTimer_Enable(eHWTimer);

	return TRUE;
}

PUBLIC void HW_TIMER_vStartRepeat(
		HW_TIMER_teTimerNumber_t	eHWTimer,
		uint16 							u16Hi_ms,			//ms
		uint16 							u16Lo_ms			//ms
)
{
	uint8 u8Timer 		= u8ArrHWTimer[eHWTimer];

	if(sHWTimerParams[eHWTimer].eState	== E_TIMER_STATE_FREE)
	{
		DBG_vPrintf(TRACE_HW_TIMER,
				"HW_TIMER: Timer %d is not enable, please enable first \n",
				eHWTimer);
		return;
	}
	// Convert from ms to number of clock
	uint16_t u16Hi = (u16Hi_ms*sHWTimerParams[eHWTimer].u32ClokFreq)/1000;
	uint16_t u16Lo = (u16Lo_ms*sHWTimerParams[eHWTimer].u32ClokFreq)/1000;

	DBG_vPrintf(TRACE_HW_TIMER,
				"HW_TIMER: Start Timer %d repeatedly with u16Hi: %d, u16Lo: %d \n",
				eHWTimer, u16Hi, u16Lo);
	vAHI_TimerStartRepeat(u8Timer, u16Hi, u16Lo);
}

PUBLIC void HW_TIMER_vStartSingleShot(
		HW_TIMER_teTimerNumber_t	eHWTimer,
		uint16 						u16Hi_ms,
		uint16 						u16Lo_ms
)
{
	uint8 u8Timer 		= u8ArrHWTimer[eHWTimer];

	if(sHWTimerParams[eHWTimer].eState	== E_TIMER_STATE_FREE)
	{
		DBG_vPrintf(TRACE_HW_TIMER,
				"HW_TIMER: Timer %d is not enable, please enable first \n",
				eHWTimer);
		return;
	}
	// Convert from ms to number of clock
	uint16_t u16Hi = (u16Hi_ms*sHWTimerParams[eHWTimer].u32ClokFreq)/1000;
	uint16_t u16Lo = (u16Lo_ms*sHWTimerParams[eHWTimer].u32ClokFreq)/1000;

	DBG_vPrintf(TRACE_HW_TIMER,
				"HW_TIMER: Start Timer %d 1 time with u16Hi: %d, u16Lo: %d \n",
				eHWTimer, u16Hi, u16Lo);
	vAHI_TimerStartSingleShot(u8Timer, u16Hi, u16Lo);
}

PUBLIC void HW_TIMER_vStartSingleShot_us(
		HW_TIMER_teTimerNumber_t	eHWTimer,
		uint16 						u16Hi_us,
		uint16 						u16Lo_us
)
{
	uint8 u8Timer 		= u8ArrHWTimer[eHWTimer];

	if(sHWTimerParams[eHWTimer].eState	== E_TIMER_STATE_FREE)
	{
		DBG_vPrintf(TRACE_HW_TIMER,
				"HW_TIMER: Timer %d is not enable, please enable first \n",
				eHWTimer);
		return;
	}
	// Convert from us to number of clock
	uint16_t u16Hi = (u16Hi_us*sHWTimerParams[eHWTimer].u32ClokFreq)/1000000;
	uint16_t u16Lo = (u16Lo_us*sHWTimerParams[eHWTimer].u32ClokFreq)/1000000;

	DBG_vPrintf(TRACE_HW_TIMER,
			"HW_TIMER: Start Timer %d 1 time with u16Hi: %d, u16Lo: %d \n",
			eHWTimer, u16Hi, u16Lo);
	vAHI_TimerStartSingleShot(u8Timer, u16Hi, u16Lo);
}

PUBLIC void HW_TIMER_vStop(
		HW_TIMER_teTimerNumber_t	eHWTimer
)
{
	if(sHWTimerParams[eHWTimer].eState == E_TIMER_STATE_FREE)
	{
		return;
	}
	uint8 u8Timer 		= u8ArrHWTimer[eHWTimer];
	vAHI_TimerStop(u8Timer);
	// Turn off all output
	if(sHWTimerParams[eHWTimer].bEnableUserOutput)
	{
		vAHI_DioSetOutput(0, sHWTimerParams[eHWTimer].u32TimerOutputMask);
		vAHI_DoSetDataOut(0, sHWTimerParams[eHWTimer].u32TimerOutputMask >> DO_0);
	}
	else if(sHWTimerParams[eHWTimer].bEnableDefaultOutput)
	{
		// Disable timer first
		vAHI_TimerDisable(u8Timer);
		// turn off output
//		vAHI_DioSetDirection(0, sHWTimerParams[eHWTimer].u32TimerOutputMask);
//		vAHI_DioSetOutput(0, sHWTimerParams[eHWTimer].u32TimerOutputMask);
//		vAHI_DoSetDataOut(0, sHWTimerParams[eHWTimer].u32TimerOutputMask >> DO_0);
		// ReEnable timer
		vHWTimer_Enable(eHWTimer);
	}
}

PUBLIC void HW_TIMER_vDisable(
		HW_TIMER_teTimerNumber_t	eHWTimer
)
{
	if(sHWTimerParams[eHWTimer].eState == E_TIMER_STATE_FREE)
	{
		return;
	}
	uint8 u8Timer 		= u8ArrHWTimer[eHWTimer];

	vAHI_TimerDisable(u8Timer);
	// Turn off all output
	vAHI_DioSetDirection(0, sHWTimerParams[eHWTimer].u32TimerOutputMask);
	vAHI_DioSetOutput(0, sHWTimerParams[eHWTimer].u32TimerOutputMask);
	vAHI_DoSetDataOut(0, sHWTimerParams[eHWTimer].u32TimerOutputMask >> DO_0);
	//
	sHWTimerParams[eHWTimer].eState				= E_TIMER_STATE_FREE;
	sHWTimerParams[eHWTimer].u32ClokFreq 		= 1;
	sHWTimerParams[eHWTimer].bIntRiseEnable 	= FALSE;
	sHWTimerParams[eHWTimer].bIntPeriodEnable 	= FALSE;
	sHWTimerParams[eHWTimer].bEnableDefaultOutput 	= FALSE;
	sHWTimerParams[eHWTimer].bEnableUserOutput 	= FALSE;
	sHWTimerParams[eHWTimer].u32TimerOutputMask	= 0;

	sHWTimerParams[eHWTimer].fnCallback 	= NULL;
	sHWTimerParams[eHWTimer].pvParams 		= NULL;
}
PUBLIC void HW_TIMER_vISRTimer0(uint32 u32Device, uint32 u32ItemBitmap)
{
	vHWTimer_ProcessInterrupt(E_HW_TIMER_0);
}
PUBLIC void HW_TIMER_vISRTimer1(uint32 u32Device, uint32 u32ItemBitmap)
{
	vHWTimer_ProcessInterrupt(E_HW_TIMER_1);
}
PUBLIC void HW_TIMER_vISRTimer2(uint32 u32Device, uint32 u32ItemBitmap)
{
	vHWTimer_ProcessInterrupt(E_HW_TIMER_2);
}
PUBLIC void HW_TIMER_vISRTimer3(uint32 u32Device, uint32 u32ItemBitmap)
{
	vHWTimer_ProcessInterrupt(E_HW_TIMER_3);
}
PUBLIC void HW_TIMER_vISRTimer4(uint32 u32Device, uint32 u32ItemBitmap)
{
	vHWTimer_ProcessInterrupt(E_HW_TIMER_4);
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vHWTimer_ProcessInterrupt(
		HW_TIMER_teTimerNumber_t	eHWTimer
)
{
	uint8_t u8Event = u8AHI_TimerFired(u8ArrHWTimer[eHWTimer]);
	if(u8Event & E_AHI_TIMER_RISE_MASK)
	{
		DBG_vPrintf(TRACE_HW_TIMER, "HW_TIMER: E_AHI_TIMER_RISE_MASK timer %d \n", eHWTimer);
		if(sHWTimerParams[eHWTimer].bIntRiseEnable && sHWTimerParams[eHWTimer].fnCallback != NULL)
		{
			HW_TIMER_tsTimerEvent_t sEvent;
			sEvent.pvParams = sHWTimerParams[eHWTimer].pvParams;
			sEvent.u8Timer	= eHWTimer;
			sEvent.eIntType	= E_TIMER_INT_RISING_EDGE;

			sHWTimerParams[eHWTimer].fnCallback(sEvent);
		}

		if(sHWTimerParams[eHWTimer].bEnableUserOutput)
		{
			vAHI_DioSetOutput(sHWTimerParams[eHWTimer].u32TimerOutputMask, 0);
			vAHI_DoSetDataOut(sHWTimerParams[eHWTimer].u32TimerOutputMask >> DO_0, 0);
		}
	}
	else if(u8Event & E_AHI_TIMER_PERIOD_MASK)
	{
		DBG_vPrintf(TRACE_HW_TIMER, "HW_TIMER: E_TIMER_INT_FALLING_EDGE timer %d \n", eHWTimer);
		if(sHWTimerParams[eHWTimer].bIntRiseEnable && sHWTimerParams[eHWTimer].fnCallback != NULL)
		{
			HW_TIMER_tsTimerEvent_t sEvent;
			sEvent.pvParams = sHWTimerParams[eHWTimer].pvParams;
			sEvent.u8Timer	= eHWTimer;
			sEvent.eIntType	= E_TIMER_INT_FALLING_EDGE;

			sHWTimerParams[eHWTimer].fnCallback(sEvent);
		}

		if(sHWTimerParams[eHWTimer].bEnableUserOutput)
		{
			vAHI_DioSetOutput(0, sHWTimerParams[eHWTimer].u32TimerOutputMask);
			vAHI_DoSetDataOut(0, sHWTimerParams[eHWTimer].u32TimerOutputMask >> DO_0);
		}
	}
}
PRIVATE void vHWTimer_Enable(
		HW_TIMER_teTimerNumber_t	eHWTimer
)
{
	uint8_t i;
	bool_t bLocation = FALSE;
	bool_t bLocationOverridePWM3andPWM2 = FALSE;

	if(sHWTimerParams[eHWTimer].eState == E_TIMER_STATE_FREE)
	{
		return;
	}
	uint8_t	u8Timer = u8ArrHWTimer[eHWTimer];
	vAHI_TimerDIOControl(
			u8Timer,
			sHWTimerConfig[eHWTimer].bDIOEnable
	);

	vAHI_TimerEnable(
			u8Timer,
			sHWTimerConfig[eHWTimer].u8Prescale,
			sHWTimerConfig[eHWTimer].bIntRiseEnable,
			sHWTimerConfig[eHWTimer].bIntPeriodEnable,
			sHWTimerConfig[eHWTimer].bOutputEnable
	);

	if (sHWTimerConfig[eHWTimer].bOutputEnable)
	{
		for(i = 0; i < NUMBER_HW_TIMER; i++)
		{
			bLocation |= sHWTimerConfig[i].bLocation;
			bLocationOverridePWM3andPWM2 |= sHWTimerConfig[i].bLocationOverridePWM3andPWM2;
		}

		if(bLocation || bLocationOverridePWM3andPWM2)
		{
			//		vAHI_TimerConfigureOutputs(
			//				u8Timer,
			//				FALSE,
			//				TRUE
			//		);

			vAHI_TimerSetLocation(
					u8Timer,
					bLocation,
					bLocationOverridePWM3andPWM2
			);
		}
	}

	DBG_vPrintf(TRACE_HW_TIMER,
			"HW_TIMER: Enable timer %d, prescale %d, IntRise %d, IntPeriod %d, OutputEnable %d, bLocation %d (%d), bLocationOverridePWM2_3 %d (%d) \n",
			u8Timer,
			sHWTimerConfig[eHWTimer].u8Prescale,
			sHWTimerConfig[eHWTimer].bIntRiseEnable,
			sHWTimerConfig[eHWTimer].bIntPeriodEnable,
			sHWTimerConfig[eHWTimer].bOutputEnable,
			sHWTimerConfig[eHWTimer].bLocation, bLocation,
			sHWTimerConfig[eHWTimer].bLocationOverridePWM3andPWM2, bLocationOverridePWM3andPWM2);

}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
