/*
 * app_bdb_task.c
 *
 *  Created on: Apr 4, 2019
 *      Author: TrungTQ9
 */

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include <dbg.h>
#include <bdb_api.h>
#include "app_common.h"
#include "app_nwk_handler.h"
#include "app_node.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_APP_BDB_TASK
    #define TRACE_APP_BDB_TASK              (FALSE)
#else   /* DEBUG_APP_BDB_TASK */
    #define TRACE_APP_BDB_TASK              (TRUE)
#endif  /* DEBUG_APP_BDB_TASK */

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
/*
 * @brief   Callback from the BDB. Read 2.9 - JN-UG-3114
 * @param   BDB_tsBdbEvent      *psBdbEvent
 */
PUBLIC void APP_vBdbCallback(
    BDB_tsBdbEvent *psBdbEvent
) {
    switch (psBdbEvent->eEventType) {
    case BDB_EVENT_NONE:
        break;

    case BDB_EVENT_ZPSAF: // Use with BDB_tsZpsAfEvent
//         DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: ZPSAF\n");
        APP_NwkHandler_vHandleAfEvent(&psBdbEvent->uEventData.sZpsAfEvent);
        break;

    case BDB_EVENT_INIT_SUCCESS:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Init Success\n");
        APP_NwkHandler_vBdbInitSuccess();
        break;

    case BDB_EVENT_REJOIN_FAILURE: // only for ZED JN-UG-3114 page20
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Rejoin failure\n");
        bBdbJoinFailed = TRUE;
        APP_NwkHandler_vRejoinFailure();
        break;

    case BDB_EVENT_REJOIN_SUCCESS: // only for ZED JN-UG-3114 page20
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Rejoin Success\n");
        bBdbJoinFailed = FALSE;
        APP_NwkHandler_vRejoinSuccess();
        break;

        /* Event network steering success */
    case BDB_EVENT_NWK_STEERING_SUCCESS:
        DBG_vPrintf(TRACE_APP_BDB_TASK,
                    "APP_BDB: NwkSteering Success 0x%016llx\n",
                    ZPS_psAplAibGetAib()->u64ApsUseExtendedPanid);
        bBdbJoinFailed = FALSE;
        APP_NwkHandler_vNetworkSteeringSuccess();
        break;
        /*********************************/
        /* Event network steering failed */
    case BDB_EVENT_NO_NETWORK:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: No Network\n");
        bBdbJoinFailed = TRUE;
        APP_NwkHandler_vNetworkSteeringFailure();
        break;
    case BDB_EVENT_NWK_JOIN_FAILURE:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Join Failure\n");
        bBdbJoinFailed = TRUE;
//        APP_NwkHandler_vNetworkSteeringFailure();
        break;
        /*********************************/
    case BDB_EVENT_NWK_JOIN_SUCCESS:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Join Success\n");
        break;

    case BDB_EVENT_APP_START_POLLING:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Start Polling\n");
        APP_NwkHandler_vStartPollTimer();
        break;

    case BDB_EVENT_NWK_FORMATION_SUCCESS:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Nwk Formation Success\n");
        break;

    case BDB_EVENT_NWK_FORMATION_FAILURE:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Nwk Formation Failure\n");
        break;

    case BDB_EVENT_FB_HANDLE_SIMPLE_DESC_RESP_OF_TARGET:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Fb Handle Simple Desc Resp Of Target\n");
        break;

    case BDB_EVENT_FB_CHECK_BEFORE_BINDING_CLUSTER_FOR_TARGET:
        DBG_vPrintf(TRACE_APP_BDB_TASK,
                    "APP_BDB: Check For Binding Cluster %d \n",
                    psBdbEvent->uEventData.psFindAndBindEvent->uEvent.u16ClusterId);
        break;

    case BDB_EVENT_FB_CLUSTER_BIND_CREATED_FOR_TARGET:
        DBG_vPrintf(TRACE_APP_BDB_TASK,
                    "APP_BDB: Bind Created for cluster %d \n",
                    psBdbEvent->uEventData.psFindAndBindEvent->uEvent.u16ClusterId);
        break;

    case BDB_EVENT_FB_BIND_CREATED_FOR_TARGET:
        DBG_vPrintf(TRACE_APP_BDB_TASK,
                    "APP_BDB: Bind Created for target EndP %d\n",
                    psBdbEvent->uEventData.psFindAndBindEvent->u8TargetEp);
        break;

    case BDB_EVENT_FB_GROUP_ADDED_TO_TARGET:
        DBG_vPrintf(TRACE_APP_BDB_TASK,
                    "APP_BDB: Group Added with ID %04x\n",
                    psBdbEvent->uEventData.psFindAndBindEvent->uEvent.u16GroupId);
        break;

    case BDB_EVENT_FB_ERR_BINDING_FAILED:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: ERR Bind\n");
        break;

    case BDB_EVENT_FB_ERR_BINDING_TABLE_FULL:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: ERR Bind Table Full\n");
        break;

    case BDB_EVENT_FB_ERR_GROUPING_FAILED:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: ERR Group\n");
        break;

    case BDB_EVENT_FB_NO_QUERY_RESPONSE:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: ERR No Query response\n");
        break;

    case BDB_EVENT_FB_TIMEOUT:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: ERR TimeOut\n");
        break;

    case BDB_EVENT_FB_OVER_AT_TARGET:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: ERR Over At Target\n");
        break;

    case BDB_EVENT_FAILURE_RECOVERY_FOR_REJOIN:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Failure Recovery For Rejoin\n");
//        APP_NwkHandler_vFailedRejoin();
        break;

    case BDB_EVENT_LEAVE_WITHOUT_REJOIN:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: Leave Without Rejoin\n");
        break;

    case BDB_EVENT_OOB_FORM_SUCCESS:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: OOB Form Success\n");
        break;

    case BDB_EVENT_OOB_JOIN_SUCCESS:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: OOB Join Success\n");
        break;

    case BDB_EVENT_OOB_FAIL:
        DBG_vPrintf(TRACE_APP_BDB_TASK, "APP_BDB: OOB Fail\n");
        break;

    default:
        break;
    }
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
