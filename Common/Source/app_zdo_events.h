/*
 * app_zdo_events.h
 *
 *  Created on: Apr 14, 2019
 *      Author: TrungTQ9
 */

#ifndef APP_ZDO_EVENTS_H_
#define APP_ZDO_EVENTS_H_

/*
 * @brief   Callback from the BDB
 * @param   BDB_tsBdbEvent      *psBdbEvent
 */
PUBLIC void APP_ZdoEvents_vHandle(BDB_tsZpsAfEvent *psZpsAfEvent);

#endif /* APP_ZDO_EVENTS_H_ */
