/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_motor.h
 *
 * DESCRIPTION:        Control motor
 *
 ***************************************************************************/

#ifndef DRV_MOTOR_H_
#define DRV_MOTOR_H_
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "jendefs.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DRV_MOTOR_ZTIMER_STORAGE
	#define DRV_MOTOR_ZTIMER_STORAGE     (1)
#endif /* DRV_MOTOR_ZTIMER_STORAGE */

#define MOTOR_PWM_CYCLE			(20)			//ms
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef void (*fnCbAfterRunMotor)(void*);

typedef struct
{
	uint8_t				u8VibrationLevel;			//0-100%
	fnCbAfterRunMotor	fnCallback;
	void				*pvParams;
}DRV_MOTOR_tsParams_t;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void DRV_MOTOR_vInitialize(
		uint8_t		u8VibrationLevel				//0-100%
);

PUBLIC void DRV_MOTOR_vSetVibrationLevel(
		uint8_t		u8VibrationLevel				//0-100%
);

PUBLIC void DRV_MOTOR_vRun(
		uint16_t 			u16TimeOn,
		fnCbAfterRunMotor	fnCallback,
		void				*pvParams
);

PUBLIC void DRV_MOTOR_vStop(void);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
#endif /* DRV_MOTOR_H_ */
