
/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_nwk_handler.h
 *
 * DESCRIPTION:        Process network event
 *
 ***************************************************************************/
#ifndef APP_NWK_HANDLER_H_
#define APP_NWK_HANDLER_H_
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "jendefs.h"
#include "bdb_api.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
// Common

// For Router Device only

// For End Device only
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
// Common for all device
PUBLIC void APP_NwkHandler_vNetworkLeaveConfirm(
    ZPS_tsAfEvent* psStackEvent
);
PUBLIC void APP_NwkHandler_vNetworkLeaveIndication(
	ZPS_tsAfEvent* psStackEvent
);

// Common but have some differences
PUBLIC void APP_NwkHandler_vBdbInitSuccess(void);
PUBLIC void APP_NwkHandler_vHandleAfEvent(
    BDB_tsZpsAfEvent *psZpsAfEvent
);
PUBLIC void APP_NwkHandler_vNetworkSteeringSuccess(void);
PUBLIC void APP_NwkHandler_vNetworkSteeringFailure(void);
// For Router Device only

// For End Device only
PUBLIC void APP_NwkHandler_vJoinedAsEndDevice(void);
PUBLIC void APP_NwkHandler_vStartPollTimer(void);
PUBLIC void APP_NwkHandler_cbTimerPoll(void *pvParam);
PUBLIC void APP_NwkHandler_vPollResponse(
    ZPS_tsAfEvent* psStackEvent
);
PUBLIC void APP_NwkHandler_vRejoinSuccess(void);
PUBLIC void APP_NwkHandler_vRejoinFailure(void);

// Utilities
PUBLIC void APP_NwkHandler_vPrintAPSTable(void);
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /* APP_NWK_HANDLER_H_ */
