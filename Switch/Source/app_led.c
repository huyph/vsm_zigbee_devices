/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_led.c
 *
 * DESCRIPTION:        Base Device application - reporting functionality
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "app_led.h"
#include "dbg.h"
#include "my_define.h"
#include "app_node.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_APP_LED
    #define TRACE_APP_LED              (TRUE)
#else   /* DEBUG_APP_LED */
    #define TRACE_APP_LED              (FALSE)
#endif  /* DEBUG_APP_LED */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
#define MAX_NUMBER_OF_LED		(4)

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vCallbackAfterBlinkLed(void *pvParams);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE	uint8_t u8ArrLedRGBPin[MAX_NUMBER_OF_LED][NUMBER_OF_SINGLE_LED_ON_LED_RGB] = {
		{
			PIN_OUT_LED_RED_SW1,
			LED_NOT_USE,
			PIN_OUT_LED_BLUE_SW1
		},
		{
			PIN_OUT_LED_RED_SW2,
			LED_NOT_USE,
			PIN_OUT_LED_BLUE_SW2
		},
		{
			PIN_OUT_LED_RED_SW3,
			LED_NOT_USE,
			PIN_OUT_LED_BLUE_SW3
		},
		{
			PIN_OUT_LED_RED_SW4,
			LED_NOT_USE,
			PIN_OUT_LED_BLUE_SW4
		}
};

PRIVATE uint32_t	u32AllLedPinMask;
PRIVATE uint32_t	u32LedPinMask[NUMBER_OF_SINGLE_LED_ON_LED_RGB];

PRIVATE uint8_t		NUMBER_OF_LED;
PRIVATE APP_LED_tsLedEvent_t sCurLedEvent;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_LED_vInitialize(void)
{
	uint8 u8TypeLed, u8LedNumber;
	//
	NUMBER_OF_LED 		= u8NumberOfSwitch;
	//
	u32AllLedPinMask = 0UL;
	for(u8TypeLed = 0; u8TypeLed < NUMBER_OF_SINGLE_LED_ON_LED_RGB; u8TypeLed++)
	{
		u32LedPinMask[u8TypeLed] = 0UL;
	}

	for(u8LedNumber = 0; u8LedNumber < NUMBER_OF_LED; u8LedNumber++)
	{
		for(u8TypeLed = 0; u8TypeLed < NUMBER_OF_SINGLE_LED_ON_LED_RGB; u8TypeLed ++)
		{
			if(LED_NOT_USE != u8ArrLedRGBPin[u8LedNumber][u8TypeLed])
			{
				u32LedPinMask[u8TypeLed] |= 1UL << u8ArrLedRGBPin[u8LedNumber][u8TypeLed];
				u32AllLedPinMask |= 1UL << u8ArrLedRGBPin[u8LedNumber][u8TypeLed];
			}
		}
	}

	DBG_vPrintf(TRACE_APP_LED,"APP_LED: LedPinMask of All Led : 0x%08x \n", u32AllLedPinMask);

	if(!DRV_LEDS_bInitialize(u32AllLedPinMask, 100))
	{
		DBG_vPrintf(TRACE_APP_LED,"APP_LED: Initialize failed! \n");
	}

	APP_LED_vUpdateLedStatusOfAllSwitch(NULL);
}

PUBLIC void APP_LED_vSetLevel(uint8_t u8LedLevel)
{
	DRV_LEDS_vSetLevel(u8LedLevel);
}

PUBLIC void APP_LED_vStart(
	APP_LED_tsLedEvent_t sLedEvent
)
{
	if(E_STATE_BLINK_FACTORY_RESET == sCurLedEvent.eLedState )
		return;

	if(E_STATE_NONE != sCurLedEvent.eLedState)
	{
		//Stop blink all Led
		DRV_LEDS_vStopBlinkLeds(TRUE, 0);
		APP_LED_vUpdateLedStatusOfAllSwitch(NULL);
	}
	//
	if(E_STATE_BLINK_NWK_STEERING_SUCCESS == sLedEvent.eLedState)
	{
		// turn off led red of switch 0
		APP_LED_vSet(0, E_LED_RED, FALSE, -1);
		// Blink led blue of switch 0
		DRV_LEDS_vBlinkLedsAlternately(1UL << u8ArrLedRGBPin[0][E_LED_BLUE], 0, -1,
				3000, 100, 100,
				vCallbackAfterBlinkLed, NULL);
	}
	else if(E_STATE_BLINK_NWK_STEERING_FAILURE == sLedEvent.eLedState)
	{
		// turn off led blue of switch 0
		APP_LED_vSet(0, E_LED_BLUE, FALSE, -1);
		// Blink led red of switch 0
		DRV_LEDS_vBlinkLedsAlternately(1UL << u8ArrLedRGBPin[0][E_LED_RED], 0, -1,
				3000, 500, 500,
				vCallbackAfterBlinkLed, NULL);
	}
	//
	else if(E_STATE_BLINK_ACK_FAIL == sLedEvent.eLedState)
	{
		if(APP_SWITCH_bGetSwitchOnOffStatus(sLedEvent.u8RGBLedNumber))
		{
			// turn off led red of switch
			APP_LED_vSet(sLedEvent.u8RGBLedNumber, E_LED_RED, FALSE, -1);
			// Blink led blue of switch
			DRV_LEDS_vBlinkLedsAlternately(1UL << u8ArrLedRGBPin[sLedEvent.u8RGBLedNumber][E_LED_BLUE], 0, -1,
					3000, 100, 100,
					vCallbackAfterBlinkLed, NULL);
		}
		else
		{
			// turn off led blue of switch
			APP_LED_vSet(sLedEvent.u8RGBLedNumber, E_LED_BLUE, FALSE, -1);
			// Blink led sLedEvent of switch
			DRV_LEDS_vBlinkLedsAlternately(1UL << u8ArrLedRGBPin[sLedEvent.u8RGBLedNumber][E_LED_RED], 0, -1,
					3000, 100, 100,
					vCallbackAfterBlinkLed, NULL);
		}
	}
	//
	else if(E_STATE_BLINK_CHECK_NWK_CONECTION_GOOD == sLedEvent.eLedState)
	{
		// turn off led red of switch 0
		APP_LED_vSet(0, E_LED_RED, FALSE, -1);
		// Blink led blue of switch 0
		DRV_LEDS_vBlinkLedsAlternately(1UL << u8ArrLedRGBPin[0][E_LED_BLUE], 0, -1,
				3000, 100, 100,
				vCallbackAfterBlinkLed, NULL);
	}
	else if(E_STATE_BLINK_CHECK_NWK_CONECTION_ERROR == sLedEvent.eLedState)
	{
		// turn off led blue of switch 0
		APP_LED_vSet(0, E_LED_BLUE, FALSE, -1);
		// Blink led red of switch 0
		DRV_LEDS_vBlinkLedsAlternately(1UL << u8ArrLedRGBPin[0][E_LED_RED], 0, -1,
				3000, 500, 500,
				vCallbackAfterBlinkLed, NULL);
	}
	//
	else if(E_STATE_BLINK_FACTORY_RESET == sLedEvent.eLedState)
	{
		APP_LED_vUpdateLedStatusOfAllSwitch(NULL);

		DRV_LEDS_vBlinkLedsAlternately(u32LedPinMask[E_LED_RED], u32LedPinMask[E_LED_BLUE], -1,
					5000, 250, 250,
					vCallbackAfterBlinkLed, NULL);
	}
	//
	else if(E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_ON_NWK == sLedEvent.eLedState)
	{
#ifdef SCENE_SWITCH_DEVICE
		DBG_vPrintf(TRACE_APP_LED,"APP_LED: Scene switch device on in 1000ms \n");
		APP_LED_vSet(sLedEvent.u8RGBLedNumber, E_LED_RED, FALSE, -1);
		APP_LED_vSet(sLedEvent.u8RGBLedNumber, E_LED_BLUE, TRUE, 1000);
#else
		APP_LED_vUpdateLedStatusOfSwitch(sLedEvent.u8RGBLedNumber);
#endif /* SCENE_SWITCH_DEVICE */
	}
	else if(E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_NOT_ON_NWK == sLedEvent.eLedState)
	{
		if(APP_SWITCH_bGetSwitchOnOffStatus(sLedEvent.u8RGBLedNumber))
		{
			// turn off led red of switch
			APP_LED_vSet(sLedEvent.u8RGBLedNumber, E_LED_RED, FALSE, -1);
			// Blink led blue of switch
			DRV_LEDS_vBlinkLedsAlternately(1UL << u8ArrLedRGBPin[sLedEvent.u8RGBLedNumber][E_LED_BLUE], 0, -1,
					3000, 100, 100,
					vCallbackAfterBlinkLed, NULL);
		}
		else
		{
			// turn off led blue of switch
			APP_LED_vSet(sLedEvent.u8RGBLedNumber, E_LED_BLUE, FALSE, -1);
			// Blink led sLedEvent of switch
			DRV_LEDS_vBlinkLedsAlternately(1UL << u8ArrLedRGBPin[sLedEvent.u8RGBLedNumber][E_LED_RED], 0, -1,
					3000, 100, 100,
					vCallbackAfterBlinkLed, NULL);
		}
	}

	sCurLedEvent = sLedEvent;
}

PUBLIC void APP_LED_vStartEvent(
	APP_LED_teLedState_t  eLedState
)
{
	APP_LED_tsLedEvent_t sLedEvent;
	sLedEvent.eLedState = eLedState;
	APP_LED_vStart(sLedEvent);
}
PUBLIC void APP_LED_vSet(
	uint8_t					u8RGBLedNumber,
	APP_LED_teTypeLed_t		eTypeLed,
	bool_t					bOnOff,
	int16_t					i16TimeOn
)
{
	DBG_vPrintf(TRACE_APP_LED,
			"APP_LED: APP_LED_vSet: Led number %d, Led type %d, LedPin %d \n",
			u8RGBLedNumber, eTypeLed, u8ArrLedRGBPin[u8RGBLedNumber][eTypeLed]);
	if((u8RGBLedNumber >= NUMBER_OF_LED) || (eTypeLed >= NUMBER_OF_SINGLE_LED_ON_LED_RGB))
			return;
	if(bOnOff)
	{
		if(i16TimeOn > 0)
		{
			DRV_LEDS_vBlinkLedsAlternately(1UL << u8ArrLedRGBPin[u8RGBLedNumber][eTypeLed], 0UL, 1,
					-1, i16TimeOn, 1,
					vCallbackAfterBlinkLed, NULL);

			DBG_vPrintf(TRACE_APP_LED,
						"APP_LED: On led %d in %d ms\n",u8ArrLedRGBPin[u8RGBLedNumber][eTypeLed], i16TimeOn);
		}
		else
		{
			DRV_LEDS_vOn(1UL << u8ArrLedRGBPin[u8RGBLedNumber][eTypeLed]);
		}
	}
	else
	{
		DRV_LEDS_vOff(1UL << u8ArrLedRGBPin[u8RGBLedNumber][eTypeLed]);
	}
}

PUBLIC void APP_LED_vUpdateLedStatusOfSwitch(
	APP_eSwitch 	eSwitch
)
{
	DBG_vPrintf(TRACE_APP_LED,
			"APP_LED: APP_LED_vUpdateLedStatusOfSwitch %d , status %d \n",
			eSwitch, APP_SWITCH_bGetSwitchOnOffStatus(eSwitch));
	if(APP_SWITCH_bGetSwitchOnOffStatus(eSwitch))
	{
		APP_LED_vSet(eSwitch, E_LED_RED, FALSE, -1);
		APP_LED_vSet(eSwitch, E_LED_BLUE, TRUE, -1);
	}
	else
	{
		APP_LED_vSet(eSwitch, E_LED_BLUE, FALSE, -1);
		APP_LED_vSet(eSwitch, E_LED_RED, TRUE, -1);
	}
}

PUBLIC void APP_LED_vUpdateLedStatusOfAllSwitch(void *pvParams)
{
	uint8_t	i;
	for(i = 0; i < NUMBER_OF_LED; i++)
	{
		APP_LED_vUpdateLedStatusOfSwitch(i);
	}
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

PRIVATE void vCallbackAfterBlinkLed(void *pvParams)
{
	DBG_vPrintf(TRACE_APP_LED,
				"APP_LED: vCallbackAfterBlinkLed event: %d \n", sCurLedEvent.eLedState);
	if(E_STATE_NONE == sCurLedEvent.eLedState)
	{
		return;
	}
	//
	if(E_STATE_BLINK_NWK_STEERING_SUCCESS == sCurLedEvent.eLedState)
	{
		APP_LED_vUpdateLedStatusOfAllSwitch(NULL);
	}
	else if(E_STATE_BLINK_NWK_STEERING_FAILURE == sCurLedEvent.eLedState)
	{
		APP_LED_vUpdateLedStatusOfAllSwitch(NULL);
	}
	//
	else if(E_STATE_BLINK_ACK_FAIL == sCurLedEvent.eLedState)
	{
		APP_LED_vUpdateLedStatusOfSwitch(sCurLedEvent.u8RGBLedNumber);
	}
	//
	else if(E_STATE_BLINK_CHECK_NWK_CONECTION_GOOD == sCurLedEvent.eLedState)
	{
		APP_LED_vUpdateLedStatusOfSwitch(0);
	}
	else if(E_STATE_BLINK_CHECK_NWK_CONECTION_ERROR == sCurLedEvent.eLedState)
	{
		APP_LED_vUpdateLedStatusOfSwitch(0);
	}
	//
	else if(E_STATE_BLINK_FACTORY_RESET == sCurLedEvent.eLedState)
	{
		APP_Node_vLeaveAndReset();
	}
	//
	else if(E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_ON_NWK == sCurLedEvent.eLedState)
	{
#ifdef SCENE_SWITCH_DEVICE
		APP_LED_vUpdateLedStatusOfSwitch(sCurLedEvent.u8RGBLedNumber);
#else

#endif /* SCENE_SWITCH_DEVICE */

	}
	else if(E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_NOT_ON_NWK == sCurLedEvent.eLedState)
	{
		APP_LED_vUpdateLedStatusOfSwitch(sCurLedEvent.u8RGBLedNumber);
	}
	sCurLedEvent.eLedState = E_STATE_NONE;
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/

