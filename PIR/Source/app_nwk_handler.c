/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_nwk_handler.c
 *
 * DESCRIPTION:        Process network event
 *
 ***************************************************************************/
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "app_nwk_handler.h"
#include "app_common.h"
#include "PDM.h"
#include "PDM_IDs.h"
#include "dbg.h"
#include "app_zdo_events.h"
#include "app_zcl_task.h"
#include "app_reporting.h"
#include "AppHardwareApi_JN516x.h"
#include "ZTimer.h"
#include "app_main.h"
#include "app_node.h"
#include "zps_gen.h"
#include "app_main.h"
#include "app_events.h"
#include "app_reporting.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_APP_NWK_HANDLER
    #define TRACE_APP_NWK_HANDLER               (FALSE)
#else   /* DEBUG_APP_NWK_HANDLER */
    #define TRACE_APP_NWK_HANDLER               (TRUE)
#endif  /* DEBUG_APP_NWK_HANDLER */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
extern PUBLIC teNodeState   eNodeState;
extern PUBLIC tszQueue 		APP_msgAppEvents;
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
// Common for all device
PUBLIC void APP_NwkHandler_vNetworkLeaveConfirm(
	ZPS_tsAfEvent* psStackEvent
)
{
	if ((ZPS_E_SUCCESS == psStackEvent->uEvent.sNwkLeaveConfirmEvent.eStatus) &&
			(0UL == psStackEvent->uEvent.sNwkLeaveConfirmEvent.u64ExtAddr))
	{
		DBG_vPrintf(TRACE_APP_NWK_HANDLER, "APP_NWK_HANDLER: Network Leave Confirm \n");
		APP_Node_vFactoryResetRecords();
		vAHI_SwReset();
	}
}

PUBLIC void APP_NwkHandler_vNetworkLeaveIndication(
	ZPS_tsAfEvent* psStackEvent
)
{
	if ((0UL == psStackEvent->uEvent.sNwkLeaveIndicationEvent.u64ExtAddr) &&
			(FALSE == psStackEvent->uEvent.sNwkLeaveIndicationEvent.u8Rejoin))
	{
		/* We are asked to Leave without rejoin */
		DBG_vPrintf(TRACE_APP_NWK_HANDLER, "APP_NWK_HANDLER: ZDO Leave \n");
		APP_Node_vFactoryResetRecords();
		vAHI_SwReset();
	}
}

// Common but have some differences
PUBLIC void APP_NwkHandler_vBdbInitSuccess(void)
{
	// For ZED, this Event happen only when it was not in the network
	BDB_teStatus eStatus;
	if (eNodeState == E_STARTUP)
	{
		eStatus = BDB_eNsStartNwkSteering();
		DBG_vPrintf(TRACE_APP_NWK_HANDLER, "NWK_HANDLER: Try Steering status %d \n",eStatus);
	}
}

PUBLIC void APP_NwkHandler_vHandleAfEvent(
    BDB_tsZpsAfEvent *psZpsAfEvent
)
{
	if ((PIR_OCCUPANCYSENSOR_ENDPOINT == psZpsAfEvent->u8EndPoint)
		||(PIR_LIGHTSENSOR_ENDPOINT == psZpsAfEvent->u8EndPoint))
	{
		APP_ZCL_vEventHandler(&psZpsAfEvent->sStackEvent);
	}
	else if (PIR_ZDO_ENDPOINT == psZpsAfEvent->u8EndPoint)
	{
		APP_ZdoEvents_vHandle(psZpsAfEvent);
	}

    /* Ensure Freeing of Apdus */
    if (ZPS_EVENT_APS_DATA_INDICATION == psZpsAfEvent->sStackEvent.eType) {
        PDUM_eAPduFreeAPduInstance(psZpsAfEvent->sStackEvent.uEvent.sApsDataIndEvent.hAPduInst);
    } else if (ZPS_EVENT_APS_INTERPAN_DATA_INDICATION == psZpsAfEvent->sStackEvent.eType) {
        PDUM_eAPduFreeAPduInstance(psZpsAfEvent->sStackEvent.uEvent.sApsInterPanDataIndEvent.hAPduInst);
    }
}

PUBLIC void APP_NwkHandler_vNetworkSteeringSuccess(void)
{
	ZTIMER_eStop(u8TimerPoll);

	eNodeState = E_RUNNING;
	PDM_eSaveRecordData(PDM_ID_NODE_STATE, &eNodeState, sizeof(teNodeState));

	// Post message to Main program
	APP_tsEvent sAppEvent;
	sAppEvent.eType = APP_E_EVENT_NWK_STEERING_SUCCESS;
	if(ZQ_bQueueSend(&APP_msgAppEvents, &sAppEvent) == FALSE)
	{
		DBG_vPrintf(TRACE_APP_NWK_HANDLER, "NWK_HANDLER: Failed to post Event %d \n", sAppEvent.eType);
	}
}
PUBLIC void APP_NwkHandler_vNetworkSteeringFailure(void)
{
	// Post message to Main program
	APP_tsEvent sAppEvent;
	sAppEvent.eType = APP_E_EVENT_NWK_STEERING_FAILURE;
	if(ZQ_bQueueSend(&APP_msgAppEvents, &sAppEvent) == FALSE)
	{
		DBG_vPrintf(TRACE_APP_NWK_HANDLER, "NWK_HANDLER: Failed to post Event %d \n", sAppEvent.eType);
	}
}
/*** For Router Device only ***/


/*** For End Device only ***/
PUBLIC void APP_NwkHandler_vJoinedAsEndDevice(void)
{
	DBG_vPrintf(TRACE_APP_NWK_HANDLER, "APP-NWK: Handle Join EndDevice !\n");
	//Create a persistent record of the EPID of the network
	ZPS_eAplAibSetApsUseExtendedPanId(ZPS_u64NwkNibGetEpid(ZPS_pvAplZdoGetNwkHandle()));
}
PUBLIC void APP_NwkHandler_vStartPollTimer(void)
{
    DBG_vPrintf(TRACE_APP_NWK_HANDLER, "APP_NWK: Start Poll Timer\n");
    if (ZTIMER_eStart(u8TimerPoll, POLL_TIME_FAST) != E_ZTIMER_OK) {
        DBG_vPrintf(TRACE_APP_NWK_HANDLER,
                    "APP-NWK: Failed to Poll Tick Timer\n");
    }
}

PUBLIC void APP_NwkHandler_cbTimerPoll(void *pvParam)
{
    uint8 u8Status = ZPS_eAplZdoPoll();
    (void) u8Status;
//    DBG_vPrintf(TRACE_APP_NWK_HANDLER,
//                "APP_NWK: APP_cbTimerPoll status %d \n",
//                u8Status);
    ZTIMER_eStop(u8TimerPoll);
    if(ZTIMER_eStart(u8TimerPoll, POLL_TIME_FAST) != E_ZTIMER_OK)
    {
        DBG_vPrintf(TRACE_APP_NWK_HANDLER, "APP_NWK: Failed to Poll Tick Timer\n");
    }
}

PUBLIC void APP_NwkHandler_vPollResponse(
    ZPS_tsAfEvent* psStackEvent
) {
//	  DBG_vPrintf(TRACE_APP_NWK_HANDLER, "APP-NWK: Event Handler Poll Response\n");
//    static uint32_t u32PollCount = 0;
//    DBG_vPrintf(TRACE_APP_NWK_HANDLER,
//                "APP-NWK (0x%04x): Network Poll Confirm 0x%02x (%d)\n",
//                ZPS_u16AplZdoGetNwkAddr(),
//                psStackEvent->uEvent.sNwkPollConfirmEvent.u8Status,
//                u32PollCount++);

    switch (psStackEvent->uEvent.sNwkPollConfirmEvent.u8Status) {
    case MAC_ENUM_SUCCESS:
    case MAC_ENUM_NO_ACK:
        ZPS_eAplZdoPoll();
        break;

    case MAC_ENUM_NO_DATA:
    default:
        break;
    }
}

PUBLIC void APP_NwkHandler_vRejoinSuccess(void)
{
	APP_REPORT_bReportDeviceInfor();
}

PUBLIC void APP_NwkHandler_vRejoinFailure(void)
{

}

// Utilities
PUBLIC void APP_NwkHandler_vPrintAPSTable(void)
{
	uint8 i,j;

	ZPS_tsAplAib * tsAplAib;

	tsAplAib = ZPS_psAplAibGetAib();

	for ( i = 0 ; i < (tsAplAib->psAplDeviceKeyPairTable->u16SizeOfKeyDescriptorTable + 1) ; i++ )
	{
		DBG_vPrintf(TRACE_APP_NWK_HANDLER, "APP_NWK_HANDLER: MAC: %016llx Key: ",
				ZPS_u64NwkNibGetMappedIeeeAddr(ZPS_pvAplZdoGetNwkHandle(),
						tsAplAib->psAplDeviceKeyPairTable->psAplApsKeyDescriptorEntry[i].u16ExtAddrLkup));
		for(j=0; j<16;j++)
		{
			DBG_vPrintf(TRACE_APP_NWK_HANDLER, "%02x ", tsAplAib->psAplDeviceKeyPairTable->psAplApsKeyDescriptorEntry[i].au8LinkKey[j]);
		}
		DBG_vPrintf(TRACE_APP_NWK_HANDLER, "\n");
		DBG_vPrintf(TRACE_APP_NWK_HANDLER, "APP_NWK_HANDLER: Incoming FC: %d\n", tsAplAib->pu32IncomingFrameCounter[i]);
		DBG_vPrintf(TRACE_APP_NWK_HANDLER, "APP_NWK_HANDLER: Outgoing FC: %d\n",
				tsAplAib->psAplDeviceKeyPairTable->psAplApsKeyDescriptorEntry[i].u32OutgoingFrameCounter);
	}
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
