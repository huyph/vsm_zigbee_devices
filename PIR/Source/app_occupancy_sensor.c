/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_occupancy_sensor.c
 *
 * DESCRIPTION:        Describe endpoint occupancy sensor
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include <string.h>
#include "pdum_gen.h"
#include "zps_gen.h"
#include "zcl_options.h"
#include "app_occupancy_sensor.h"
#include "dbg.h"
#include "drv_occupancy_sensor.h"
#include "app_main.h"
#include "app_events.h"
#include "app_reporting.h"
#include "app_node.h"
#include "ZTimer.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_APP_OCCUPANCY_SENSOR
	#define TRACE_APP_OCCUPANCY_SENSOR                     (FALSE)
#else /* DEBUG_APP_OCCUPANCY_SENSOR */
	#define TRACE_APP_OCCUPANCY_SENSOR                     (TRUE)
#endif /* DEBUG_APP_OCCUPANCY_SENSOR */

#define DELAY_TO_ENABLE_OCCUPANCY_SENSOR					(10000) //ms
#define PIR_OCCUPIED_TO_UNOCCUPIED_DELAY           			(5)// s
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef struct
{
	uint16_t u16OccupiedToUnoccupiedDelay;
}APP_OCCUPANCY_SENSOR_tsSaveParams;
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vCallbackDelayToEnableSensor(void* pvParams);
PRIVATE void vCallbackOccupancySensor(uint8_t u8SensorState);
PRIVATE PDM_teStatus eRestoreRecordedSensorParams(void);
PRIVATE PDM_teStatus eSaveSensorParams(void);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
PUBLIC tsZLO_OccupancySensorDevice  s_OccupancySensor;
extern PUBLIC tszQueue APP_msgAppEvents;
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE const APP_OCCUPANCY_SENSOR_tsSaveParams	sDefaultParams = {
		PIR_OCCUPIED_TO_UNOCCUPIED_DELAY
};
PRIVATE APP_OCCUPANCY_SENSOR_tsSaveParams	sSaveParams;
PRIVATE uint8_t			u8TimerDelayToEnableSensor;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC teZCL_Status APP_OCCUPANCY_SENSOR_eZCLRegisterEndpoint(
    tfpZCL_ZCLCallBackFunction fptr
)
{
    return  eZLO_RegisterOccupancySensorEndPoint(
                                PIR_OCCUPANCYSENSOR_ENDPOINT,
                                fptr,
                                &s_OccupancySensor);
}

PUBLIC void APP_OCCUPANCY_SENSOR_vZCLDeviceSpecificInit(void)
{
    /* Init Occupancy Sensor Endpoint */
    memcpy(s_OccupancySensor.sBasicServerCluster.au8ManufacturerName, BAS_MANUF_NAME_STRING, CLD_BAS_MANUF_NAME_SIZE);
    memcpy(s_OccupancySensor.sBasicServerCluster.au8ModelIdentifier,  BAS_MODEL_ID_STRING,   CLD_BAS_MODEL_ID_SIZE);
    memcpy(s_OccupancySensor.sBasicServerCluster.au8DateCode,         BAS_DATE_STRING,       CLD_BAS_DATE_SIZE);
    memcpy(s_OccupancySensor.sBasicServerCluster.au8SWBuildID,        BAS_SW_BUILD_STRING,   CLD_BAS_SW_BUILD_SIZE);
    s_OccupancySensor.sBasicServerCluster.eGenericDeviceType = E_CLD_BAS_GENERIC_DEVICE_TYPE_MOTION_OR_LIGHT_SENSOR;

    /* Initialise the strings in Occupancy Cluster */
    s_OccupancySensor.sOccupancySensingServerCluster.eOccupancySensorType = E_CLD_OS_SENSORT_TYPE_PIR;
    s_OccupancySensor.sOccupancySensingServerCluster.u8Occupancy = 0;
    s_OccupancySensor.sOccupancySensingServerCluster.u16PIROccupiedToUnoccupiedDelay = PIR_OCCUPIED_TO_UNOCCUPIED_DELAY;
}

PUBLIC void APP_OCCUPANCY_SENSOR_vInitialize(void)
{
	// Restore sensor parameters
	if(eNodeState == E_STARTUP)
	{
		// Restore default setting of occupancy sensor
		sSaveParams = sDefaultParams;
		eSaveSensorParams();
	}
	else
	{
		eRestoreRecordedSensorParams();
	}

	DRV_OCCUPANCY_SENSOR_vInitialize(vCallbackOccupancySensor,
			sSaveParams.u16OccupiedToUnoccupiedDelay
	);
	// Disable occupancy sensor
	DRV_OCCUPANCY_SENSOR_vDisable();

	(void) ZTIMER_eOpen(&u8TimerDelayToEnableSensor,
							vCallbackDelayToEnableSensor,
							NULL,
							ZTIMER_FLAG_PREVENT_SLEEP);
	ZTIMER_eStart(u8TimerDelayToEnableSensor, DELAY_TO_ENABLE_OCCUPANCY_SENSOR);
}
PUBLIC void APP_OCCUPANCY_SENSOR_vReportState(void)
{
	APP_REPORT_eReportAttribute(PIR_OCCUPANCYSENSOR_ENDPOINT,
			MEASUREMENT_AND_SENSING_CLUSTER_ID_OCCUPANCY_SENSING,
			E_CLD_OS_ATTR_ID_OCCUPANCY);
}

PUBLIC void APP_OCCUPANCY_SENSOR_vTimeOccupiedToUnoccupiedChange(void)
{
	DRV_OCCUPANCY_SENSOR_vSetTimeOccupiedToUnoccupiedDelay(
			s_OccupancySensor.sOccupancySensingServerCluster.u16PIROccupiedToUnoccupiedDelay);

	// Save parameters
	sSaveParams.u16OccupiedToUnoccupiedDelay = s_OccupancySensor.sOccupancySensingServerCluster.u16PIROccupiedToUnoccupiedDelay;
	eSaveSensorParams();
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vCallbackDelayToEnableSensor(void* pvParams)
{
	DBG_vPrintf(TRACE_APP_OCCUPANCY_SENSOR, "APP_OCCUPANCY_SENSOR: Enable ! \n");
	ZTIMER_eStop(u8TimerDelayToEnableSensor);
	DRV_OCCUPANCY_SENSOR_vEnable();
}

PRIVATE void vCallbackOccupancySensor(uint8_t u8SensorState)
{
	DBG_vPrintf(TRACE_APP_OCCUPANCY_SENSOR, "APP_OCCUPANCY_SENSOR: Sensor state 0x%2x \n", u8SensorState);
	s_OccupancySensor.sOccupancySensingServerCluster.u8Occupancy = u8SensorState;
	// Post message to main program to report
	APP_tsEvent sAppEvent;
	sAppEvent.eType = APP_E_EVENT_OCCUPANCY_SENSOR_REPORT_STATUS;
	if(ZQ_bQueueSend(&APP_msgAppEvents, &sAppEvent) == FALSE)
	{
		DBG_vPrintf(TRACE_APP_OCCUPANCY_SENSOR, "APP_OCCUPANCY_SENSOR: Failed to post Event %d \n", sAppEvent.eType);
	}
}

PRIVATE PDM_teStatus eRestoreRecordedSensorParams(void)
{
	 /* Restore any parameters data that is previously saved to flash */
	    uint16 u16ByteRead;
	    PDM_teStatus eStatusRestoreParas = PDM_eReadDataFromRecord(PDM_ID_PIR_OCCUPANCY_SENSOR_PARAMETERS,
	                                                               &sSaveParams,
	                                                              sizeof(APP_OCCUPANCY_SENSOR_tsSaveParams),
	                                                              &u16ByteRead);
	DBG_vPrintf(TRACE_APP_OCCUPANCY_SENSOR,
			"APP_OCCUPANCY_SENSOR: Load parameter of occupancy sensor status: %d \n",
			eStatusRestoreParas);

	    return  (eStatusRestoreParas);
}

PRIVATE PDM_teStatus eSaveSensorParams(void)
{
    PDM_teStatus eStatusSaveParas =   PDM_eSaveRecordData(PDM_ID_PIR_OCCUPANCY_SENSOR_PARAMETERS,
                                                                &sSaveParams,
                                                                sizeof(APP_OCCUPANCY_SENSOR_tsSaveParams));
	DBG_vPrintf(TRACE_APP_OCCUPANCY_SENSOR,
			"APP_OCCUPANCY_SENSOR: Save parameter of occupancy sensor status: %d \n",
			eStatusSaveParas);

    return eStatusSaveParas;
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
