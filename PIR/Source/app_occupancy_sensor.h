/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_occupancy_sensor.h
 *
 * DESCRIPTION:        Describe endpoint occupancy sensor
 *
 ***************************************************************************/
#ifndef APP_OCCUPANCY_SENSOR_H_
#define APP_OCCUPANCY_SENSOR_H_
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "jendefs.h"
#include "occupancy_sensor.h"
#include "PDM.h"
#include "PDM_IDs.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef APP_OCCUPANCY_SENSOR_ZTIMER_STORAGE
	#define APP_OCCUPANCY_SENSOR_ZTIMER_STORAGE     (1)
#endif /* APP_OCCUPANCY_SENSOR_ZTIMER_STORAGE */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC teZCL_Status APP_OCCUPANCY_SENSOR_eZCLRegisterEndpoint(
    tfpZCL_ZCLCallBackFunction fptr
);
PUBLIC void APP_OCCUPANCY_SENSOR_vZCLDeviceSpecificInit(void);
PUBLIC void APP_OCCUPANCY_SENSOR_vInitialize(void);
PUBLIC void APP_OCCUPANCY_SENSOR_vReportState(void);
PUBLIC void APP_OCCUPANCY_SENSOR_vTimeOccupiedToUnoccupiedChange(void);
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/
extern PUBLIC tsZLO_OccupancySensorDevice s_OccupancySensor;
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_OCCUPANCY_SENSOR_H_*/
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
