/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_coil.c
 *
 * DESCRIPTION:        Control relay coil
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "drv_coil.h"
#include "drv_coil_dio.h"
#ifdef NORMAL_SWITCH_DEVICE
#include "drv_coil_mux.h"
#endif	/* NORMAL_SWITCH_DEVICE */
#include "dbg.h"
#include "delay.h"
#include "zero_detect.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_DRV_COIL
    #define TRACE_DRV_COIL         (TRUE)
#else   /* DEBUG_DRV_COIL */
    #define TRACE_DRV_COIL         (FALSE)
#endif  /* DEBUG_DRV_COIL */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void TurnOnCoil(
	uint8_t 	u8CoilNumber
);
PRIVATE void TurnOffCoil(
	uint8_t 	u8CoilNumber
);
PRIVATE void vCallbackWhenZeroDetected(void *pvParmas);
PRIVATE void vCallbackDelayTurnOnCoil(void *pvParams);
PRIVATE void vCallbackDelayTurnOffCoil(void *pvParams);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE bool_t		bUsingMux = FALSE;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void DRV_COIL_vInitialize(
	bool_t			bInitMux
)
{
	DBG_vPrintf(TRACE_DRV_COIL, "DRV_COIL: DRV_COIL_vInitialize bUsingMux = %d \n",bInitMux);
	bUsingMux = bInitMux;
	if(bInitMux)
	{
#ifdef NORMAL_SWITCH_DEVICE
		DRV_COIL_MUX_vInitialize();
#endif	/* NORMAL_SWITCH_DEVICE */
	}
	else
	{
		DRV_COIL_DIO_vInitialize();
	}
	ZERO_DETECT_vInitialize();
}

PUBLIC	void DRV_COIL_vOn(
	DRV_COIL_tsCoil_t		*sCoil
)
{
	DBG_vPrintf(TRACE_DRV_COIL, "DRV_COIL: Start turn on coil: %d \n",sCoil->u8CoilNumber);
	// Enable Zero Detect
	ZERO_DETECT_vDisable();
	ZERO_DETECT_vEnable(vCallbackWhenZeroDetected, sCoil);
}

PUBLIC	void DRV_COIL_vOff(
	DRV_COIL_tsCoil_t		*sCoil
)
{
	DBG_vPrintf(TRACE_DRV_COIL, "DRV_COIL: Start turn off coil: %d \n",sCoil->u8CoilNumber);

	TurnOffCoil(sCoil->u8CoilNumber);
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void TurnOnCoil(
	uint8_t 	u8CoilNumber
)
{
	if(bUsingMux)
	{
#ifdef NORMAL_SWITCH_DEVICE
		DRV_COIL_MUX_vSet(u8CoilNumber, TRUE);
#endif	/* NORMAL_SWITCH_DEVICE */
	}
	else
	{
		DRV_COIL_DIO_vSet(u8CoilNumber, TRUE);
	}
}

PRIVATE void TurnOffCoil(
	uint8_t	 	u8CoilNumber
)
{
	if(bUsingMux)
	{
#ifdef NORMAL_SWITCH_DEVICE
		DRV_COIL_MUX_vSet(u8CoilNumber, FALSE);
#endif	/* NORMAL_SWITCH_DEVICE */
	}
	else
	{
		DRV_COIL_DIO_vSet(u8CoilNumber, FALSE);
	}
}

PRIVATE void vCallbackWhenZeroDetected(void *pvParams)
{
	if(pvParams == NULL)	return;
	DRV_COIL_tsCoil_t *sCoil = (DRV_COIL_tsCoil_t*)pvParams;
	DBG_vPrintf(TRACE_DRV_COIL, "DRV_COIL: vCallbackWhenZeroDetected() coil %d \n", sCoil->u8CoilNumber);
	// Delay to Turn on relay
	if(sCoil->u16DelayToOn > 0)
	{
		DELAY_vUseHWtimerStart(&sCoil->u8TimerDelayToOn, sCoil->u16DelayToOn,  vCallbackDelayTurnOnCoil, sCoil);
	}
	else
	{
		vCallbackDelayTurnOnCoil(sCoil);
	}
}

PRIVATE void vCallbackDelayTurnOnCoil(void *pvParams)
{
	if(pvParams == NULL)	return;
	DRV_COIL_tsCoil_t *sCoil = (DRV_COIL_tsCoil_t*)pvParams;
	DBG_vPrintf(TRACE_DRV_COIL, "DRV_COIL: vCallbackDelayTurnOnCoil() coil: %d \n",sCoil->u8CoilNumber);
	// Turn on coil
	TurnOnCoil(sCoil->u8CoilNumber);
	// Delay to Turn off coil
	DELAY_vUseHWtimerStart(&sCoil->u8TimerPulseWidth, sCoil->u16PulseWidth,  vCallbackDelayTurnOffCoil, sCoil);
}

PRIVATE void vCallbackDelayTurnOffCoil(void *pvParams)
{
	if(pvParams == NULL)	return;
	DRV_COIL_tsCoil_t *sCoil = (DRV_COIL_tsCoil_t*)pvParams;
	DBG_vPrintf(TRACE_DRV_COIL, "DRV_COIL: vCallbackDelayTurnOffCoil() coil: %d \n",sCoil->u8CoilNumber);
	// Turn off coil
	TurnOffCoil(sCoil->u8CoilNumber);
	// call callback function
	if(sCoil->fnCallback != NULL)
	{
		sCoil->fnCallback(sCoil->pvParams);
	}
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
