/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_motor.c
 *
 * DESCRIPTION:        Control motor
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "drv_motor.h"
#include "AppHardwareApi_JN516x.h"
#include "dbg.h"
#include "ZTimer.h"
#include "app_common.h"
#include "my_define.h"
#include "hw_timer.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_DRV_MOTOR
    #define TRACE_DRV_MOTOR              (TRUE)
#else   /* DEBUG_DRV_MOTOR */
    #define TRACE_DRV_MOTOR              (FALSE)
#endif  /* DEBUG_DRV_MOTOR */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void 	vCallbackTimerRunMotor(void *pvParams);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE DRV_MOTOR_tsParams_t	sMotorParams;
PRIVATE uint16_t	u16Hi = 0;
PRIVATE uint16_t	u16Lo = MOTOR_PWM_CYCLE;
PRIVATE bool_t		bMotorInit = FALSE;
PRIVATE uint8_t		u8TimerRunMotor;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void DRV_MOTOR_vInitialize(
		uint8_t		u8VibrationLevel				//0-100%
)
{
	if(u8VibrationLevel > 100)
	{
		sMotorParams.u8VibrationLevel = 100;
	}
	else
	{
		sMotorParams.u8VibrationLevel = u8VibrationLevel;
	}
	u16Lo = MOTOR_PWM_CYCLE;
	u16Hi = u16Lo - (sMotorParams.u8VibrationLevel*MOTOR_PWM_CYCLE)/100;

	// Turn off output
	vAHI_DioSetDirection(0, 1UL << PIN_OUT_PWM_MOTOR);
	vAHI_DioSetOutput(0, 1UL << PIN_OUT_PWM_MOTOR);

	// Initialize timer run motor
	ZTIMER_eOpen(&u8TimerRunMotor, vCallbackTimerRunMotor,  NULL, ZTIMER_FLAG_PREVENT_SLEEP);

	// Enable pwm to control motor
	HW_TIMER_bEnable(
			HW_TIMER_MOTOR_CONTROL,
			FALSE,
			E_HW_TIMER_PERIOD_RANGE_64US_TO_4S,
			FALSE,
			FALSE,
			NULL,
			NULL,
			E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_11_12_13_17,
			0
	);
	bMotorInit = TRUE;
}

PUBLIC void DRV_MOTOR_vSetVibrationLevel(
		uint8_t		u8VibrationLevel				//0-100%
)
{
	sMotorParams.u8VibrationLevel = u8VibrationLevel;
	if(sMotorParams.u8VibrationLevel > 100)
	{
		sMotorParams.u8VibrationLevel = 100;

	}
	u16Lo = MOTOR_PWM_CYCLE;
	u16Hi = u16Lo - (sMotorParams.u8VibrationLevel*MOTOR_PWM_CYCLE)/100;

	DBG_vPrintf(TRACE_DRV_MOTOR,
			"DRV_MOTOR: Set vibration level: %d, Cycle = %d, Duty = %d \n",
			u8VibrationLevel, u16Lo, u16Lo - u16Hi);
}

PUBLIC void DRV_MOTOR_vRun(
		uint16_t 			u16TimeOn,
		fnCbAfterRunMotor	fnCallback,
		void				*pvParams
)
{
	DBG_vPrintf(TRACE_DRV_MOTOR,"DRV_MOTOR: Run vibration motor in %d ms \n", u16TimeOn);
	if(!bMotorInit)
	{
		DBG_vPrintf(TRACE_DRV_MOTOR,"DRV_MOTOR: Please Initialize motor first \n");
		return;
	}

	sMotorParams.fnCallback	= fnCallback;
	sMotorParams.pvParams 	= pvParams;

	DRV_MOTOR_vStop();

	// Run motor
	HW_TIMER_vStartRepeat(HW_TIMER_MOTOR_CONTROL, u16Hi, u16Lo);

	// Start Timer run motor
	if(u16TimeOn > 0)
	{
		ZTIMER_eStart(u8TimerRunMotor, u16TimeOn);
	}
}

PUBLIC void DRV_MOTOR_vStop(void)
{
	if(E_ZTIMER_STATE_RUNNING == ZTIMER_eGetState(u8TimerRunMotor))
	{
		ZTIMER_eStop(u8TimerRunMotor);
	}
	HW_TIMER_vStop(HW_TIMER_MOTOR_CONTROL);
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void 	vCallbackTimerRunMotor(void *pvParams)
{
	DBG_vPrintf(TRACE_DRV_MOTOR,"DRV_MOTOR: Callback timer run motor \n");

	DRV_MOTOR_vStop();

	if(sMotorParams.fnCallback != NULL)
	{
		sMotorParams.fnCallback(sMotorParams.pvParams);
	}
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/


