/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_mux.h
 *
 * DESCRIPTION:        Multiplexer 3in 8Out
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "drv_mux.h"
#include "my_define.h"
#include "AppHardwareApi_JN516x.h"
#include "dbg.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_DRV_MUX
    #define TRACE_DRV_MUX            FALSE
#else
    #define TRACE_DRV_MUX            TRUE
#endif

#define MUX_PIN_OE_MASK			(1UL << PIN_OUT_OE_MULTIPLEXER)
#define MUX_PIN_S2_MASK			(1UL << PIN_OUT_S2_MULTIPLEXER)
#define MUX_PIN_S1_MASK			(1UL << PIN_OUT_S1_MULTIPLEXER)
#define MUX_PIN_S0_MASK			(1UL << PIN_OUT_S0_MULTIPLEXER)

#define MUX_NUMBER_PIN			(4)
#define MUX_NUMBER_CHANEL		(8)
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vDioSetOutput(uint32_t u32On, uint32_t u32Off);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE uint8_t	u8ArrPinMux[MUX_NUMBER_PIN] = {
		PIN_OUT_OE_MULTIPLEXER,
		PIN_OUT_S2_MULTIPLEXER,
		PIN_OUT_S1_MULTIPLEXER,
		PIN_OUT_S0_MULTIPLEXER
};
PRIVATE uint32_t	u32MuxChanelMaskOn[MUX_NUMBER_CHANEL] = {
		/*#1*/ (0UL)											  ,
		/*#2*/ 								       MUX_PIN_S0_MASK,
		/*#3*/ 					 MUX_PIN_S1_MASK				  ,
		/*#4*/ 					 MUX_PIN_S1_MASK | MUX_PIN_S0_MASK,
		/*#5*/ MUX_PIN_S2_MASK									  ,
		/*#6*/ MUX_PIN_S2_MASK 					 | MUX_PIN_S0_MASK,
		/*#7*/ MUX_PIN_S2_MASK | MUX_PIN_S1_MASK				  ,
		/*#8*/ MUX_PIN_S2_MASK | MUX_PIN_S1_MASK | MUX_PIN_S0_MASK,
};
PRIVATE uint32_t	u32MuxChanelMaskOff[MUX_NUMBER_CHANEL] = {
		/*#1*/ MUX_PIN_S2_MASK | MUX_PIN_S1_MASK | MUX_PIN_S0_MASK,
		/*#2*/ MUX_PIN_S2_MASK | MUX_PIN_S1_MASK				  ,
		/*#3*/ MUX_PIN_S2_MASK 					 | MUX_PIN_S0_MASK,
		/*#4*/ MUX_PIN_S2_MASK									  ,
		/*#5*/ 					 MUX_PIN_S1_MASK | MUX_PIN_S0_MASK,
		/*#6*/ 					 MUX_PIN_S1_MASK				  ,
		/*#7*/ 		 							   MUX_PIN_S0_MASK,
		/*#8*/ (0UL)											  ,
};
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void DRV_MUX_vInitialize(void)
{
	uint8_t	i;
	// Set pin OE,S2,S1,S0 as output
	for(i = 0; i < MUX_NUMBER_PIN; i++)
	{
		if((u8ArrPinMux[i] == DO_0) || (u8ArrPinMux[i] == DO_1))
		{
			bAHI_DoEnableOutputs(TRUE);
		}
		else
		{
			vAHI_DioSetDirection(0, 1UL << u8ArrPinMux[i]);
		}
	}
}

PUBLIC void DRV_MUX_vSet(
		uint8_t		u8Channel,
		bool_t		bOnOff
)
{
	if(u8Channel >= MUX_NUMBER_CHANEL)
		return;
	if(bOnOff)
	{
		// Disable output
		vDioSetOutput(MUX_PIN_OE_MASK, 0UL);
		// Set output buffer
		vDioSetOutput(u32MuxChanelMaskOn[u8Channel], u32MuxChanelMaskOff[u8Channel]);
		// Enable Output
		vDioSetOutput(0UL, MUX_PIN_OE_MASK);
	}
	else
	{
		// Disable output
		vDioSetOutput(MUX_PIN_OE_MASK, 0UL);
	}
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vDioSetOutput(uint32_t u32On, uint32_t u32Off)
{
	vAHI_DioSetOutput(u32On, u32Off);
	// Turn on_off DO_0,DO_1
	vAHI_DoSetDataOut(u32On >> DO_0, u32Off >> DO_0);
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
