/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_events.h
 *
 * DESCRIPTION:        Base Device application - generic event definitions
 *
 ****************************************************************************
 * * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5168, JN5179].
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2016. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_GENERIC_EVENTS_H_
#define APP_GENERIC_EVENTS_H_

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/


/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef enum
{
    APP_E_EVENT_NONE = 0,
    /* Network Event */
    APP_E_EVENT_NWK_STEERING_SUCCESS,
    APP_E_EVENT_NWK_STEERING_FAILURE,
    APP_E_EVENT_ZCL_APS_ACK_FAIL,
    /* function button event */
    APP_E_EVENT_FUNC_BUTTON_UP_SHORTER_5S,
    APP_E_EVENT_FUNC_BUTTON_HOLD_5S,
    /**** For SWITCH only ****/
    /* Relay event */
    APP_E_EVENT_RELAY_REPORT_STATUS,
    /* Cap sense event */
    APP_E_EVENT_CAP_SENSE_PRESSED,
    /* Event from mobile app (coordinator) */
    APP_E_EVENT_COORDINATOR_ON_OFF_CMD,

    /**** For PIR only ****/
    APP_E_EVENT_OCCUPANCY_SENSOR_REPORT_STATUS,
    APP_E_EVENT_LIGHT_SENSOR_REPORT_STATUS,

    APP_E_EVENT_MAX
} APP_teEventType;

typedef struct
{
    uint8 u8Button;
    uint32 u32DIOState;
} APP_tsEventButton;

typedef struct
{
	uint8	u8Switch;
	bool	bStatus;
}APP_tsEventSwitch;

typedef struct
{
	uint8_t		u8EndPoint;
	uint16_t	u16ClusterID;
}APP_tsACKEvent;
typedef struct
{
    APP_teEventType eType;
    union
    {
        APP_tsEventButton                   sButton;
        APP_tsEventSwitch					sSwitch;
        APP_tsACKEvent						sACKEvent;
    }uEvent;
} APP_tsEvent;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_GENERIC_EVENTS_H_*/
