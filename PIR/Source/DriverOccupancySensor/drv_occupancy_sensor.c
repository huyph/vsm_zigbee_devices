/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_occupancy_sensor.c
 *
 * DESCRIPTION:
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "drv_occupancy_sensor.h"
#include "my_define.h"
#include "hw_isr.h"
#include "dbg.h"
#include "AppHardwareApi_JN516x.h"
//#include "app_led.h"
#include "ZTimer.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_DRV_OCCUPANCY_SENSOR
    #define TRACE_DRV_OCCUPANCY_SENSOR   TRUE
#else
    #define TRACE_DRV_OCCUPANCY_SENSOR   FALSE
#endif

#define TIME_DEBOUNCE_INPUT				(2)	//ms

#define OCCUPANCY_SENSOR_DIO_MASK		(1UL << PIN_IN_OCCUPANCY_SENOR)
#define APP_BUTTON_SAMPLE_MASK          (0x1f)

#define OCCUPIED_STATE		(0x01)
#define UNOCCUPIED_STATE	(0x00)

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef struct
{
	uint8_t 				u8OccupancyState;
	uint32_t				u32OccupiedToUnoccupiedDelay;
	uint8_t					u8OccupiedToUnoccupiedTimer;
	drvOccupancySensorCb	s_fnCallback;
}DRV_OCCUPANCY_SENSOR_tsParams;
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vOccupancySenor_ISR(void *pvParams);
PRIVATE void vCallbackTimerDebounceInput(void *pvParams);
PRIVATE void vHandleRisingEdgeEvent(void);
PRIVATE void vHandleFallingEdgeEvent(void);
PRIVATE void vCallbackOccupiedToUnoccupiedTimer(void* pvPrams);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE uint8_t	u8IntCbIndex;
uint8 s_u8ButtonDebounce                = APP_BUTTON_SAMPLE_MASK;
PRIVATE uint32 u32PreviousDioState      = OCCUPANCY_SENSOR_DIO_MASK;
PRIVATE const uint32 s_u32ButtonDIOLine = OCCUPANCY_SENSOR_DIO_MASK;

PRIVATE DRV_OCCUPANCY_SENSOR_tsParams	sSensorParams;
PRIVATE uint8_t							u8TimerDebounceInput;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void DRV_OCCUPANCY_SENSOR_vInitialize(
		drvOccupancySensorCb 	fptr,
		uint16_t				u32OccupiedToUnoccupiedDelay_s		//second
)
{
	// Set Occupancy sensor pin as input
	vAHI_DioSetDirection(OCCUPANCY_SENSOR_DIO_MASK, 0);

	vAHI_DioSetPullup(0, OCCUPANCY_SENSOR_DIO_MASK);

	// Set interrupt on rising edge
	vAHI_DioWakeEdge(OCCUPANCY_SENSOR_DIO_MASK, 0);
	// Enable interrupt
	vAHI_DioWakeEnable(OCCUPANCY_SENSOR_DIO_MASK, 0);

	// Register callback when interrupt happen
	HW_ISR_vRegisterCallback(&u8IntCbIndex,
			OCCUPANCY_SENSOR_DIO_MASK,
			vOccupancySenor_ISR,
			NULL
	);

	sSensorParams.u8OccupancyState 				= UNOCCUPIED_STATE;
	sSensorParams.u32OccupiedToUnoccupiedDelay	= 1000*u32OccupiedToUnoccupiedDelay_s; //ms
	sSensorParams.s_fnCallback					= fptr;

	(void) ZTIMER_eOpen(&u8TimerDebounceInput,
							vCallbackTimerDebounceInput,
							NULL,
							ZTIMER_FLAG_PREVENT_SLEEP);
	(void) ZTIMER_eOpen(&sSensorParams.u8OccupiedToUnoccupiedTimer,
							vCallbackOccupiedToUnoccupiedTimer,
							NULL,
							ZTIMER_FLAG_PREVENT_SLEEP);
}

PUBLIC void DRV_OCCUPANCY_SENSOR_vEnable(void)
{
	HW_ISR_vEnable(u8IntCbIndex);
}

PUBLIC void DRV_OCCUPANCY_SENSOR_vDisable(void)
{
	HW_ISR_vDisable(u8IntCbIndex);
}

PUBLIC void DRV_OCCUPANCY_SENSOR_vSetTimeOccupiedToUnoccupiedDelay(
		uint16_t				u16TimeDelay_s		//second
)
{
	sSensorParams.u32OccupiedToUnoccupiedDelay	= 1000*u16TimeDelay_s; //ms
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vOccupancySenor_ISR(void *pvParams)
{
	/* Disable edge detection until scan complete */
	vAHI_DioInterruptEnable(0, OCCUPANCY_SENSOR_DIO_MASK);
	/* Begin debouncing the button press */
	ZTIMER_eStop(u8TimerDebounceInput);
    ZTIMER_eStart(u8TimerDebounceInput, TIME_DEBOUNCE_INPUT);
}

PRIVATE void vCallbackTimerDebounceInput(void *pvParams)
{
	ZTIMER_eStop(u8TimerDebounceInput);

	uint32 u32DioInput = 0;
	bool_t bButtonDebounceComplete = TRUE;

	/* Clear any existing interrupt pending flags */
	(void)u32AHI_DioInterruptStatus();

	u32DioInput = u32AHI_DioReadInput();

	/* Shift the previous debounce checks and add the new debounce reading*/
	s_u8ButtonDebounce <<= 1;
	s_u8ButtonDebounce |= (u32DioInput & s_u32ButtonDIOLine) ? TRUE : FALSE;
	s_u8ButtonDebounce &= APP_BUTTON_SAMPLE_MASK;


	/* If previously the button was down but now it is up, post an event to the queue */
	if (((u32PreviousDioState & s_u32ButtonDIOLine) == 0) && (s_u8ButtonDebounce == APP_BUTTON_SAMPLE_MASK))
	{
		/* Save the new state */
		u32PreviousDioState |= s_u32ButtonDIOLine;
//		DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR, "DRV_OCCUPANCY_SENSOR: Rising edge, Dio State = %08x \n", u32PreviousDioState);

		/*Call function to process falling edge event*/
		vHandleRisingEdgeEvent();
	}
	/* If previously the button was up but now it is down, post an event to the queue */
	else if (((u32PreviousDioState & s_u32ButtonDIOLine) != 0) && (s_u8ButtonDebounce == 0x0))
	{
		/* Save the new state */
		u32PreviousDioState &= ~s_u32ButtonDIOLine;
//		DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR, "DRV_OCCUPANCY_SENSOR: Falling edge, Dio State = %08x \n", u32PreviousDioState);

		/*Call function to process falling edge event*/
		vHandleFallingEdgeEvent();
	}
	/* Still debouncing this button, clear flag to indicate more samples are required */
	else if(((s_u8ButtonDebounce!= 0) && (s_u8ButtonDebounce != APP_BUTTON_SAMPLE_MASK)))
	{
		bButtonDebounceComplete &= FALSE;
	}

	/* If all buttons are in a stable state, stop the scan timer and set the new interrupt edge requirements */
	if(bButtonDebounceComplete == TRUE)
	{
		/* Set the new interrupt edge requirements */
		vAHI_DioWakeEdge((OCCUPANCY_SENSOR_DIO_MASK & ~u32PreviousDioState), (OCCUPANCY_SENSOR_DIO_MASK & u32PreviousDioState));

		/* Re enable DIO wake interrupts on all buttons */
		vAHI_DioInterruptEnable(OCCUPANCY_SENSOR_DIO_MASK, 0);

//		DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR,
//				"DRV_OCCUPANCY_SENSOR: Debounce complete, timer stopped, interrupts re-enabled, previous state %08x \n",
//				u32PreviousDioState);
//		DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR,
//				"DRV_OCCUPANCY_SENSOR: Wake edges: Rising=%08x Falling=%08x \n",
//				(OCCUPANCY_SENSOR_DIO_MASK & ~u32PreviousDioState),
//				(OCCUPANCY_SENSOR_DIO_MASK & u32PreviousDioState));
	}
	else
	{
//		DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR, "DRV_OCCUPANCY_SENSOR: Debounce in progress, timer continued \n");
		ZTIMER_eStart(u8TimerDebounceInput, TIME_DEBOUNCE_INPUT);
	}
}

PRIVATE void vHandleRisingEdgeEvent(void)
{
	DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR,"DRV_OCCUPANCY_SENSOR: Rising Edge Event \n");
//	APP_LED_vOn(-1);
	if (sSensorParams.u8OccupancyState == UNOCCUPIED_STATE)
	{
		/* We were previously unoccupied*/
		DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR,"DRV_OCCUPANCY_SENSOR: Occupied \n");
		sSensorParams.u8OccupancyState = OCCUPIED_STATE;

		if(NULL != sSensorParams.s_fnCallback)
		{
			sSensorParams.s_fnCallback(sSensorParams.u8OccupancyState);
		}
	}
	else
	{
		/* We are already occupied*/
		ZTIMER_eStop(sSensorParams.u8OccupiedToUnoccupiedTimer);
		DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR,"DRV_OCCUPANCY_SENSOR: Stopped Timer Occupied to Unoccupied \n");
	}
}

PRIVATE void vHandleFallingEdgeEvent(void)
{
	DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR,"DRV_OCCUPANCY_SENSOR: Falling Edge Event \n");

//	APP_LED_vOff();
	if (0 == sSensorParams.u32OccupiedToUnoccupiedDelay)
	{
		vCallbackOccupiedToUnoccupiedTimer(NULL);
	}
	else
	{
		ZTIMER_eStart(sSensorParams.u8OccupiedToUnoccupiedTimer, sSensorParams.u32OccupiedToUnoccupiedDelay);
		DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR,"DRV_OCCUPANCY_SENSOR: Start timer Occupied to Unoccupied \n");
	}
}

PRIVATE void vCallbackOccupiedToUnoccupiedTimer(void* pvPrams)
{
	DBG_vPrintf(TRACE_DRV_OCCUPANCY_SENSOR,"DRV_OCCUPANCY_SENSOR: Timer expired \n");
	sSensorParams.u8OccupancyState = UNOCCUPIED_STATE;
	ZTIMER_eStop(sSensorParams.u8OccupiedToUnoccupiedTimer);

    if(NULL != sSensorParams.s_fnCallback)
    {
    	sSensorParams.s_fnCallback(sSensorParams.u8OccupancyState);
    }
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
