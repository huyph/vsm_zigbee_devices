/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_reporting.c
 *
 * DESCRIPTION:        Base Device application - reporting functionality
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include <jendefs.h>
#include <ZQueue.h>
#include <ZTimer.h>
#include <dbg.h>
#include <AppHardwareApi.h>

#ifdef SLEEPY_ZED
#include <pwrm.h>
#include "app_events.h"
#endif /* SLEEPY_ZED */

#include "hw_isr.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_HW_ISR
#define TRACE_HW_ISR                (FALSE)
#else   /* DEBUG_HW_ISR */
#define TRACE_HW_ISR                (TRUE)
#endif  /* DEBUG_HW_ISR */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef struct {
	bool_t	bEnalbe;
    uint32_t u32flag;
    fnIsrCallback pfCallback;
    void *pParams;
} tsHwIsr_t;
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
#ifdef SLEEPY_ZED
extern tszQueue APP_msgAppEvents;
#endif /* SLEEPY_ZED */
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE tsHwIsr_t s_HwIsr[HW_ISR_NUM];
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void HW_ISR_vRegisterCallback(uint8_t *pu8Index, uint32_t u32Flag,
        fnIsrCallback fnCallback, void* pvParams) {
    uint8_t i;
    for (i = 0; i < HW_ISR_NUM; i++) {
        if (NULL == s_HwIsr[i].pfCallback) {
            *pu8Index = i;
            s_HwIsr[i].bEnalbe = TRUE;
            s_HwIsr[i].pfCallback = fnCallback;
            s_HwIsr[i].u32flag = u32Flag;
            s_HwIsr[i].pParams = pvParams;
            break;
        }
    }
}

PUBLIC void vISR_SystemController(void) {
    /* clear pending DIO changed bits by reading register */
#ifdef SLEEPY_ZED
//    uint8 u8WakeInt = u8AHI_WakeTimerFiredStatus();
//
//    if (u8WakeInt & E_AHI_WAKE_TIMER_MASK_0) {
//        APP_tsEvent sButtonEvent;
//        DBG_vPrintf(TRACE_HW_ISR, "HW_ISR: Wake Timer 0 Interrupt\n");
//        vAHI_WakeTimerStop(E_AHI_WAKE_TIMER_0);
//        sButtonEvent.eType = APP_E_EVENT_WAKE_TIMER;
//        ZQ_bQueueSend(&APP_msgAppEvents, &sButtonEvent);
//    }
#endif /* SLEEPY_ZED */

    uint8_t i;
    uint32_t u32IOStatus = u32AHI_DioInterruptStatus();
    DBG_vPrintf(TRACE_HW_ISR, "HW_ISR: Interrupt happened 0x%08x\n",
            u32IOStatus);

    for (i = 0; i < HW_ISR_NUM; i++)
    {
    	if(s_HwIsr[i].bEnalbe)
    	{
    		if ((NULL != s_HwIsr[i].pfCallback)
    				&& ((s_HwIsr[i].u32flag & u32IOStatus) != 0))
    		{
    			s_HwIsr[i].pfCallback(s_HwIsr[i].pParams);
    		}
    	}
    }

#ifdef SLEEPY_ZED
//    if (u8WakeInt & E_AHI_WAKE_TIMER_MASK_1) {
//        APP_tsEvent sButtonEvent;
//        DBG_vPrintf(TRACE_HW_ISR, "HW_ISR: Wake Timer 1 Interrupt\n");
//        PWRM_vWakeInterruptCallback();
//        sButtonEvent.eType = APP_E_EVENT_PERIODIC_REPORT;
//        ZQ_bQueueSend(&APP_msgAppEvents, &sButtonEvent);
//    }
#endif /* SLEEPY_ZED */
}


PUBLIC void HW_ISR_vDisable(
	uint8_t			u8Index
)
{
	if(HW_ISR_NUM > u8Index)
	{
		s_HwIsr[u8Index].bEnalbe = FALSE;
	}
}

PUBLIC void HW_ISR_vEnable(
	uint8_t			u8Index
)
{
	if(HW_ISR_NUM > u8Index)
	{
		s_HwIsr[u8Index].bEnalbe = TRUE;
	}
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
