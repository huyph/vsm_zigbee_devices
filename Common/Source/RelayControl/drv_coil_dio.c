/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_coil_dio.c
 *
 * DESCRIPTION:
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "drv_coil_dio.h"
#include "my_define.h"
#include "AppHardwareApi_JN516x.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef PIR_DEVICE
	#define MAX_NUMBER_OF_COIL			(2)
#else	/* NORMAL_SWITCH_DEVICE */
	#define MAX_NUMBER_OF_COIL			(6)
#endif	/* PIR_DEVICE */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vDioSetOutput(uint32_t u32On, uint32_t u32Off);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE uint8_t		u8ArrCoilPin[MAX_NUMBER_OF_COIL] = {
		PIN_OUT_COIL_RST_RELAY_SW1,
		PIN_OUT_COIL_SET_RELAY_SW1,
#ifdef NORMAL_SWITCH_DEVICE
		PIN_OUT_COIL_RST_RELAY_SW2,
		PIN_OUT_COIL_SET_RELAY_SW2,
		PIN_OUT_COIL_RST_RELAY_SW3,
		PIN_OUT_COIL_SET_RELAY_SW3
#endif	/* NORMAL_SWITCH_DEVICE */
};
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void DRV_COIL_DIO_vInitialize(void)
{
	uint8_t	i;
	// Set pin coil as output
	for(i = 0; i < MAX_NUMBER_OF_COIL; i++)
	{
		if((u8ArrCoilPin[i] == DO_0) || (u8ArrCoilPin[i] == DO_1))
		{
			bAHI_DoEnableOutputs(TRUE);
		}
		else
		{
			vAHI_DioSetDirection(0, 1UL << u8ArrCoilPin[i]);
		}
	}
}

PUBLIC void DRV_COIL_DIO_vSet(
		uint8_t u8CoilNumber,
		bool_t	bOnOff
)
{
	if(u8CoilNumber >= MAX_NUMBER_OF_COIL)
		return;
	if(bOnOff)
	{
		vDioSetOutput(1UL << u8ArrCoilPin[u8CoilNumber], 0UL);
	}
	else
	{
		vDioSetOutput(0UL, 1UL << u8ArrCoilPin[u8CoilNumber]);
	}
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vDioSetOutput(uint32_t u32On, uint32_t u32Off)
{
	vAHI_DioSetOutput(u32On, u32Off);
	// Turn on_off DO_0,DO_1
	vAHI_DoSetDataOut(u32On >> DO_0, u32Off >> DO_0);
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
