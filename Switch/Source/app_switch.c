/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_reporting.c
 *
 * DESCRIPTION:        Base Device application - reporting functionality
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "app_switch.h"
#include "zps_gen.h"
#include "app_reporting.h"
#include "AppHardwareApi.h"
#include "PDM.h"
#include "PDM_IDs.h"
#include "my_define.h"
#include "dbg.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_APP_SWITCH
    #define TRACE_APP_SWITCH         (TRUE)
#else   /* DEBUG_APP_SWITCH */
    #define TRACE_APP_SWITCH          (FALSE)
#endif  /* DEBUG_APP_SWITCH */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vCheckNumberOfSwitch(void);
PRIVATE void vReadRecordedStatusOfSwitch(void);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
PUBLIC uint8_t u8NumberOfSwitch = 1;
PUBLIC uint8_t u8ArrSwitchEndpoint[MAX_NUMBER_OF_SWITCH] = {
																SWITCH_SW1_ENDPOINT,
																SWITCH_SW2_ENDPOINT,
																SWITCH_SW3_ENDPOINT,
																SWITCH_SW4_ENDPOINT,
															};
PUBLIC tsZLO_OnOffLightDevice sOnOffLightDevice[MAX_NUMBER_OF_SWITCH];
PUBLIC APP_eSwitch	eArrSwitch[MAX_NUMBER_OF_SWITCH] = {
		APP_E_SWITCH_1,
		APP_E_SWITCH_2,
		APP_E_SWITCH_3,
		APP_E_SWITCH_4,
};
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_SWITCH_vInitialize(void)
{
	vCheckNumberOfSwitch();
//	vReadRecordedStatusOfSwitch();
}

PUBLIC void APP_SWITCH_vZCLDeviceSpecificInit(void)
{
	uint8 i;
	for(i = 0; i < u8NumberOfSwitch; i++)
	{
		memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ManufacturerName, "VSM", CLD_BAS_MANUF_NAME_SIZE);

		if(u8NumberOfSwitch == SWITCH_1_GANG)
		{
#ifdef SCENE_SWITCH_DEVICE
			memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ModelIdentifier, "HS-SWS100ZB-VNM", CLD_BAS_MODEL_ID_SIZE);
#else
			memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ModelIdentifier, "HS-SWL100ZB-VNM", CLD_BAS_MODEL_ID_SIZE);
#endif /*SCENE_SWITCH_DEVICE*/
		}
		else if(u8NumberOfSwitch == SWITCH_2_GANG)
		{
#ifdef SCENE_SWITCH_DEVICE
			memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ModelIdentifier, "HS-SWS200ZB-VNM", CLD_BAS_MODEL_ID_SIZE);
#else
			memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ModelIdentifier, "HS-SWL200ZB-VNM", CLD_BAS_MODEL_ID_SIZE);
#endif /*SCENE_SWITCH_DEVICE*/
		}
		else if(u8NumberOfSwitch == SWITCH_3_GANG)
		{
#ifdef SCENE_SWITCH_DEVICE
			memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ModelIdentifier, "HS-SWS300ZB-VNM", CLD_BAS_MODEL_ID_SIZE);
#else
			memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ModelIdentifier, "HS-SWL300ZB-VNM", CLD_BAS_MODEL_ID_SIZE);
#endif /*SCENE_SWITCH_DEVICE*/
		}
		else if(u8NumberOfSwitch == SWITCH_4_GANG)
		{
#ifdef SCENE_SWITCH_DEVICE
			memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ModelIdentifier, "HS-SWS400ZB-VNM", CLD_BAS_MODEL_ID_SIZE);
#else
			memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ModelIdentifier, "HS-SWL400ZB-VNM", CLD_BAS_MODEL_ID_SIZE);
#endif /*SCENE_SWITCH_DEVICE*/
		}

		memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8DateCode, "20190613", CLD_BAS_DATE_SIZE);
		memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8SWBuildID, "1000-0001", CLD_BAS_SW_BUILD_SIZE);
	}
//	vReadRecordedStatusOfSwitch();
}

PUBLIC teZCL_Status APP_SWITCH_eZCLRegisterEndpoint(tfpZCL_ZCLCallBackFunction fptr)
{
	teZCL_Status eZCL_Status;
	uint8 i;
	for(i = 0; i < u8NumberOfSwitch; i++)
	{
		  eZCL_Status =  eZLO_RegisterOnOffLightEndPoint(u8ArrSwitchEndpoint[i],
		                                                      fptr,
		                                                      &sOnOffLightDevice[i]);
		  if(eZCL_Status != E_ZCL_SUCCESS)
			  return eZCL_Status;
	}

	return E_ZCL_SUCCESS;
}

PUBLIC bool	APP_SWITCH_vIsApplicationEndpoint(uint8_t u8Endpoint)
{
	uint8 i;
	for(i = 0; i < u8NumberOfSwitch; i++)
	{
		if(u8Endpoint == u8ArrSwitchEndpoint[i])
			return TRUE;
	}
	return FALSE;
}

PUBLIC bool APP_SWITCH_bGetSwitchOnOffStatus(
	APP_eSwitch	 eSwitch
)
{
	return (sOnOffLightDevice[eSwitch].sOnOffServerCluster.bOnOff);
}

PUBLIC void APP_SWITCH_bSetSwitchOnOffStatus(APP_eSwitch eSwitch, bool bStatus)
{
	sOnOffLightDevice[eSwitch].sOnOffServerCluster.bOnOff = bStatus;
}

PUBLIC bool APP_SWITCH_bSendSwitchStatus(
	APP_eSwitch	 eSwitch
)
{
	if(E_ZCL_SUCCESS  != APP_REPORT_eReportAttribute(u8ArrSwitchEndpoint[eSwitch], GENERAL_CLUSTER_ID_ONOFF, E_CLD_ONOFF_ATTR_ID_ONOFF));
		return FALSE;

	return TRUE;
}

PUBLIC uint8_t APP_SWITCH_u8GetEndpointIndex(uint8_t u8Endpoint)
{
	uint8 i;
	for(i = 0; i < u8NumberOfSwitch; i++)
	{
		if(u8Endpoint == u8ArrSwitchEndpoint[i])
			return i;
	}
	return 0;
}

PUBLIC void APP_SWITCH_vSaveSwitchStatus(void)
{
	uint8 i;
	bool_t	bSwitchStatus[MAX_NUMBER_OF_SWITCH] = {FALSE};

	for(i = 0; i < u8NumberOfSwitch; i++)
	{
		bSwitchStatus[i] = sOnOffLightDevice[i].sOnOffServerCluster.bOnOff;
	}

	PDM_eSaveRecordData(PDM_ID_SWITCH_STATUS,
							bSwitchStatus,
							sizeof(bSwitchStatus));
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vCheckNumberOfSwitch(void)
{
	uint32_t	u32SwitchTypePinMask = (1UL << PIN_IN_SWITCH_TYPE_BIT_0) | (1UL << PIN_IN_SWITCH_TYPE_BIT_1);
	/* Set switch pin as input */
	vAHI_DioSetDirection(u32SwitchTypePinMask, 0);
	/* Disable internal pull-up resistor */
	vAHI_DioSetPullup(0, u32SwitchTypePinMask);
	/* Disable interrupt */
	vAHI_DioInterruptEnable(0, u32SwitchTypePinMask);

	uint32 u32ReadInput = u32AHI_DioReadInput() & u32SwitchTypePinMask;
	u8NumberOfSwitch = (uint8_t)((((u32ReadInput >> PIN_IN_SWITCH_TYPE_BIT_1) & 1UL) << 1) | ((u32ReadInput >> PIN_IN_SWITCH_TYPE_BIT_0) & 1UL)) + 1;
}

PRIVATE void vReadRecordedStatusOfSwitch(void)
{
	uint8 i;
	uint16 u16BytesRead = 0;

	bool_t	bSwitchStatus[MAX_NUMBER_OF_SWITCH] = {FALSE};

	PDM_eReadDataFromRecord(PDM_ID_SWITCH_STATUS,
								bSwitchStatus,
								sizeof(bSwitchStatus),
								&u16BytesRead);

	for(i = 0; i < u8NumberOfSwitch; i++)
	{
		sOnOffLightDevice[i].sOnOffServerCluster.bOnOff = bSwitchStatus[i];
	}
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
