/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_node.c
 *
 * DESCRIPTION:        Base Device Demo: Router application
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5168, JN5179].
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2017. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include <jendefs.h>
#include <string.h>
#include "dbg.h"
#include "pdum_apl.h"
#include "pdum_nwk.h"
#include "pdum_gen.h"
#include "pwrm.h"
#include "PDM.h"
#include "zps_gen.h"
#include "zps_apl.h"
#include "zps_apl_af.h"
#include "zps_apl_zdo.h"
#include "zps_apl_zdp.h"
#include "zps_apl_aib.h"
#include "zps_apl_aps.h"
#include "app_common.h"
#include "app_main.h"
#include "app_buttons.h"
#include "ZTimer.h"
#include "app_events.h"
#include <rnd_pub.h>
#include "app_zcl_task.h"
#include "app_node.h"
#include "zps_nwk_nib.h"
#include "PDM_IDs.h"
#include "zcl_options.h"
#include "zcl.h"
#include "app_reporting.h"
#include "app_switch.h"
#include "app_nwk_handler.h"
#include "app_capsense.h"
#include "app_led.h"
#include "delay.h"
#include "app_relay.h"
#include "function_button.h"
#include "drv_motor.h"
#include "wd_reset.h"
#include "hw_timer.h"
#include "my_define.h"
#include "AHI_ModuleConfiguration.h"
#ifndef SCENE_SWITCH_DEVICE
#include "drv_coil.h"
#include "zero_detect.h"
#endif
#include "bdb_start.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_APP
    #define TRACE_APP   FALSE
#else
    #define TRACE_APP   TRUE
#endif

#ifdef DEBUG_APP_EVENT
    #define TRACE_APP_EVENT   TRUE
#else
    #define TRACE_APP_EVENT   FALSE
#endif

#define NUMBER_RETRY_STEERING_NWK	(3)

#ifdef TEST_RELAY
	#define TEST_RELAY_CYCLE_ON_OFF_RELAY	(500) //ms
#endif
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void APP_vBdbInit(void);
#ifdef TEST_RELAY
	PRIVATE void vCallbackTimerTestRelay(void *pvParams);
#endif
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
PUBLIC teNodeState eNodeState;
PUBLIC bool_t bBdbJoinFailed = FALSE;
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE uint8_t	u8NumberRetrySteeringNwk = 0;
#ifdef TEST_RELAY
	PRIVATE uint8_t u8TimerTestRelay;
#endif
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

#ifdef PDM_EEPROM
    extern uint8 u8PDM_CalculateFileSystemCapacity();
    extern uint8 u8PDM_GetFileSystemOccupancy();
#endif
/****************************************************************************
 *
 * NAME: APP_vInitialiseRouter
 *
 * DESCRIPTION:
 * Initialises the application related functions
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vInitialiseRouter(void)
{
    uint16 u16ByteRead;

    /* Stay awake */
    PWRM_eStartActivity();

    DBG_vPrintf(TRACE_APP,"APP-NODE:  HW_TIMER_vInitialize() \n");
    HW_TIMER_vInitialize();

    DBG_vPrintf(TRACE_APP,"APP-NODE:  WD_RESET_vInitialize() \n");
    WD_RESET_vInitialize();

    DBG_vPrintf(TRACE_APP,"APP-NODE:  APP_SWITCH_vInitialize() \n");
    APP_SWITCH_vInitialize();
    DBG_vPrintf(TRACE_APP,"APP-NODE:  number of switch: %d \n", u8NumberOfSwitch);

    DBG_vPrintf(TRACE_APP,"APP-NODE:  DRV_MOTOR_vInitialize() \n");
    DRV_MOTOR_vInitialize(100);		// vibration level is 80

    DBG_vPrintf(TRACE_APP,"APP-NODE:  DELAY_vInitialize() \n");
    DELAY_vInitialize();

    DBG_vPrintf(TRACE_APP,"APP-NODE:  APP_CAPSENSE_vInitialize() \n");
    APP_CAPSENSE_vInitialize();

    DBG_vPrintf(TRACE_APP,"APP-NODE:  APP_LED_vInitialize() \n");
    APP_LED_vInitialize();

#ifndef SCENE_SWITCH_DEVICE
    DBG_vPrintf(TRACE_APP,"APP-NODE:  APP_RELAY_vInitialize() \n");
    APP_RELAY_vInitialize();
#endif

#ifdef TEST_DEVICE
    DBG_vPrintf(TRACE_APP,"APP-NODE:  APP_BUTTONS_vInitialize() \n");
    APP_BUTTONS_vInitialize();
#else
    DBG_vPrintf(TRACE_APP,"APP-NODE:  FUNC_BUTTON_vInitialize() \n");
    FUNC_BUTTON_vInitialize();
#endif

    eNodeState = E_STARTUP;
    PDM_eReadDataFromRecord(PDM_ID_NODE_STATE,
                            &eNodeState,
                            sizeof(teNodeState),
                            &u16ByteRead);

    ZPS_u32MacSetTxBuffers  (4);

    /* Initialise ZBPro stack */
    ZPS_eAplAfInit();

    /* Initialise ZCL */
    APP_ZCL_vInitialise();

    /* Initialise other software modules
     * HERE
     */
    //
    DBG_vPrintf(TRACE_APP,"APP-NODE:  APP_vBdbInit() \n");
    APP_vBdbInit();

    /* Always initialise any peripherals used by the application
     * HERE
     */


    /* The functions u8PDM_CalculateFileSystemCapacity and u8PDM_GetFileSystemOccupancy
     * may be called at any time to monitor space available in  the eeprom  */
    DBG_vPrintf(TRACE_APP, "PDM: Capacity %d\n", u8PDM_CalculateFileSystemCapacity() );
    DBG_vPrintf(TRACE_APP, "PDM: Occupancy %d\n", u8PDM_GetFileSystemOccupancy() );

    DBG_vPrintf(TRACE_APP, "Start Up StaTe %d On Network %d\n",
            eNodeState,
            sBDB.sAttrib.bbdbNodeIsOnANetwork);

#ifdef TEST_RELAY
    DELAY_vStart(TEST_RELAY_CYCLE_ON_OFF_RELAY, vCallbackTimerTestRelay, NULL, &u8TimerTestRelay);
#endif
}

/****************************************************************************
 *
 * NAME: APP_taskRouter
 *
 * DESCRIPTION:
 * Task that handles application related functions
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_taskRouter(void)
{
    APP_tsEvent sAppEvent;
    sAppEvent.eType = APP_E_EVENT_NONE;

    APP_LED_tsLedEvent_t sLedEvent;
    sLedEvent.eLedState = E_STATE_NONE;

    if (ZQ_bQueueReceive(&APP_msgAppEvents, &sAppEvent) == TRUE)
    {
        DBG_vPrintf(TRACE_APP, "APP_NODE: App event %d, NodeState=%d\n", sAppEvent.eType, eNodeState);

        /* Network Events*/
        if(sAppEvent.eType == APP_E_EVENT_NWK_STEERING_SUCCESS)
        {
        	DBG_vPrintf(TRACE_APP, "APP_NODE: App event: APP_E_EVENT_NWK_STEERING_SUCCESS \n");
        	APP_REPORT_bReportDeviceInfor();
        	// CTKT khong mo ta nhay led
        	APP_LED_vStartEvent(E_STATE_BLINK_NWK_STEERING_SUCCESS);
        }
        else if(sAppEvent.eType == APP_E_EVENT_NWK_STEERING_FAILURE)
        {
        	DBG_vPrintf(TRACE_APP, "APP_NODE: App event: APP_E_EVENT_NWK_STEERING_FAILURE \n");
        	// CTKT khong mo ta nhay led
        	if(eNodeState == E_STARTUP)
        	{
        		// Retry steering network
        		u8NumberRetrySteeringNwk ++;
        		if(u8NumberRetrySteeringNwk >= NUMBER_RETRY_STEERING_NWK)
        		{
        			u8NumberRetrySteeringNwk = 0;
        			APP_LED_vStartEvent(E_STATE_BLINK_NWK_STEERING_FAILURE);
        		}
        		else
        		{
        			BDB_teStatus eStatus = BDB_eNsStartNwkSteering();
        			DBG_vPrintf(TRACE_APP, "APP_NODE: Retry Steering Network time %d, status %d \n",u8NumberRetrySteeringNwk, eStatus);
        		}
        	}
        }
        else if(sAppEvent.eType == APP_E_EVENT_ZCL_APS_ACK_FAIL)
        {
			DBG_vPrintf(TRACE_APP,
					"APP_NODE: App event: APP_E_EVENT_ZCL_APS_ACK_FAIL EP: %d, clusterID: 0x%04x \n",
					sAppEvent.uEvent.sACKEvent.u8EndPoint,
					sAppEvent.uEvent.sACKEvent.u16ClusterID);
			if(GENERAL_CLUSTER_ID_ONOFF == sAppEvent.uEvent.sACKEvent.u16ClusterID)
			{
				uint8_t u8SwitchIndex = APP_SWITCH_u8GetEndpointIndex(sAppEvent.uEvent.sACKEvent.u8EndPoint);
				sLedEvent.eLedState 		= E_STATE_BLINK_ACK_FAIL;
				sLedEvent.u8RGBLedNumber 	= u8SwitchIndex;
				APP_LED_vStart(sLedEvent);
			}
			bBdbJoinFailed = TRUE;
        }

        /* Function button event */
        else if (sAppEvent.eType == APP_E_EVENT_FUNC_BUTTON_UP_SHORTER_5S)
        {
        	DBG_vPrintf(TRACE_APP, "APP_NODE: App event : APP_E_EVENT_FUNC_BUTTON_UP_SHORTER_5S \n");
        	if(eNodeState == E_RUNNING)
        	{
        		APP_REPORT_bReportDeviceInfor();
        		// CTKT khong mo ta nhay led
        		APP_LED_vStartEvent(E_STATE_BLINK_CHECK_NWK_CONECTION_GOOD);
        	}
        	else
        	{
        		// CTKT khong mo ta nhay led
        		APP_LED_vStartEvent(E_STATE_BLINK_CHECK_NWK_CONECTION_ERROR);
        	}
        }
        else if (sAppEvent.eType == APP_E_EVENT_FUNC_BUTTON_HOLD_5S)
        {
        	DBG_vPrintf(TRACE_APP, "APP_NODE: App event: APP_E_EVENT_FUNC_BUTTON_HOLD_5S \n");
        	//Blink led red and blue alternately in 5s T = 500ms, then do factory reset
        	APP_LED_vStartEvent(E_STATE_BLINK_FACTORY_RESET);
        }
        /*Switch report status on off to coordinator*/
        else if (sAppEvent.eType == APP_E_EVENT_RELAY_REPORT_STATUS)
        {
			DBG_vPrintf(TRACE_APP,
					"APP_NODE: App event: APP_E_EVENT_RELAY_REPORT_STATUS switch: %d , status: %d \n",
					sAppEvent.uEvent.sSwitch.u8Switch,
					sOnOffLightDevice[sAppEvent.uEvent.sSwitch.u8Switch].sOnOffServerCluster.bOnOff);
			if(eNodeState == E_RUNNING)
			{
				APP_SWITCH_bSendSwitchStatus(sAppEvent.uEvent.sSwitch.u8Switch);
			}
          }
        /* Cap sense event */
        else if (sAppEvent.eType == APP_E_EVENT_CAP_SENSE_PRESSED)
        {
        	DBG_vPrintf(TRACE_APP,
        			"APP_NODE: App event: APP_E_EVENT_CAP_SENSE_PRESSED switch: %d , status: %d \n",
        			sAppEvent.uEvent.sSwitch.u8Switch,
        			sOnOffLightDevice[sAppEvent.uEvent.sSwitch.u8Switch].sOnOffServerCluster.bOnOff);
        	if(sAppEvent.uEvent.sSwitch.u8Switch >= u8NumberOfSwitch)
        		return;
        	DRV_MOTOR_vRun(250, NULL, NULL);
#ifdef SCENE_SWITCH_DEVICE
        	APP_SWITCH_bSetSwitchOnOffStatus(sAppEvent.uEvent.sSwitch.u8Switch, TRUE);
        	if(eNodeState == E_RUNNING)
        	{
        		APP_SWITCH_bSendSwitchStatus(sAppEvent.uEvent.sSwitch.u8Switch);
        		//Update led status
				sLedEvent.eLedState 		= E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_ON_NWK;
				sLedEvent.u8RGBLedNumber 	= sAppEvent.uEvent.sSwitch.u8Switch;
				APP_LED_vStart(sLedEvent);
        	}
        	else
        	{
        		sLedEvent.eLedState 		= E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_NOT_ON_NWK;
        		sLedEvent.u8RGBLedNumber 	= sAppEvent.uEvent.sSwitch.u8Switch;
        		APP_LED_vStart(sLedEvent);
        	}
        	APP_SWITCH_bSetSwitchOnOffStatus(sAppEvent.uEvent.sSwitch.u8Switch, FALSE);
#else
        	APP_SWITCH_bSetSwitchOnOffStatus(sAppEvent.uEvent.sSwitch.u8Switch,
        			!sOnOffLightDevice[sAppEvent.uEvent.sSwitch.u8Switch].sOnOffServerCluster.bOnOff);
        	APP_RELAY_vSet(sAppEvent.uEvent.sSwitch.u8Switch,
        			sOnOffLightDevice[sAppEvent.uEvent.sSwitch.u8Switch].sOnOffServerCluster.bOnOff);
//        	APP_SWITCH_vSaveSwitchStatus();
        	if(eNodeState == E_RUNNING)
        	{
        		//Update led status
				sLedEvent.eLedState 		= E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_ON_NWK;
				sLedEvent.u8RGBLedNumber 	= sAppEvent.uEvent.sSwitch.u8Switch;
				APP_LED_vStart(sLedEvent);
        	}
        	else
        	{
        		sLedEvent.eLedState 		= E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_NOT_ON_NWK;
        		sLedEvent.u8RGBLedNumber 	= sAppEvent.uEvent.sSwitch.u8Switch;
        		APP_LED_vStart(sLedEvent);
        	}
#endif
        }
        /* Event from mobile app (coordinator) */
        else if (sAppEvent.eType == APP_E_EVENT_COORDINATOR_ON_OFF_CMD)
        {
			DBG_vPrintf(TRACE_APP,
					"APP_NODE: App event: APP_E_EVENT_COORDINATOR_ON_OFF_CMD switch: %d , status: %d \n",
					sAppEvent.uEvent.sSwitch.u8Switch,
					sOnOffLightDevice[sAppEvent.uEvent.sSwitch.u8Switch].sOnOffServerCluster.bOnOff);
#ifdef SCENE_SWITCH_DEVICE

#else
        	APP_RELAY_vSet(sAppEvent.uEvent.sSwitch.u8Switch,
        			sOnOffLightDevice[sAppEvent.uEvent.sSwitch.u8Switch].sOnOffServerCluster.bOnOff);
//        	APP_SWITCH_vSaveSwitchStatus();
    		//Update led status
			sLedEvent.eLedState 		= E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_ON_NWK;
			sLedEvent.u8RGBLedNumber 	= sAppEvent.uEvent.sSwitch.u8Switch;
			APP_LED_vStart(sLedEvent);
#endif
        }
    }

    // Scan capsense
    if(APP_CAPSENSE_bIsNeedToScanCapsense())
    {
    	APP_CAPSENSE_vScanCapsense();
    	APP_CAPSENSE_vClearFlagScanCapsense();
    }
}
/****************************************************************************
 *
 * NAME: APP_Node_vFactoryResetRecords
 *
 * DESCRIPTION:
 * Resets persisted data structures to factory new state
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_Node_vFactoryResetRecords(void)
{
    /* clear out the stack */
    ZPS_vDefaultStack();
    ZPS_vSetKeys();
    ZPS_eAplAibSetApsUseExtendedPanId (0);

    /* save everything */
    eNodeState = E_STARTUP;
    PDM_eSaveRecordData(PDM_ID_NODE_STATE,&eNodeState,sizeof(teNodeState));

//    // Record that Capsense already configure, no need to configure in the future reset
//    bool_t	bStatus = FALSE;
//    PDM_eSaveRecordData(PDM_ID_CAPSENSE_ALREADY_CONFIGURE,
//    		&bStatus,
//    		sizeof(bool_t));
    // Reset status of switch
	bool_t	bSwitchStatus[MAX_NUMBER_OF_SWITCH] = {FALSE};
	PDM_eSaveRecordData(PDM_ID_SWITCH_STATUS,
							bSwitchStatus,
							sizeof(bSwitchStatus));

    ZPS_vSaveAllZpsRecords();
}

PUBLIC void APP_Node_vLeaveAndReset(void)
{
	// Do factory reset
	if (eNodeState == E_RUNNING)
	{
		if (ZPS_eAplZdoLeaveNetwork( 0UL, FALSE, FALSE) != ZPS_E_SUCCESS )
		{
			APP_Node_vFactoryResetRecords();
			vAHI_SwReset();
		}
	}
	else
	{
		APP_Node_vFactoryResetRecords();
		vAHI_SwReset();
	}
}

PUBLIC void APP_Node_vRejoin(void)
{
    bBdbJoinFailed = FALSE;
    sBDB.sAttrib.bbdbNodeIsOnANetwork = TRUE;
#if (defined ZBPRO_DEVICE_TYPE_ZED)
    BDB_vStart(); // Rejoin
#elif (defined ZBPRO_DEVICE_TYPE_ZCR)
    DBG_vPrintf(TRACE_APP, "APP_NODE: u8RejoinCycles = %d", u8RejoinCycles);
    if ((0xFF == u8RejoinCycles) || (0 == u8RejoinCycles))
    {
        DBG_vPrintf(TRACE_APP, " => RejoinCycle");
        BDB_vRejoinCycle(FALSE);
    }
    DBG_vPrintf(TRACE_APP, "\n");
#endif
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vBdbInit
 *
 * DESCRIPTION:
 * Function to initialize BDB attributes and message queue
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void APP_vBdbInit(void)
{
    BDB_tsInitArgs sInitArgs;

    sBDB.sAttrib.bbdbNodeIsOnANetwork = ((eNodeState >= E_RUNNING)?(TRUE):(FALSE));
    sInitArgs.hBdbEventsMsgQ = &APP_msgBdbEvents;
    BDB_vInit(&sInitArgs);
}

#ifdef TEST_RELAY
PRIVATE void vCallbackTimerTestRelay(void *pvParams)
{
	// Stop timer
	DELAY_vStop(&u8TimerTestRelay);
	// Send message
	APP_tsEvent sAppEventTestRelay;
	sAppEventTestRelay.eType = APP_E_EVENT_CAP_SENSE_PRESSED;
	sAppEventTestRelay.uEvent.sSwitch.u8Switch = 0;
	if(ZQ_bQueueSend(&APP_msgAppEvents, &sAppEventTestRelay) == FALSE)
	{
		DBG_vPrintf(TRACE_APP, "APP_NODE: Test relay Failed to post Event %d \n", sAppEventTestRelay.eType);
	}
	// Start timer again
	DELAY_vStart(TEST_RELAY_CYCLE_ON_OFF_RELAY, vCallbackTimerTestRelay, NULL, &u8TimerTestRelay);
}
#endif
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
