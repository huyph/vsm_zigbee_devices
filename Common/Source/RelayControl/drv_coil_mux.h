/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_coil_mux.h
 *
 * DESCRIPTION:
 *
 ***************************************************************************/
#ifndef DRV_COIL_MUX_H_
#define DRV_COIL_MUX_H_
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "jendefs.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
PUBLIC void DRV_COIL_MUX_vInitialize(void);

PUBLIC void DRV_COIL_MUX_vSet(
		uint8_t u8CoilNumber,
		bool_t	bOnOff
);
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
#endif /* DRV_COIL_MUX_H_ */
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
