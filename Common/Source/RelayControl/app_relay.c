/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_relay.c
 *
 * DESCRIPTION:        Control relay
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "app_relay.h"
#ifdef NORMAL_SWITCH_DEVICE
#include "app_switch.h"
#endif	/* NORMAL_SWITCH_DEVICE */
#include "dbg.h"
#include "app_main.h"
#include "delay.h"
#include "app_events.h"
#include "drv_relay.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_APP_RELAY
    #define TRACE_APP_RELAY         (TRUE)
#else   /* DEBUG_APP_RELAY */
    #define TRACE_APP_RELAY         (FALSE)
#endif  /* DEBUG_APP_RELAY */

#ifdef PIR_DEVICE
	#define MAX_NUMBER_OF_RELAY 	(1)
#else	/* NORMAL_SWITCH_DEVICE */
	#define MAX_NUMBER_OF_RELAY 	(4)
#endif	/* PIR_DEVICE */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef enum
{
	E_PROCESS_NONE = 0,
	E_PROCESS_ON,
	E_PROCESS_OFF,
	NUMBER_PROCESS
}APP_RELAY_teRelayProcess;
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vProcessOnOffRelay(void);
PRIVATE void vCallbackRelayOnOff(void* pvParams);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
extern PUBLIC tszQueue APP_msgAppEvents;
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE uint8_t						NUMBER_OF_RELAY;
PRIVATE DRV_RELAY_tsRelay_t			sRelay[MAX_NUMBER_OF_RELAY];
PRIVATE APP_RELAY_teRelayProcess	eRelayProcess[MAX_NUMBER_OF_RELAY]; 	// = TRUE: processing turn on relay, FALSE: processing turn off relay
PRIVATE bool_t						bIsProcessingTurnOnOffRelay = FALSE;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_RELAY_vInitialize(void)
{
	uint8_t	i;
	//
#ifdef PIR_DEVICE
	 NUMBER_OF_RELAY = 1;
#else	/* NORMAL_SWITCH_DEVICE */
	 NUMBER_OF_RELAY = u8NumberOfSwitch;
#endif	/* PIR_DEVICE */
	//
	 for(i = 0; i < NUMBER_OF_RELAY; i++)
	 {
		 sRelay[i].u8RelayNumber 				= i;

		 sRelay[i].sCoilRst.u8CoilNumber		= 2*i;
		 sRelay[i].sCoilRst.u16DelayToOn		= TIME_DELAY_TO_RELEASE_RELAY;
		 sRelay[i].sCoilRst.u16PulseWidth		= TIME_TO_ACTIVE_COIL;
		 sRelay[i].sCoilRst.u8TimerDelayToOn	= NUMBER_OF_HW_TIMER_DELAY;
		 sRelay[i].sCoilRst.u8TimerPulseWidth	= NUMBER_OF_HW_TIMER_DELAY;

		 sRelay[i].sCoilSet.u8CoilNumber		= 2*i + 1;
		 sRelay[i].sCoilSet.u16DelayToOn		= TIME_DELAY_TO_OPEN_RELAY;
		 sRelay[i].sCoilSet.u16PulseWidth		= TIME_TO_ACTIVE_COIL;
		 sRelay[i].sCoilSet.u8TimerDelayToOn	= NUMBER_OF_HW_TIMER_DELAY;
		 sRelay[i].sCoilSet.u8TimerPulseWidth	= NUMBER_OF_HW_TIMER_DELAY;

		 sRelay[i].fnCallback					= vCallbackRelayOnOff;
		 sRelay[i].pvParams						= &sRelay[i].u8RelayNumber;
	 }
	 // Relay initialize
	 DRV_RELAY_vInitialize(NUMBER_OF_RELAY == 4);
	 // Turn off all relay
	 for(i = 0; i < NUMBER_OF_RELAY; i++)
	 {

#ifdef NORMAL_SWITCH_DEVICE
		 APP_RELAY_vSet(i, sOnOffLightDevice[i].sOnOffServerCluster.bOnOff);
#else
		 APP_RELAY_vSet(i, FALSE);
#endif
	 }
}

PUBLIC void APP_RELAY_vSet(
	uint8_t		u8RelayNumber,
	bool_t		bOnOff
)
{
	DBG_vPrintf(TRACE_APP_RELAY, "APP_RELAY: APP_RELAY_vSet %d \n", u8RelayNumber);
	if(bOnOff)
	{
		APP_RELAY_vOn(u8RelayNumber);
	}
	else
	{
		APP_RELAY_vOff(u8RelayNumber);
	}
}

PUBLIC void APP_RELAY_vOn(
	uint8_t		u8RelayNumber
)
{
	if(u8RelayNumber >= NUMBER_OF_RELAY)
		return;
	DBG_vPrintf(TRACE_APP_RELAY, "APP_RELAY: APP_RELAY_vOn %d \n", u8RelayNumber);
	eRelayProcess[u8RelayNumber] = E_PROCESS_ON;
	if(!bIsProcessingTurnOnOffRelay)
	{
		vProcessOnOffRelay();
	}
}

PUBLIC void APP_RELAY_vOff(
	uint8_t		u8RelayNumber
)
{
	if(u8RelayNumber >= NUMBER_OF_RELAY)
		return;
	DBG_vPrintf(TRACE_APP_RELAY, "APP_RELAY: APP_RELAY_vOff %d \n", u8RelayNumber);
	eRelayProcess[u8RelayNumber] = E_PROCESS_OFF;
	if(!bIsProcessingTurnOnOffRelay)
	{
		vProcessOnOffRelay();
	}
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

PRIVATE void vProcessOnOffRelay(void)
{
	uint8_t	i;
	bIsProcessingTurnOnOffRelay = FALSE;
	for(i = 0; i < NUMBER_OF_RELAY; i++)
	{
		if(eRelayProcess[i] == E_PROCESS_ON)
		{
			DBG_vPrintf(TRACE_APP_RELAY, "APP_RELAY: Start process turn on relay %d \n", i);
			bIsProcessingTurnOnOffRelay = TRUE;
			DRV_RELAY_vOn(&sRelay[i]);
			break;
		}
		else if(eRelayProcess[i] == E_PROCESS_OFF)
		{
			DBG_vPrintf(TRACE_APP_RELAY, "APP_RELAY: Start process turn off relay %d \n", i);
			bIsProcessingTurnOnOffRelay = TRUE;
			DRV_RELAY_vOff(&sRelay[i]);
			break;
		}
	}

	if(!bIsProcessingTurnOnOffRelay)
	{
		DBG_vPrintf(TRACE_APP_RELAY, "APP_RELAY: No more relay need to process \n", i);
	}
}

PRIVATE void vCallbackRelayOnOff(void* pvParams)
{
	if(NULL == pvParams)	return;
	uint8_t u8RelayNumber = *(uint8_t*)pvParams;
	DBG_vPrintf(TRACE_APP_RELAY,"APP_RELAY: vCallbackRelayOnOff() Relay %d \n", u8RelayNumber);

	// Post message to main program
	eRelayProcess[u8RelayNumber] = E_PROCESS_NONE;

	// Post message to Main program
	APP_tsEvent sAppEvent;
	sAppEvent.eType = APP_E_EVENT_RELAY_REPORT_STATUS;
	sAppEvent.uEvent.sSwitch.u8Switch	= u8RelayNumber;
	if(ZQ_bQueueSend(&APP_msgAppEvents, &sAppEvent) == FALSE)
	{
		DBG_vPrintf(TRACE_APP_RELAY, "APP_RELAY: Failed to post Event %d \n", sAppEvent.eType);
	}
	// Check Is there any relay waiting process
	vProcessOnOffRelay();
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
