/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_capsense.c
 *
 * DESCRIPTION:        Cypress cap sense
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "app_capsense.h"
#include "AppHardwareApi_JN516x.h"
#include "ZTimer.h"
#include "PDM.h"
#include "CY8CMBR3xxx_HostFunctions.h"
#include "CY8CMBR3xxx_APIs.h"
#include "my_define.h"
#include "dbg.h"
#include "app_switch.h"
#include "app_main.h"
#include "app_events.h"
#include "PDM_IDs.h"
#include "hw_timer.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_APP_CAPSENSE
    #define TRACE_APP_CAPSENSE          (TRUE)
#else   /* DEBUG_APP_CAPSENSE */
    #define TRACE_APP_CAPSENSE          (FALSE)
#endif  /* DEBUG_APP_CAPSENSE */

#define SENSOR_ADDR 					(0x37)

#ifdef TEST_CAP_SENCE_ON_KIT
	#define	CAP_SENSE_SWITCH_1			(3)
	#define	CAP_SENSE_SWITCH_2			(4)
	#define	CAP_SENSE_SWITCH_3			(5)
	#define	CAP_SENSE_SWITCH_4			(6)
#else
	#define	CAP_SENSE_SWITCH_1			(0)
	#define	CAP_SENSE_SWITCH_2			(1)
	#define	CAP_SENSE_SWITCH_3			(4)
	#define	CAP_SENSE_SWITCH_4			(5)
#endif /*TEST_CAP_SENCE_ON_KIT*/

#define MAX_NUMBER_CAP_SENSE_SWITCH		(4)

#define TOUCH_POWER_RST_DIO_MASK		(1UL << PIN_OUT_TOUCH_POWER_RST)
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void APP_CAPSENSE_vPowerOn(void);
PRIVATE void APP_CAPSENSE_vPowerOff(void);
PRIVATE void APP_CAPSENSE_vResetPower(void);

PRIVATE void vCallbackTimerScanCapSense(HW_TIMER_tsTimerEvent_t sEvent);
PRIVATE bool_t bIsConfigAlready(void);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
extern PUBLIC tszQueue APP_msgAppEvents;
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE uint8_t		NUMBER_OF_CAPSENSE;
PRIVATE	uint16_t	u16PreviousStateCapsense = 0x0000;
PRIVATE uint16_t	u16ArrCapSenseSwitch[MAX_NUMBER_CAP_SENSE_SWITCH] = {
												1<<CAP_SENSE_SWITCH_1,
												1<<CAP_SENSE_SWITCH_2,
												1<<CAP_SENSE_SWITCH_3,
												1<<CAP_SENSE_SWITCH_4
												};

PRIVATE bool_t	bNeedToScanCapsense = FALSE;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_CAPSENSE_vInitialize(void)
{
	NUMBER_OF_CAPSENSE = u8NumberOfSwitch;
	//Setting power reset pin
	if(TOUCH_POWER_RST_DIO_MASK >> DO_0)
	{
		bAHI_DoEnableOutputs(TRUE);
		vAHI_DoSetPullup(0, (uint8_t)(TOUCH_POWER_RST_DIO_MASK >> DO_0));
	}
	else
	{
		vAHI_DioSetDirection(0UL, TOUCH_POWER_RST_DIO_MASK);
		vAHI_DioSetPullup(0UL, TOUCH_POWER_RST_DIO_MASK);
	}

//	APP_CAPSENSE_vResetPower();

	uint8_t i;
	for(i = 0; i < 3; i++)
	{
		vAHI_WatchdogRestart();

		if(bHost_Init(bIsConfigAlready()))
		{
			// Record that Capsense already configure, no need to configure in the future reset
			bool_t	bStatus = TRUE;
			PDM_eSaveRecordData(PDM_ID_CAPSENSE_ALREADY_CONFIGURE,
					&bStatus,
					sizeof(bool_t));

			// for Switch 3Gang we need to swap Switch 1 and 2 (Hardware layout requirement)
#ifndef TEST_CAP_SENCE_ON_KIT
			if(NUMBER_OF_CAPSENSE == SWITCH_3_GANG)
			{
				uint16_t u16Tmp = u16ArrCapSenseSwitch[0];
				u16ArrCapSenseSwitch[0] = u16ArrCapSenseSwitch[1];
				u16ArrCapSenseSwitch[1] = u16Tmp;
			}
#endif
			uint8_t i;
			uint16_t u16CapsenseMask = 0;
			for (i = 0; i < NUMBER_OF_CAPSENSE; i++)
			{
				u16CapsenseMask |= u16ArrCapSenseSwitch[i];
			}

			CY8CMBR3xxx_SENSORSTATUS sensorStatus;
			if (TRUE == CY8CMBR3xxx_ReadSensorStatus(SENSOR_ADDR, &sensorStatus) )
			{
				u16PreviousStateCapsense = u16CapsenseMask & sensorStatus.buttonStatus;
			}

			DBG_vPrintf(TRACE_APP_CAPSENSE,
					"APP_CAPSENSE: bHost_Init Success, Current capsense status: %4x \n",
					u16PreviousStateCapsense);

			// Start timer scan read touch status
			HW_TIMER_bEnable(
							HW_TIMER_SCAN_CAPSENSE,
							FALSE,
							E_HW_TIMER_PERIOD_RANGE_64US_TO_4S,
							TRUE,
							FALSE,
							vCallbackTimerScanCapSense,
							NULL,
							E_TIMER_OUTPUT_NONE,
							0
					);
			HW_TIMER_vStartSingleShot(HW_TIMER_SCAN_CAPSENSE, TIME_SCAN_CAPSENSE, 0);
			break;
		}
		else
		{
			DBG_vPrintf(TRACE_APP_CAPSENSE, "APP_CAPSENSE: bHost_Init fail \n");
			APP_CAPSENSE_vResetPower();
		}
	}
}

PUBLIC bool_t APP_CAPSENSE_bIsNeedToScanCapsense(void)
{
	return bNeedToScanCapsense;
}

PUBLIC void APP_CAPSENSE_vClearFlagScanCapsense(void)
{
	bNeedToScanCapsense = FALSE;
}

PUBLIC void APP_CAPSENSE_vScanCapsense(void)
{
	static uint8_t u8CountReadCapsenseFail = 0;
	CY8CMBR3xxx_SENSORSTATUS sensorStatus;
	if (TRUE == CY8CMBR3xxx_ReadSensorStatus(SENSOR_ADDR, &sensorStatus) )
	{
		u8CountReadCapsenseFail = 0;
		uint8_t i;
		for (i = 0; i < NUMBER_OF_CAPSENSE; i++)
		{
			if((u16ArrCapSenseSwitch[i] & sensorStatus.buttonStatus)	// current press
					&& (!(u16ArrCapSenseSwitch[i] & u16PreviousStateCapsense)))  // previous release
			{
				u16PreviousStateCapsense |=  u16ArrCapSenseSwitch[i];
				DBG_vPrintf(TRACE_APP_CAPSENSE,"APP_CAPSENSE: Button [%d] pressed \n", i);

				APP_tsEvent sAppEvent;
				sAppEvent.eType = APP_E_EVENT_CAP_SENSE_PRESSED;
				sAppEvent.uEvent.sSwitch.u8Switch = i;
				if(ZQ_bQueueSend(&APP_msgAppEvents, &sAppEvent) == FALSE)
				{
					DBG_vPrintf(TRACE_APP_CAPSENSE, "APP_CAPSENSE: Failed to post Event %d \n", sAppEvent.eType);
				}
			}
			else if((!(u16ArrCapSenseSwitch[i] & sensorStatus.buttonStatus))	// current release
					&& (u16ArrCapSenseSwitch[i] & u16PreviousStateCapsense))   // previous press
			{
				u16PreviousStateCapsense &=  ~u16ArrCapSenseSwitch[i];
				DBG_vPrintf(TRACE_APP_CAPSENSE,"APP_CAPSENSE: Button [%d] released \n", i);
			}
		}
	}
	else
	{
		u8CountReadCapsenseFail++;
		if(u8CountReadCapsenseFail == 100)
		{
			u8CountReadCapsenseFail = 0;
			APP_CAPSENSE_vResetPower();
		}
	}
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void APP_CAPSENSE_vPowerOn(void)
{
	if(TOUCH_POWER_RST_DIO_MASK >> DO_0)
	{
		vAHI_DoSetDataOut(TOUCH_POWER_RST_DIO_MASK >> DO_0, 0);
	}
	else
	{
		vAHI_DioSetOutput(TOUCH_POWER_RST_DIO_MASK, 0);
	}
}

PRIVATE void APP_CAPSENSE_vPowerOff(void)
{
	if(TOUCH_POWER_RST_DIO_MASK >> DO_0)
	{
		vAHI_DoSetDataOut(0, TOUCH_POWER_RST_DIO_MASK >> DO_0);
	}
	else
	{
		vAHI_DioSetOutput(0, TOUCH_POWER_RST_DIO_MASK);
	}
}

PRIVATE void APP_CAPSENSE_vResetPower(void)
{
	DBG_vPrintf(TRACE_APP_CAPSENSE,"APP_CAPSENSE: Reset power \n");
	APP_CAPSENSE_vPowerOff();
	//Warm up
	volatile uint32_t u32Delay = 0x0001FFFF;				// sensor need at least 15ms for first communication
	while(u32Delay--);

	APP_CAPSENSE_vPowerOn();
	vAHI_WatchdogRestart();
	//Warm up
	u32Delay = 0x000FFFFF;				// sensor need at least 15ms for first communication
	while(u32Delay--);
}

PRIVATE void vCallbackTimerScanCapSense(HW_TIMER_tsTimerEvent_t sEvent)
{
	HW_TIMER_vStop(HW_TIMER_SCAN_CAPSENSE);
	bNeedToScanCapsense = TRUE;
	HW_TIMER_vStartSingleShot(HW_TIMER_SCAN_CAPSENSE, TIME_SCAN_CAPSENSE, 0);
}

PRIVATE bool_t bIsConfigAlready(void)
{
	uint16_t byte_read = 1;
	PDM_teStatus status;

	status = PDM_bDoesDataExist(PDM_ID_CAPSENSE_ALREADY_CONFIGURE, &byte_read);

	DBG_vPrintf(1,"bIsTouchSensorConfigureBefore return %d, byte_read %d\r\n", status, byte_read);

	if( (status != TRUE) || (byte_read != 1) )
	{
		return FALSE;
	}
	return TRUE;
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
