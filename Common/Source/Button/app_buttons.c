/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_button.h
 *
 * DESCRIPTION:        Button events
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "app_buttons.h"
#include "drv_buttons.h"
#include "dbg.h"
#include "my_define.h"
#include "app_main.h"
#include "app_events.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_APP_BUTTON
    #define TRACE_APP_BUTTON            FALSE
#else
    #define TRACE_APP_BUTTON            TRUE
#endif
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vCallbackDrvButtons(DRV_BUTTONS_tsEvent_t sEvent);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
extern PUBLIC tszQueue APP_msgAppEvents;
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_BUTTONS_vInitialize(void)
{
	if(DRV_BUTTONS_bInitialize(vCallbackDrvButtons))
	{
		DBG_vPrintf(TRACE_APP_BUTTON, "APP_BUTTONS: Button Initialize success \n");
	}
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vCallbackDrvButtons(DRV_BUTTONS_tsEvent_t sEvent)
{
	APP_tsEvent sAppEvent;
	sAppEvent.eType = APP_E_EVENT_NONE;

	if(sEvent.u8Button == PIN_IN_FUNC_BUTTON)
	{
		if(sEvent.eEventType == DRV_E_BUTTON_EVENT_DOWN)
		{

		}
		else if(sEvent.eEventType == DRV_E_BUTTON_EVENT_UP)
		{
			if(sEvent.u8TimeHold < 5)
			{
				sAppEvent.eType = APP_E_EVENT_FUNC_BUTTON_UP_SHORTER_5S;
			}
		}
		else if(sEvent.eEventType == DRV_E_BUTTON_EVENT_HOLD)
		{
			if(sEvent.u8TimeHold == 5)
			{
				sAppEvent.eType = APP_E_EVENT_FUNC_BUTTON_HOLD_5S;
			}
		}
	}
#if (defined TEST_SCENE_SWITCH) | (defined TEST_NORMAL_SWITCH)
	else if(sEvent.u8Button == PIN_IN_BUTTON_TEST_1)
	{
		if(sEvent.eEventType == DRV_E_BUTTON_EVENT_DOWN)
		{
			sAppEvent.eType = APP_E_EVENT_CAP_SENSE_PRESSED;
			sAppEvent.uEvent.sSwitch.u8Switch = 0;
		}
	}
	else if(sEvent.u8Button == PIN_IN_BUTTON_TEST_2)
	{
		if(sEvent.eEventType == DRV_E_BUTTON_EVENT_DOWN)
		{
			sAppEvent.eType = APP_E_EVENT_CAP_SENSE_PRESSED;
			sAppEvent.uEvent.sSwitch.u8Switch = 1;
		}
	}
	else if(sEvent.u8Button == PIN_IN_BUTTON_TEST_3)
	{
		if(sEvent.eEventType == DRV_E_BUTTON_EVENT_DOWN)
		{
			sAppEvent.eType = APP_E_EVENT_CAP_SENSE_PRESSED;
			sAppEvent.uEvent.sSwitch.u8Switch = 2;
		}
	}
	else if(sEvent.u8Button == PIN_IN_BUTTON_TEST_4)
	{
		if(sEvent.eEventType == DRV_E_BUTTON_EVENT_DOWN)
		{
			sAppEvent.eType = APP_E_EVENT_CAP_SENSE_PRESSED;
			sAppEvent.uEvent.sSwitch.u8Switch = 3;
		}
	}
#endif /*TEST_SCENE_SWITCH*/
	if(sAppEvent.eType != APP_E_EVENT_NONE)
	{
		if(ZQ_bQueueSend(&APP_msgAppEvents, &sAppEvent) == FALSE)
		{
			DBG_vPrintf(TRACE_APP_BUTTON, "APP_BUTTONS: Failed to post Event %d \n", sAppEvent.eType);
		}
	}
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
