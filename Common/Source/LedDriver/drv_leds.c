/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_leds.c
 *
 * DESCRIPTION:        API for control led
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "drv_leds.h"
#include "AppHardwareApi_JN516x.h"
#include "dbg.h"
#include "ZTimer.h"
#include "app_common.h"
#include "my_define.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_DRV_LEDS
    #define TRACE_DRV_LEDS              (TRUE)
#else   /* DEBUG_DRV_LEDS */
    #define TRACE_DRV_LEDS              (FALSE)
#endif  /* DEBUG_DRV_LEDS */

#define PWM_CYCLE						(20)		//ms T=20ms => f = 50Hz

#define OUTPUT_HIGH						(TRUE)
#define OUTPUT_LOW						(FALSE)

#ifdef PIR_DEVICE
	#define OUTPUT_TO_LED_ON			(OUTPUT_LOW)
#else
	#define OUTPUT_TO_LED_ON			(OUTPUT_HIGH)
#endif
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef enum
{
	E_BLINK_TIMER_NONE = 0,
	E_BLINK_TIMER_ON,
	E_BLINK_TIMER_OFF
}DRV_LEDS_teBlinkTimerRuning;

typedef enum
{
	E_MAIN_BLINK_LED = 0,
	E_ALTERNATE_BLINK_LED,
	SIZE_OF_BLINK_LED_MASK
}DRV_LEDS_teLedMaskIndex;

typedef struct
{
	uint32_t				u32PinMask[SIZE_OF_BLINK_LED_MASK];		//[1] main led blink, [2] alternate led blink
	int16_t 				i16NumberBlink;
	uint16_t 				u16TimeOn;			//ms
	uint16_t 				u16TimeOff;			//ms
	fnCbAfterBlink			fnCallback;
	void                	*pParams;

	DRV_LEDS_teBlinkTimerRuning	eTimerRun;
	uint8_t					u8TimerIndex;
	uint8_t					u8TimerBlink;
}DRV_LEDS_tsBlinkGroupLeds;

typedef struct
{
	uint8						u8LedLevel;
	uint32_t					u32DimLedPinMask;

	DRV_LEDS_tsBlinkGroupLeds	sBlinkGroupLeds[MAX_NUMBER_GROUP_LED_BLINK];
}DRV_LEDS_tsParams;

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
/* Dim led functions */
PRIVATE void vStartDimTimer(void);
PRIVATE void vStopDimTimer(void);
PRIVATE void DRV_LEDS_vcbDimTimer(HW_TIMER_tsTimerEvent_t sEvent);
PRIVATE void vTurnOnAllDimLeds(void);
PRIVATE void vTurnOffAllDimLeds(void);
PRIVATE void vAddLedTo_ArrayLedDim(uint32_t u32LedPinMask);
PRIVATE void vRemoveLedOutOf_ArrayLedDim(uint32_t u32LedPinMask);
/* Blink led functions */
PRIVATE void vcbTimerBlinkLed(void *pvParam);
PRIVATE void vStartBlinkTimer(uint8_t u8TimerIndex, DRV_LEDS_teBlinkTimerRuning	eTimerRun);
PRIVATE void vStopBlinkTimer(uint8_t u8TimerIndex);
PRIVATE void vRemoveLedInOtherGroupThatHaveInThisGroup(uint8_t u8GroupLedsIndex);
PRIVATE void vAddBlinkLedTo_ArrayLedDim(uint8_t u8GroupLedsIndex, DRV_LEDS_teLedMaskIndex eLedMaskIndex);
PRIVATE void vRemoveBlinkLedTo_ArrayLedDim(uint8_t u8GroupLedsIndex, DRV_LEDS_teLedMaskIndex eLedMaskIndex);
PRIVATE void vTurnOnAllBlinkLeds(uint8_t u8GroupLedsIndex, DRV_LEDS_teLedMaskIndex eLedMaskIndex);
PRIVATE void vTurnOffAllBlinkLeds(uint8_t u8GroupLedsIndex, DRV_LEDS_teLedMaskIndex eLedMaskIndex);
/* Common functions */
PRIVATE void vDioSetOutput(uint32_t	u32On, uint32_t u32Off);
PRIVATE void vTurnLed_On(uint32_t u32LedPinMask);
PRIVATE void vTurnLed_Off(uint32_t u32LedPinMask);
PRIVATE bool_t bIsLedPinMaskAlreadyInitialized(uint32_t u32LedPinMask);
PRIVATE void vRemoveThisLedInBlinkLedGroupThatContainsIt(uint32_t	u32LedPinMask);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE DRV_LEDS_tsParams 	sLedParams;
PRIVATE uint32_t			u32LedInitializedMask;

bool_t						bDimTimerStopped = TRUE;

uint16_t					u16Hi;		// for PWM refer: JN-UG-3087 p80
uint16_t					u16Lo;		// for PWM refer: JN-UG-3087 p80
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC bool_t DRV_LEDS_bInitialize(
		uint32_t				u32LedPinMask,
		uint8					u8LedLevel
){
	uint8_t i,j;
	// initialize dim led
	sLedParams.u8LedLevel = u8LedLevel;
	if(u8LedLevel == LED_LEVEL_0)
	{
		sLedParams.u8LedLevel = LED_LEVEL_0;
	}
	else if((u8LedLevel > LED_LEVEL_0) && (u8LedLevel < LED_LEVEL_MIN))
	{
		sLedParams.u8LedLevel = LED_LEVEL_MIN;
	}
	else if(sLedParams.u8LedLevel > LED_LEVEL_100)
	{
		sLedParams.u8LedLevel = LED_LEVEL_100;
	}
	u16Lo = PWM_CYCLE;
	u16Hi = u16Lo - (sLedParams.u8LedLevel*PWM_CYCLE)/LED_LEVEL_100;

	sLedParams.u32DimLedPinMask = 0;

	// initialize blink led
	for(i = 0; i < MAX_NUMBER_GROUP_LED_BLINK; i++)
	{
		for(j = 0; j < SIZE_OF_BLINK_LED_MASK; j++)
		{
			sLedParams.sBlinkGroupLeds[i].u32PinMask[j]	= 0;
		}
		sLedParams.sBlinkGroupLeds[i].i16NumberBlink 	= 0;
		sLedParams.sBlinkGroupLeds[i].fnCallback 		= NULL;
		sLedParams.sBlinkGroupLeds[i].pParams	 		= NULL;
		sLedParams.sBlinkGroupLeds[i].u16TimeOff 		= 0;
		sLedParams.sBlinkGroupLeds[i].u16TimeOn 		= 0;
	}

	/* Initialize led information */
	u32LedInitializedMask = u32LedPinMask;
	//Set led pin as output
	vAHI_DioSetDirection(0, u32LedInitializedMask);
	//Turn off all the LEDs
	vAHI_DioSetOutput(0, u32LedInitializedMask);
	// Check special DO_0,DO_1 was required to initialize or not
	if(u32LedInitializedMask >> DO_0)
	{
		bAHI_DoEnableOutputs(TRUE);
		// Turn off led DO_0,DO_1
		vAHI_DoSetDataOut(0, u32LedInitializedMask >> DO_0);
	}
	// Initialize blink timer
	for(i = 0; i < MAX_NUMBER_GROUP_LED_BLINK; i ++)
	{
		sLedParams.sBlinkGroupLeds[i].u8TimerIndex = i;
		sLedParams.sBlinkGroupLeds[i].eTimerRun = E_BLINK_TIMER_NONE;
		(void) ZTIMER_eOpen(&sLedParams.sBlinkGroupLeds[i].u8TimerBlink,
					vcbTimerBlinkLed,
					&sLedParams.sBlinkGroupLeds[i].u8TimerIndex,
					ZTIMER_FLAG_PREVENT_SLEEP);
	}

	/* Dim timer */
	HW_TIMER_bEnable(
			HW_TIMER_DIM_LED,
			FALSE,
			E_HW_TIMER_PERIOD_RANGE_64US_TO_4S,
			TRUE,					// enable rising interrupt
			TRUE,					// enable falling interrupt
			DRV_LEDS_vcbDimTimer,
			NULL,
			E_TIMER_OUTPUT_NONE,
			0
	);

	if((sLedParams.u8LedLevel != LED_LEVEL_0) && (sLedParams.u8LedLevel != LED_LEVEL_100) )
	{
		vStartDimTimer();
	}

	DBG_vPrintf(TRACE_DRV_LEDS,
			"DRV-LED: Led Initialize finished Led level: %d, cycle = %d Timer = %d \n",
			sLedParams.u8LedLevel, u16Lo, HW_TIMER_DIM_LED);

    return TRUE;
}

PUBLIC void DRV_LEDS_vBlinkLedsAlternately(
		uint32_t				u32LedPinMask,
		uint32_t				u32LedPinMask2,
		int16_t 				i16NumberBlink,
		int16_t 				i16BlinkDuration,		//ms
		uint16_t 				u16TimeOn,				//ms
		uint16_t 				u16TimeOff,				//ms
		fnCbAfterBlink			fnCallback,
		void	               	*pParams
)
{
	uint8_t i;
	//make sure all led mask are different
	bool_t	bBlinkLedAlternately = TRUE;
	uint32_t u32MaskDiff = u32LedPinMask & u32LedPinMask2;
	if(u32MaskDiff)
	{
		bBlinkLedAlternately = FALSE;
		DBG_vPrintf(TRACE_DRV_LEDS,
						"DRV-LED: Some LEDs in 2 led pin mask are duplicated: %8x \n", u32MaskDiff);
	}

	//Check whether all led in u8ArrLedPin,u8ArrLedPin2 already initialized or not
	bool_t	bAllLedAlreadyInit = TRUE;
	if(!bIsLedPinMaskAlreadyInitialized(u32LedPinMask))
	{
		bAllLedAlreadyInit = FALSE;
		DBG_vPrintf(TRACE_DRV_LEDS,
								"DRV-LED: Some LEDs in u32LedPinMask are not initialized: %8x \n", u32LedPinMask);
	}
	if(!bIsLedPinMaskAlreadyInitialized(u32LedPinMask2))
	{
		bAllLedAlreadyInit = FALSE;
		DBG_vPrintf(TRACE_DRV_LEDS,
				"DRV-LED: Some LEDs in u32LedPinMask2 are not initialized: %8x \n", u32LedPinMask2);
	}
	if(!bAllLedAlreadyInit)
		return;

	/* Initialize Group led */
	uint8_t	u8GroupLedsIndex = MAX_NUMBER_GROUP_LED_BLINK;
	for(i = 0; i < MAX_NUMBER_GROUP_LED_BLINK; i++)
	{
		if(sLedParams.sBlinkGroupLeds[i].eTimerRun == E_BLINK_TIMER_NONE)
		{
			u8GroupLedsIndex = i;
			break;
		}
	}
	if(u8GroupLedsIndex == MAX_NUMBER_GROUP_LED_BLINK)
	{
		DBG_vPrintf(TRACE_DRV_LEDS,
						"DRV-LED: Error: Group led array are already full \n");
		return;
	}

	sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u32PinMask[E_MAIN_BLINK_LED] = u32LedPinMask;
	if(bBlinkLedAlternately)
	{
		sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u32PinMask[E_ALTERNATE_BLINK_LED] = u32LedPinMask2;
	}

	if(i16NumberBlink > 0)
	{
		sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].i16NumberBlink = i16NumberBlink;
	}
	else
	{
		if(i16BlinkDuration > 0)
		{
			sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].i16NumberBlink = i16BlinkDuration/(u16TimeOn + u16TimeOff);
			if(sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].i16NumberBlink == 0)
			{
				DBG_vPrintf(TRACE_DRV_LEDS,
						"DRV-LED: Error: No need to blink: Cycle = u16TimeOn + u16TimeOff >  i16BlinkDuration (%d > %d) \n",
						(u16TimeOn + u16TimeOff), i16BlinkDuration);
				// Call callback function
				if(NULL !=	fnCallback)
				{
					fnCallback(pParams);
				}
				return;
			}
		}
		else
		{
			sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].i16NumberBlink = LED_BLINK_FOREVER;
		}
	}
	sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u16TimeOn 		= u16TimeOn;
	sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u16TimeOff 	= u16TimeOff;

	sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].fnCallback		= fnCallback;
	sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].pParams		= pParams;

	//Remove all led in other group that has in this group
	vRemoveLedInOtherGroupThatHaveInThisGroup(u8GroupLedsIndex);

	//Start timer to blink led
	vAddBlinkLedTo_ArrayLedDim(u8GroupLedsIndex, E_MAIN_BLINK_LED);
	vRemoveBlinkLedTo_ArrayLedDim(u8GroupLedsIndex, E_ALTERNATE_BLINK_LED);
	if(bDimTimerStopped)
	{
		if(sLedParams.u8LedLevel != LED_LEVEL_0)
		{
			vTurnOnAllBlinkLeds(u8GroupLedsIndex, E_MAIN_BLINK_LED);
		}
		vTurnOffAllBlinkLeds(u8GroupLedsIndex, E_ALTERNATE_BLINK_LED);
	}

	if((bDimTimerStopped) && (sLedParams.u8LedLevel != LED_LEVEL_0) && (sLedParams.u8LedLevel != LED_LEVEL_100))
	{
		vStartDimTimer();
	}
	vStartBlinkTimer(u8GroupLedsIndex, E_BLINK_TIMER_ON);

	DBG_vPrintf(TRACE_DRV_LEDS, "DRV-LED: start led[%d] Alternately = %d, Time on = %d, Time off = %d \n",
			u8GroupLedsIndex, bBlinkLedAlternately,
			sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u16TimeOn,
			sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u16TimeOff);
}

PUBLIC void DRV_LEDS_vStopBlinkLeds(
		bool_t					bStopBlinkAllLeds,
		uint32_t				u32LedPinMask
)
{
	DBG_vPrintf(TRACE_DRV_LEDS, "DRV-LED: Stop blink led \n");

	uint8 i,j;
	if(bStopBlinkAllLeds)
	{
		for(i = 0; i < MAX_NUMBER_GROUP_LED_BLINK; i++)
		{
			vStopBlinkTimer(i);
			sLedParams.sBlinkGroupLeds[i].eTimerRun = E_BLINK_TIMER_NONE;
			for(j = 0; j < SIZE_OF_BLINK_LED_MASK; j++)
			{
				sLedParams.sBlinkGroupLeds[i].u32PinMask[j]	= 0;
			}
			sLedParams.sBlinkGroupLeds[i].i16NumberBlink 	= 0;
			sLedParams.sBlinkGroupLeds[i].fnCallback 		= NULL;
			sLedParams.sBlinkGroupLeds[i].pParams	 		= NULL;
			sLedParams.sBlinkGroupLeds[i].u16TimeOff 		= 0;
			sLedParams.sBlinkGroupLeds[i].u16TimeOn 		= 0;
		}
	}
	else
	{
		vRemoveThisLedInBlinkLedGroupThatContainsIt(u32LedPinMask);
		vTurnLed_Off(u32LedPinMask);
	}
}
PUBLIC void DRV_LEDS_vSetLevel(
		uint8					u8LedLevel
)
{
	sLedParams.u8LedLevel = u8LedLevel;
	if(u8LedLevel == LED_LEVEL_0)
	{
		sLedParams.u8LedLevel = LED_LEVEL_0;
	}
	else if((u8LedLevel > LED_LEVEL_0) && (u8LedLevel < LED_LEVEL_MIN))
	{
		sLedParams.u8LedLevel = LED_LEVEL_MIN;
	}
	else if(sLedParams.u8LedLevel > LED_LEVEL_100)
	{
		sLedParams.u8LedLevel = LED_LEVEL_100;
	}
	u16Lo = PWM_CYCLE;
	u16Hi = u16Lo - (sLedParams.u8LedLevel*PWM_CYCLE)/LED_LEVEL_100;

	vStopDimTimer();
	if(sLedParams.u8LedLevel == LED_LEVEL_0)
	{
		vTurnOffAllDimLeds();
	}
	else if(sLedParams.u8LedLevel == LED_LEVEL_100)
	{
		vTurnOnAllDimLeds();
	}
	else
	{
		vStartDimTimer();
	}

	DBG_vPrintf(TRACE_DRV_LEDS, "DRV-LED: Set led level: %d ,  u16Hi %d , PwmOn %d \n", u8LedLevel, u16Hi, u16Lo - u16Hi);
}

PUBLIC void DRV_LEDS_vOn(
		uint32_t				u32LedPinMask
)
{
	DBG_vPrintf(TRACE_DRV_LEDS, "DRV-LED: Turn on Led pin mask %8x \n", u32LedPinMask);
	if(!bIsLedPinMaskAlreadyInitialized(u32LedPinMask))
	{
		DBG_vPrintf(TRACE_DRV_LEDS, "DRV-LED: Please initialize pin[%] as led pin first \n", u32LedPinMask);
		return;
	}

	vAddLedTo_ArrayLedDim(u32LedPinMask);

	vRemoveThisLedInBlinkLedGroupThatContainsIt(u32LedPinMask);

	if(sLedParams.u8LedLevel == LED_LEVEL_0)
	{
		vTurnLed_Off(u32LedPinMask);
	}
	else
	{
		vTurnLed_On(u32LedPinMask);
	}


	if(bDimTimerStopped && sLedParams.u8LedLevel != LED_LEVEL_0 && sLedParams.u8LedLevel != LED_LEVEL_100)
	{
		vStartDimTimer();
	}
}

PUBLIC void DRV_LEDS_vOff(
		uint32_t				u32LedPinMask
)
{
	DBG_vPrintf(TRACE_DRV_LEDS, "DRV-LED: Turn off Led pin %8x \n", u32LedPinMask);
	if(!bIsLedPinMaskAlreadyInitialized(u32LedPinMask))
	{
		DBG_vPrintf(TRACE_DRV_LEDS, "DRV-LED: Please initialize led pin as led pin first \n");
		return;
	}

	vRemoveLedOutOf_ArrayLedDim(u32LedPinMask);

	vRemoveThisLedInBlinkLedGroupThatContainsIt(u32LedPinMask);

	vTurnLed_Off(u32LedPinMask);
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void DRV_LEDS_vcbDimTimer(HW_TIMER_tsTimerEvent_t sEvent)
{

	if(sEvent.eIntType == E_TIMER_INT_RISING_EDGE)
	{
		vTurnOnAllDimLeds();
	}
	else if(sEvent.eIntType ==  E_TIMER_INT_FALLING_EDGE)
	{
		vTurnOffAllDimLeds();
	}
}

PRIVATE void vStartDimTimer(void)
{
	// Start Hardware Timer0 as Base time to Blink led and PWM led
	HW_TIMER_vStartRepeat(HW_TIMER_DIM_LED, u16Hi, u16Lo);
    bDimTimerStopped = FALSE;
}

PRIVATE void vStopDimTimer(void)
{
	if(bDimTimerStopped) return;
	HW_TIMER_vStop(HW_TIMER_DIM_LED);
	bDimTimerStopped = TRUE;
}

PRIVATE void vcbTimerBlinkLed(void *pvParam)
{
	if (NULL == pvParam) return;
	uint8_t u8GroupLedsIndex = *((uint8_t*)pvParam);
	vStopBlinkTimer(u8GroupLedsIndex);

	if(sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].i16NumberBlink == 0)
	{
		vRemoveBlinkLedTo_ArrayLedDim(u8GroupLedsIndex, E_MAIN_BLINK_LED);
		vRemoveBlinkLedTo_ArrayLedDim(u8GroupLedsIndex, E_ALTERNATE_BLINK_LED);

		vTurnOffAllBlinkLeds(u8GroupLedsIndex, E_MAIN_BLINK_LED);
		vTurnOffAllBlinkLeds(u8GroupLedsIndex, E_ALTERNATE_BLINK_LED);

		sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].eTimerRun = E_BLINK_TIMER_NONE;
		if(NULL != sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].fnCallback)
		{
			// Callback function
			sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].fnCallback(sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].pParams);
		}
		return;
	}

	/* Start timer again */
	if(E_BLINK_TIMER_ON == sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].eTimerRun)
	{
		if(sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].i16NumberBlink != LED_BLINK_FOREVER)
		{
			sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].i16NumberBlink --;
		}

		vRemoveBlinkLedTo_ArrayLedDim(u8GroupLedsIndex, E_MAIN_BLINK_LED);
		vAddBlinkLedTo_ArrayLedDim(u8GroupLedsIndex, E_ALTERNATE_BLINK_LED);

		vTurnOffAllBlinkLeds(u8GroupLedsIndex, E_MAIN_BLINK_LED);
		if(bDimTimerStopped && (sLedParams.u8LedLevel != LED_LEVEL_0))
		{
			vTurnOnAllBlinkLeds(u8GroupLedsIndex, E_ALTERNATE_BLINK_LED);
		}

		vStartBlinkTimer(u8GroupLedsIndex, E_BLINK_TIMER_OFF);
	}
	else if(E_BLINK_TIMER_OFF == sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].eTimerRun)
	{
		vAddBlinkLedTo_ArrayLedDim(u8GroupLedsIndex, E_MAIN_BLINK_LED);
		vRemoveBlinkLedTo_ArrayLedDim(u8GroupLedsIndex, E_ALTERNATE_BLINK_LED);

		vTurnOffAllBlinkLeds(u8GroupLedsIndex, E_ALTERNATE_BLINK_LED);
		if(bDimTimerStopped && (sLedParams.u8LedLevel != LED_LEVEL_0))
		{
			vTurnOnAllBlinkLeds(u8GroupLedsIndex, E_MAIN_BLINK_LED);
		}

		vStartBlinkTimer(u8GroupLedsIndex, E_BLINK_TIMER_ON);
	}

}

PRIVATE void vStartBlinkTimer(uint8_t u8TimerIndex, DRV_LEDS_teBlinkTimerRuning	eTimerRun)
{
	switch(eTimerRun)
	{
	case E_BLINK_TIMER_ON:
		ZTIMER_eStart(sLedParams.sBlinkGroupLeds[u8TimerIndex].u8TimerBlink, sLedParams.sBlinkGroupLeds[u8TimerIndex].u16TimeOn);
		sLedParams.sBlinkGroupLeds[u8TimerIndex].eTimerRun = E_BLINK_TIMER_ON;
		break;

	case E_BLINK_TIMER_OFF:
		ZTIMER_eStart(sLedParams.sBlinkGroupLeds[u8TimerIndex].u8TimerBlink, sLedParams.sBlinkGroupLeds[u8TimerIndex].u16TimeOff);
		sLedParams.sBlinkGroupLeds[u8TimerIndex].eTimerRun = E_BLINK_TIMER_OFF;
			break;
	default:
		break;
	}
}

PRIVATE void vStopBlinkTimer(uint8_t u8TimerIndex)
{
	ZTIMER_eStop(sLedParams.sBlinkGroupLeds[u8TimerIndex].u8TimerBlink);
}

PRIVATE void vRemoveLedInOtherGroupThatHaveInThisGroup(uint8_t u8GroupLedsIndex)
{
	uint8 i,j,k;
	for(i = 0; i < MAX_NUMBER_GROUP_LED_BLINK; i++)
	{
		if(i != u8GroupLedsIndex)
		{
			for(j = 0; j < SIZE_OF_BLINK_LED_MASK; j++)
				for(k = 0; k < SIZE_OF_BLINK_LED_MASK; k++)
					sLedParams.sBlinkGroupLeds[i].u32PinMask[j] &= ~sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u32PinMask[k];
		}
	}
}

PRIVATE void vAddBlinkLedTo_ArrayLedDim(uint8_t u8GroupLedsIndex, DRV_LEDS_teLedMaskIndex eLedMaskIndex)
{
	sLedParams.u32DimLedPinMask |= sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u32PinMask[eLedMaskIndex];
}

PRIVATE void vRemoveBlinkLedTo_ArrayLedDim(uint8_t u8GroupLedsIndex, DRV_LEDS_teLedMaskIndex eLedMaskIndex)
{
	sLedParams.u32DimLedPinMask &= ~sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u32PinMask[eLedMaskIndex];
}

PRIVATE void vTurnLed_On(uint32_t u8LedPinMask)
{
	vDioSetOutput(u8LedPinMask, 0UL);
}

PRIVATE void vTurnLed_Off(uint32_t u8LedPinMask)
{
	vDioSetOutput(0UL, u8LedPinMask);
}

PRIVATE void vDioSetOutput(uint32_t	u32On, uint32_t u32Off)
{
	if(OUTPUT_TO_LED_ON == OUTPUT_HIGH)
	{
		vAHI_DioSetOutput(u32On, u32Off);
		// Turn off led DO_0,DO_1
		vAHI_DoSetDataOut(u32On >> DO_0, u32Off >> DO_0);
	}
	else
	{
		vAHI_DioSetOutput(u32Off, u32On);
		// Turn off led DO_0,DO_1
		vAHI_DoSetDataOut(u32Off >> DO_0, u32On >> DO_0);
	}
}

PRIVATE void vTurnOnAllDimLeds(void)
{
	vTurnLed_On(sLedParams.u32DimLedPinMask);
}

PRIVATE void vTurnOffAllDimLeds(void)
{
	vTurnLed_Off(sLedParams.u32DimLedPinMask);
}

PRIVATE void vTurnOnAllBlinkLeds(uint8_t u8GroupLedsIndex, DRV_LEDS_teLedMaskIndex eLedMaskIndex)
{
	vTurnLed_On(sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u32PinMask[eLedMaskIndex]);
}

PRIVATE void vTurnOffAllBlinkLeds(uint8_t u8GroupLedsIndex, DRV_LEDS_teLedMaskIndex eLedMaskIndex)
{
	vTurnLed_Off(sLedParams.sBlinkGroupLeds[u8GroupLedsIndex].u32PinMask[eLedMaskIndex]);
}

PRIVATE bool_t bIsLedPinMaskAlreadyInitialized(uint32_t u32LedPinMask)
{
	return ((u32LedInitializedMask & u32LedPinMask) == u32LedPinMask);
}

PRIVATE void vAddLedTo_ArrayLedDim(uint32_t u32LedPinMask)
{
	sLedParams.u32DimLedPinMask |= u32LedPinMask;
}

PRIVATE void vRemoveLedOutOf_ArrayLedDim(uint32_t u32LedPinMask)
{
	sLedParams.u32DimLedPinMask &= ~u32LedPinMask;
}

PRIVATE void vRemoveThisLedInBlinkLedGroupThatContainsIt(uint32_t	u32LedPinMask)
{
	uint8 i,j;
	for(i = 0; i < MAX_NUMBER_GROUP_LED_BLINK; i++)
		for(j = 0; j < SIZE_OF_BLINK_LED_MASK; j++)
		{
			sLedParams.sBlinkGroupLeds[i].u32PinMask[j] &= ~(u32LedPinMask);
		}
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
