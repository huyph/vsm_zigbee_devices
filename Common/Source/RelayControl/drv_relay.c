/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_relay.c
 *
 * DESCRIPTION:        Control relay
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "drv_relay.h"
#include "AppHardwareApi_JN516x.h"
#include "dbg.h"
#include "app_common.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_DRV_RELAY
    #define TRACE_DRV_RELAY         (TRUE)
#else   /* DEBUG_DRV_RELAY */
    #define TRACE_DRV_RELAY         (FALSE)
#endif  /* DEBUG_DRV_RELAY */

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vCallbackRelayOnOff(void* pvParams);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void DRV_RELAY_vInitialize(
	bool_t			bInitMux
)
{
	DRV_COIL_vInitialize(bInitMux);
}

PUBLIC void DRV_RELAY_vOn(
	DRV_RELAY_tsRelay_t		*sRelay
)
{
	sRelay->sCoilSet.fnCallback = vCallbackRelayOnOff;
	sRelay->sCoilSet.pvParams 	= sRelay;

	// Make sure coil Reset already turn off
	DRV_COIL_vOff(&sRelay->sCoilRst);
	// Turn on coil Set
	DRV_COIL_vOn(&sRelay->sCoilSet);
}

PUBLIC void DRV_RELAY_vOff(
	DRV_RELAY_tsRelay_t		*sRelay
)
{
	sRelay->sCoilRst.fnCallback = vCallbackRelayOnOff;
	sRelay->sCoilRst.pvParams 	= sRelay;

	// Make sure coil set already turn off
	DRV_COIL_vOff(&sRelay->sCoilSet);
	// Turn on coil Reset
	DRV_COIL_vOn(&sRelay->sCoilRst);
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vCallbackRelayOnOff(void* pvParams)
{
	if(NULL == pvParams)	return;
	DRV_RELAY_tsRelay_t sRelay = *(DRV_RELAY_tsRelay_t*)pvParams;
	DBG_vPrintf(TRACE_DRV_RELAY,"DRV_RELAY: vCallbackRelayOnOff() Relay %d \n", sRelay.u8RelayNumber);

	if(sRelay.fnCallback != NULL)
	{
		sRelay.fnCallback(sRelay.pvParams);
	}
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
