/*****************************************************************************
 *
 * MODULE:             JN-AN-1220 ZLO Sensor Demo
 *
 * COMPONENT:          app_device_config.h
 *
 * DESCRIPTION:        DK4 (DR1175) Button Press detection (Implementation)
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5168, JN5179].
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2017. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_DEVICE_CONFIG_H_
#define APP_DEVICE_CONFIG_H_

/****************************************************************************/
/***        Include Files                                                 ***/
/****************************************************************************/
#include <jendefs.h>
#include "zcl_options.h"
#include "Basic.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef CLD_BAS_ATTR_MAN_SPEC_LED_INTENSITY
#define DEVICE_CONFIG_LED_INTENSITY_BIT     (1 << 0)
#endif

#ifdef CLD_BAS_ATTR_MAN_SPEC_VIBRATION_INTENSITY
#define DEVICE_CONFIG_VIBRATION_INTENSITY_BIT  (1 << 1)
#endif

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

typedef struct
{
    uint8 u8LedIntensity;
    uint8 u8VibrationIntensity;
}tsDeviceConfigParams;

typedef BOOL_T (*tfpDeviceConfigCb) (uint8 u8UpdateMask,
	 	 	                       tsDeviceConfigParams* psDeviceConfigParam);

typedef struct
{
	tsDeviceConfigParams sDeviceConfigParams;
	tfpDeviceConfigCb pCallbackFunction;
}tsDeviceConfig;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC BOOL_T bDeviceConfig_UpdateConfig(uint8 u8ParamId,
										 void* pParam);
PUBLIC BOOL_T bDeviceConfig_LoadConfig(tsCLD_Basic* psBasicServerCluster);
PUBLIC BOOL_T bDeviceConfig_RegisterCallback(tfpDeviceConfigCb pfCallBack);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_DEVICE_CONFIG_H_*/
