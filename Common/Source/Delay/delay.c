/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          delay.c
 *
 * DESCRIPTION:        This for delay
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "delay.h"
#include "ZTimer.h"
#include "dbg.h"
#include "my_define.h"
#include "hw_timer.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_DELAY
    #define TRACE_DELAY              (TRUE)
#else   /* DEBUG_DELAY */
    #define TRACE_DELAY              (FALSE)
#endif  /* DEBUG_DELAY */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef struct
{
	uint8_t 		u8TimerID;
	uint8_t			u8TimerIndex;
	bool_t			bRunning;
	fnCbAfterDelay	fnCallback;
	void	        *pParams;
}DELAY_tsTimerDelay;

typedef struct
{
	uint8_t			u8Index;
	bool_t			bRunning;
	fnCbAfterDelay	fnCallback;
	void 			*pvParams;
}DELAY_tsHWTimerDelay;
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vCallbackDelayTimer(void *pvParam);
PRIVATE void vCallbackDelayHWTimer(HW_TIMER_tsTimerEvent_t sEvent);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE DELAY_tsTimerDelay		sTimerDelay[NUMBER_DELAY_TIMER_SUPPORT];
PRIVATE DELAY_tsHWTimerDelay	sHWTimerDelay[NUMBER_OF_HW_TIMER_DELAY];
PRIVATE uint8_t					u8ArrHWTimerDelay[NUMBER_OF_HW_TIMER_DELAY] = {
		HW_TIMER_DELAY,
//		HW_TIMER_DELAY_2
};
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void DELAY_vInitialize(void)
{
	uint8_t i;
	// open Timer
	for(i = 0; i < NUMBER_DELAY_TIMER_SUPPORT; i++)
	{
		sTimerDelay[i].u8TimerIndex 	= i;
		sTimerDelay[i].bRunning 		= FALSE;
		sTimerDelay[i].fnCallback 		= NULL;
		sTimerDelay[i].pParams			= NULL;

		(void) ZTIMER_eOpen(&sTimerDelay[i].u8TimerID,
							vCallbackDelayTimer,
							&sTimerDelay[i].u8TimerIndex,
							ZTIMER_FLAG_PREVENT_SLEEP);
	}
	// Initialize delay using hardware timer
	for(i = 0; i < NUMBER_OF_HW_TIMER_DELAY; i++)
	{
		sHWTimerDelay[i].u8Index	= i;
		sHWTimerDelay[i].bRunning 	= FALSE;
		sHWTimerDelay[i].fnCallback	= NULL;
		sHWTimerDelay[i].pvParams 	= NULL;

		HW_TIMER_bEnable(
				u8ArrHWTimerDelay[i],
				FALSE,
				E_HW_TIMER_PERIOD_RANGE_64US_TO_4S,
				TRUE,
				FALSE,
				vCallbackDelayHWTimer,
				&sHWTimerDelay[i].u8Index,
				E_TIMER_OUTPUT_NONE,
				0
		);
	}
}

PUBLIC void DELAY_vStart(
		uint16_t				u16TimeDelay,		//ms
		fnCbAfterDelay			fnCallback,
		void	               	*pParams,
		uint8_t					*u8TimerIndex
)
{
	uint8_t	i;
	uint8_t u8Index = NUMBER_DELAY_TIMER_SUPPORT;
	/* Find free timer */
	for(i = 0; i < NUMBER_DELAY_TIMER_SUPPORT; i++)
	{
		if(!sTimerDelay[i].bRunning)
		{
			u8Index = i;
			break;
		}
	}
	if(u8Index == NUMBER_DELAY_TIMER_SUPPORT)
	{
		DBG_vPrintf(TRACE_DELAY, "DELAY: All timers are running now \n");
		u8Index = 0;
		ZTIMER_eStop(sTimerDelay[u8Index].u8TimerID);
	}

	sTimerDelay[u8Index].bRunning = TRUE;
	sTimerDelay[u8Index].fnCallback = fnCallback;
	sTimerDelay[u8Index].pParams 	= pParams;

	*u8TimerIndex = u8Index;

	ZTIMER_eStart(sTimerDelay[u8Index].u8TimerID, u16TimeDelay);

	DBG_vPrintf(TRACE_DELAY, "DELAY: Start Delay timerIndex [%d] in [%d]ms \n", *u8TimerIndex, u16TimeDelay);
}

PUBLIC void DELAY_vStop(
		uint8_t					*u8TimerIndex
)
{
	if(*u8TimerIndex >= NUMBER_DELAY_TIMER_SUPPORT)
		return;

	if(ZTIMER_eGetState(sTimerDelay[*u8TimerIndex].u8TimerID) == E_ZTIMER_STATE_RUNNING)
	{
		ZTIMER_eStop(sTimerDelay[*u8TimerIndex].u8TimerID);
	}
	sTimerDelay[*u8TimerIndex].bRunning = FALSE;

	*u8TimerIndex = NUMBER_DELAY_TIMER_SUPPORT;


}

PUBLIC void DELAY_vWaitHere(
		uint16_t				u16TimeDelay		//ms
)
{

}

PUBLIC void DELAY_vUseHWtimerStart(
		uint8_t					*pu8HWTimerIndex,
		uint16_t				u16TimeDelay_us,		//us
		fnCbAfterDelay			fnCallback,
		void	               	*pvParams
)
{
	uint8_t i;
	for(i = 0; i < NUMBER_OF_HW_TIMER_DELAY; i++)
	{
		if(sHWTimerDelay[i].bRunning == FALSE)
		{
			DBG_vPrintf(TRACE_DELAY,
						"DELAY: Start Delay using hardware timer %d in %d us \n",
						u8ArrHWTimerDelay[i], u16TimeDelay_us);
			*pu8HWTimerIndex = i;
			sHWTimerDelay[i].fnCallback 	= fnCallback;
			sHWTimerDelay[i].pvParams 		= pvParams;
			sHWTimerDelay[i].bRunning 		= TRUE;
			HW_TIMER_vStartSingleShot_us(u8ArrHWTimerDelay[i], u16TimeDelay_us, 0);
//			HW_TIMER_vStartSingleShot(u8ArrHWTimerDelay[i], u16TimeDelay_us, 0);
			break;
		}
	}
}

PUBLIC void DELAY_vUseHWtimerStop(
		uint8_t					*u8HWTimerIndex
)
{
	if(*u8HWTimerIndex > NUMBER_OF_HW_TIMER_DELAY)
	{
		DBG_vPrintf(TRACE_DELAY,
				"DELAY: Error Stop delay using hardware timer u8HWTimerIndex[%d] >= NUMBER_OF_HW_TIMER_DELAY[%d] \n",
				*u8HWTimerIndex, NUMBER_OF_HW_TIMER_DELAY);
		return;
	}

	DBG_vPrintf(TRACE_DELAY, "DELAY: Stop delay using hardware timer \n");
	sHWTimerDelay[*u8HWTimerIndex].bRunning 		= FALSE;
	HW_TIMER_vStop(u8ArrHWTimerDelay[*u8HWTimerIndex]);

	*u8HWTimerIndex = NUMBER_OF_HW_TIMER_DELAY;

}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vCallbackDelayTimer(void *pvParam)
{
	if (NULL == pvParam) return;
	uint8_t u8TimerIndex = *((uint8_t*)pvParam);
	DBG_vPrintf(TRACE_DELAY, "DELAY: Delay Timer callback: timerIndex [%d] \n", u8TimerIndex);

	sTimerDelay[u8TimerIndex].bRunning = FALSE;

	if(sTimerDelay[u8TimerIndex].fnCallback != NULL)
	{
		sTimerDelay[u8TimerIndex].fnCallback(sTimerDelay[u8TimerIndex].pParams);
	}
}

PRIVATE void vCallbackDelayHWTimer(HW_TIMER_tsTimerEvent_t sEvent)
{
	if(sEvent.pvParams == NULL)
		return;

	uint8_t u8HWTimerIndex = *(uint8_t*)sEvent.pvParams;
	sHWTimerDelay[u8HWTimerIndex].bRunning 		= FALSE;
	if((sEvent.u8Timer == u8ArrHWTimerDelay[u8HWTimerIndex]) && (sEvent.eIntType == E_TIMER_INT_RISING_EDGE))
	{
		DBG_vPrintf(TRACE_DELAY, "DELAY: HW Timer delay callback \n");
		if(sHWTimerDelay[u8HWTimerIndex].fnCallback != NULL)
		{
			sHWTimerDelay[u8HWTimerIndex].fnCallback(sHWTimerDelay[u8HWTimerIndex].pvParams);
		}
	}
	else
	{
		DBG_vPrintf(TRACE_DELAY,
				"DELAY: HW delay timer Interrupt Noise edge %d, timer %d \n",
				sEvent.eIntType, sEvent.u8Timer);
	}
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
