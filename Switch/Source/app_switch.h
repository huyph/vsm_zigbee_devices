/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_reporting.c
 *
 * DESCRIPTION:        Base Device application - reporting functionality
 *
 ***************************************************************************/
#ifndef APP_SWITCH_H_
#define APP_SWITCH_H_

#include "on_off_light.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef enum
{
	APP_E_SWITCH_1 = 0,
	APP_E_SWITCH_2,
	APP_E_SWITCH_3,
	APP_E_SWITCH_4,

	MAX_NUMBER_OF_SWITCH
}APP_eSwitch;

enum
{
	SWITCH_1_GANG = 1,
	SWITCH_2_GANG = 2,
	SWITCH_3_GANG = 3,
	SWITCH_4_GANG = 4,
};
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_SWITCH_vInitialize(void);

PUBLIC void APP_SWITCH_vZCLDeviceSpecificInit(void);
PUBLIC teZCL_Status APP_SWITCH_eZCLRegisterEndpoint(tfpZCL_ZCLCallBackFunction fptr);
PUBLIC bool	APP_SWITCH_vIsApplicationEndpoint(uint8_t u8Endpoint);
PUBLIC uint8_t APP_SWITCH_u8GetEndpointIndex(uint8_t u8Endpoint);

PUBLIC bool APP_SWITCH_bGetSwitchOnOffStatus(APP_eSwitch eSwitch);
PUBLIC void APP_SWITCH_bSetSwitchOnOffStatus(APP_eSwitch eSwitch, bool bStatus);
PUBLIC bool APP_SWITCH_bSendSwitchStatus(APP_eSwitch eSwitch);

PUBLIC void APP_SWITCH_vSaveSwitchStatus(void);
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/
extern PUBLIC uint8_t u8NumberOfSwitch;
extern PUBLIC uint8_t u8ListEndpoint[MAX_NUMBER_OF_SWITCH];
extern PUBLIC tsZLO_OnOffLightDevice sOnOffLightDevice[MAX_NUMBER_OF_SWITCH];
extern PUBLIC APP_eSwitch	eArrSwitch[MAX_NUMBER_OF_SWITCH];
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /* APP_SWITCH_H_ */
