/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_reporting.c
 *
 * DESCRIPTION:        Base Device application - reporting functionality
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5168, JN5179].
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2016. All rights reserved
 *
 ***************************************************************************/


/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include <zcl.h>
#include <dbg.h>
#include <zcl_customcommand.h>
#include "app_reporting.h"
#include "Basic.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_APP_REPORT
    #define TRACE_APP_REPORT         (FALSE)
#else   /* DEBUG_APP_COMMON_REPORT */
    #define TRACE_APP_REPORT         (TRUE)
#endif  /* DEBUG_APP_REPORT */

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC teZCL_Status APP_REPORT_eReportAttribute(uint8_t u8Endpoint, uint16_t u16ClusterID, uint16_t u16Attribute)
{
    tsZCL_Address sDesAddress;
    teZCL_Status eStatus = E_ZCL_SUCCESS;

    PDUM_thAPduInstance myPDUM_thAPduInstance = hZCL_AllocateAPduInstance();
    sDesAddress.eAddressMode = E_ZCL_AM_SHORT;
    sDesAddress.uAddress.u16DestinationAddress = 0; // Coordinator

    if (PDUM_INVALID_HANDLE == myPDUM_thAPduInstance)
    {
        DBG_vPrintf(TRACE_APP_REPORT,"APP-REPORT: PDUM_INVALID_HANDLE \n");
    }

    if (E_ZCL_SUCCESS != (eStatus = eZCL_ReportAttribute(&sDesAddress,
                    u16ClusterID,
                    u16Attribute,
                    u8Endpoint,
                    1,  // Coordinator Endpoint
                    myPDUM_thAPduInstance)))
    {
        DBG_vPrintf(TRACE_APP_REPORT, "APP-REPORT: EP: %d, Cluster ID: 0x%04x Attribute: 0x%04x Error %d \n",
        		u8Endpoint, u16ClusterID, u16Attribute, eStatus);
    }
    else
    {
    	DBG_vPrintf(TRACE_APP_REPORT, "APP-REPORT: Sent report \n");
    }
    /* Free Buffer And Return */
    PDUM_eAPduFreeAPduInstance(myPDUM_thAPduInstance);

    return eStatus;
}

PUBLIC teZCL_Status APP_REPORT_eReportAllAttributes(uint8_t u8Endpoint, uint16_t u16ClusterID)
{
	 tsZCL_Address sDesAddress;
	 teZCL_Status eStatus = E_ZCL_SUCCESS;

	 PDUM_thAPduInstance myPDUM_thAPduInstance = hZCL_AllocateAPduInstance();
	 sDesAddress.eAddressMode = E_ZCL_AM_SHORT;
	 sDesAddress.uAddress.u16DestinationAddress = 0; // Coordinator

	 if (PDUM_INVALID_HANDLE == myPDUM_thAPduInstance)
	 {
		 DBG_vPrintf(TRACE_APP_REPORT,"APP-REPORT: PDUM_INVALID_HANDLE \n");
	 }

	 if (E_ZCL_SUCCESS != (eStatus = eZCL_ReportAllAttributes(&sDesAddress,
			 u16ClusterID,
			 u8Endpoint,
			 1,  // Coordinator Endpoint
			 myPDUM_thAPduInstance)))
	 {
		 DBG_vPrintf(TRACE_APP_REPORT, "APP-REPORT:Report all attributes error EP: %d, Cluster ID: 0x%04x : Error %d \n",
				 u8Endpoint, u16ClusterID, eStatus);
	 }
	 else
	 {
		 DBG_vPrintf(TRACE_APP_REPORT, "APP-REPORT: Sent report \n");
	 }

	 /* Free Buffer And Return */
	 PDUM_eAPduFreeAPduInstance(myPDUM_thAPduInstance);

	 return eStatus;
}

PUBLIC bool_t APP_REPORT_bReportDeviceInfor(void)
{
//	if(E_ZCL_SUCCESS != APP_REPORT_eReportAllAttributes(1, GENERAL_CLUSTER_ID_BASIC))
//		return FALSE;
	if(E_ZCL_SUCCESS  != APP_REPORT_eReportAttribute(1,GENERAL_CLUSTER_ID_BASIC,E_CLD_BAS_ATTR_ID_MODEL_IDENTIFIER))
		return FALSE;

	return TRUE;
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
