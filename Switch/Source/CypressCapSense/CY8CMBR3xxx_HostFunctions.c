/*****************************************************************************
* File Name: CY8CMBR3xxx_HostFunctions.c
*
* Version 1.00
*
* Description:
*   This file contains the definitions of the low-level APIs. You may need to 
*   modify the content of these APIs to suit your host processor�s I2C 
*   implementation.
*
* Note:
*   These host-dependent Low Level APIs are provided as an example of
*   low level I2C read and write functions. This set of low level APIs are 
*   written for PSoC 4200/4100 devices and hence should be re-written
*   with equivalent host-dependent APIs from the respective IDEs, if the 
*   host design does not include PSoC 4200/4100 device.
* 
*   To use these APIs, the host should implement a working I2C communication
*   interface. This interface will be used by these APIs to communicate to the
*   CY8CMBR3xxx device.
*
*   For PSoC 4200/4100 devices, please ensure that you have created an instance 
*   of SCB component with I2C Master configuration. The component should be
*   named "SCB".
*
* Owner:
*   SRVS
*
* Related Document:
*   MBR3 Design Guide
*   MBR3 Device Datasheet
*
* Hardware Dependency:
*   PSoC 4200 (Update this as per the host used)
*
* Code Tested With:
*   PSoC Creator 3.0 CP7
*   CY3280-MBR3 Evaluation Kit
*   CY8CKIT-042 Pioneer Kit
*
******************************************************************************
* Copyright (2014), Cypress Semiconductor Corporation.
******************************************************************************
* This software is owned by Cypress Semiconductor Corporation (Cypress) and is
* protected by and subject to worldwide patent protection (United States and
* foreign), United States copyright laws and international treaty provisions.
* Cypress hereby grants to licensee a personal, non-exclusive, non-transferable
* license to copy, use, modify, create derivative works of, and compile the
* Cypress Source Code and derivative works for the sole purpose of creating
* custom software in support of licensee product to be used only in conjunction
* with a Cypress integrated circuit as specified in the applicable agreement.
* Any reproduction, modification, translation, compilation, or representation of
* this software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH
* REGARD TO THIS MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes without further notice to the
* materials described herein. Cypress does not assume any liability arising out
* of the application or use of any product or circuit described herein. Cypress
* does not authorize its products for use as critical components in life-support
* systems where a malfunction or failure may reasonably be expected to result in
* significant injury to the user. The inclusion of Cypress' product in a life-
* support systems application implies that the manufacturer assumes all risk of
* such use and in doing so indemnifies Cypress against all charges. Use may be
* limited by and subject to the applicable Cypress software license agreement.
*****************************************************************************/
/**
 * Author: Thinh Nguyen (thinhnt13), Modify : HuyTV5
 * Modified: 2019/03/28
 * Project: Vinsmart Smarthome
 * Description: Modify to use with JN5169 (host) & CY8CMR3
 */
/*******************************************************************************
* Included headers
*******************************************************************************/
#include "CY8CMBR3xxx_HostFunctions.h"
#include "CY8CMBR3xxx_APIs.h"
#include "CY8CMBR3xxx_Device.h"

#ifdef DEBUG_CY8CMBR3
    #define TRACE_CY8CMBR3              (TRUE)
#else   /* DEBUG_CY8CMBR3 */
    #define TRACE_CY8CMBR3              (FALSE)
#endif  /* DEBUG_CY8CMBR3 */
/*******************************************************************************
* API Constants
*******************************************************************************/
#define CY8CMBR3xxx_READ                    (1)
#define CY8CMBR3xxx_WRITE                   (0)
#define CY8CMBR3xxx_ACK                     (0)
#define CY8CMBR3xxx_NACK                    (1)
#define CY8CMBR3xxx_READ_BYTE_ERROR         (0x80000000)
#define MBR3_BOOT_DELAY                 	(100)

const unsigned char CY8CMBR3116_configuration[128] =
{
#ifdef TEST_CAP_SENCE_ON_KIT
	    0x78u, 0x00u, 0x00u, 0x00u, 0x78u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x7Fu, 0x7Fu, 0x7Fu, 0x80u,
	    0x80u, 0x80u, 0x80u, 0x7Fu, 0x7Fu, 0x7Fu, 0x7Fu, 0x7Fu,
	    0x7Fu, 0x7Fu, 0x7Fu, 0x7Fu, 0x03u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x80u,
	    0x05u, 0x00u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x01u, 0x01u,
	    0x08u, 0x00u, 0x00u, 0x00u, 0x0Fu, 0x0Fu, 0x0Fu, 0x0Fu,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x03u, 0x01u, 0x50u,
	    0x00u, 0x37u, 0x02u, 0x00u, 0x00u, 0x0Au, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x99u, 0x26u
#else
	    0x33u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x46u, 0x46u, 0x7Fu, 0x7Fu,
	    0x46u, 0x46u, 0x7Fu, 0x7Fu, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x03u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x80u,
	    0x05u, 0x00u, 0x00u, 0x02u, 0x00u, 0x02u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x1Eu, 0x1Eu, 0x00u,
	    0x00u, 0x1Eu, 0x1Eu, 0x00u, 0x00u, 0x00u, 0x01u, 0x01u,
	    0x00u, 0xFFu, 0xFFu, 0xFFu, 0xFFu, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x01u, 0x60u,
	    0x00u, 0x37u, 0x01u, 0x00u, 0x00u, 0x0Au, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
	    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0xE3u, 0x6Du
#endif /*TEST_CAP_SENCE_ON_KIT*/
};

/* The following macro defines the maximum number of times the low-level read
 * and write functions try to communicate with the CY8CMBR3xxx device, as
 * long as the I2C communication is unsuccessful.
 */
#define CY8CMBR3xxx_RETRY_TIMES             (2)



void vStopI2C(void)
{
	bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,								// Transmit a stop sequence
							E_AHI_SI_STOP_BIT,
							E_AHI_SI_NO_SLAVE_READ,
							E_AHI_SI_NO_SLAVE_WRITE,
							E_AHI_SI_SEND_ACK,
							E_AHI_SI_NO_IRQ_ACK);
	volatile uint32_t timeout = 0x0002FFFF;
	while(bAHI_SiMasterPollTransferInProgress() && timeout--);								// Wait until the transfer is complete
}

bool bCheckArbitration(void)
{
	if (bAHI_SiMasterCheckRxNack() ||  bAHI_SiMasterPollArbitrationLost())		// If a Nack is received (i.e. the slave can no longer receive data)
	{																			// or the master has lost arbitration, release and restart the bus
		bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,							// Transmit a stop sequence
								E_AHI_SI_STOP_BIT,
								E_AHI_SI_NO_SLAVE_READ,
								E_AHI_SI_NO_SLAVE_WRITE,
								E_AHI_SI_SEND_NACK,
								E_AHI_SI_NO_IRQ_ACK);
		bAHI_SiMasterSetCmdReg(	E_AHI_SI_START_BIT,								// Transmit a start sequence
								E_AHI_SI_NO_STOP_BIT,
								E_AHI_SI_NO_SLAVE_READ,
								E_AHI_SI_NO_SLAVE_WRITE,
								E_AHI_SI_SEND_NACK,
								E_AHI_SI_NO_IRQ_ACK);
		return 0;
	}
	return 1;
}

static bool bWriteSingleByteWithoutStopFlag(uint8_t u8Data)
{
	volatile uint32_t timeout = 0x0002FFFF;

	while(1)
	{
		vAHI_SiMasterWriteData8(u8Data);										// Send character data to the I2C LCD driver
		bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,								// Transmit a write sequence
								E_AHI_SI_NO_STOP_BIT,
								E_AHI_SI_NO_SLAVE_READ,
								E_AHI_SI_SLAVE_WRITE,
								E_AHI_SI_SEND_ACK,
								E_AHI_SI_NO_IRQ_ACK);
		while(bAHI_SiMasterPollTransferInProgress() && timeout--);								// Wait until the transfer is complete
		if( !bCheckArbitration() || !timeout )
		{
			return 0;
		}
		else goto exit;
	}

	exit:
	if(timeout == 0) return 0;
	return 1;

}

static bool bWriteLastSingleByteWithStopFlag(uint8_t u8Data)
{
	volatile uint32_t timeout = 0x0002FFFF;

	while(1)
	{
		vAHI_SiMasterWriteData8(u8Data);
		// Send character data to the I2C LCD driver
		bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,								// Transmit a write sequence
								E_AHI_SI_STOP_BIT,
								E_AHI_SI_NO_SLAVE_READ,
								E_AHI_SI_SLAVE_WRITE,
								E_AHI_SI_SEND_ACK,
								E_AHI_SI_NO_IRQ_ACK);
		while(bAHI_SiMasterPollTransferInProgress() && timeout--);								// Wait until the transfer is complete

		if( !bCheckArbitration() || !timeout )
		{
			DBG_vPrintf(TRACE_CY8CMBR3,"bCheckArbitration error\r\n");
			return 0;
		}
		else goto exit;
	}

	exit:
	if(timeout == 0) return 0;
	return 1;
}


static bool bReadLastSingleByteWithStopFlag(uint8_t * u8Data)
{
	volatile uint32_t timeout = 0x0002FFFF;

	while(1)
	{
		// Send character data to the I2C LCD driver
		bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,								// Transmit a write sequence
								E_AHI_SI_STOP_BIT,
								E_AHI_SI_SLAVE_READ,
								E_AHI_SI_NO_SLAVE_WRITE,
								E_AHI_SI_SEND_NACK,
								E_AHI_SI_NO_IRQ_ACK);


		while(bAHI_SiMasterPollTransferInProgress() && timeout--);								// Wait until the transfer is complete
		if( !bCheckArbitration() || !timeout )
		{
			vStopI2C();
			return 0;
		}
		else goto exit;
	}

	exit:
	*u8Data = u8AHI_SiMasterReadData8();
	vStopI2C();
	if(timeout == 0) return 0;
	return 1;
}


static bool bReadSingleByteWithoutStopFlag(uint8_t *u8Data)
{
	 volatile uint32_t count  = 0x0000FFFF;
	  vAHI_SiMasterSetCmdReg(E_AHI_SI_NO_START_BIT,
	  							  E_AHI_SI_NO_STOP_BIT,
	  							  E_AHI_SI_SLAVE_READ,
	  							  E_AHI_SI_NO_SLAVE_WRITE,
	  							  E_AHI_SI_SEND_ACK,
	  							  E_AHI_SI_NO_IRQ_ACK);
	  while(bAHI_SiMasterPollTransferInProgress() && count--); // wait while busy

	  if( bAHI_SiMasterPollTransferInProgress() ) return 0;
	  *u8Data = u8AHI_SiMasterReadData8();
	  return 1;
}


bool bStartWriteI2C(uint8_t slaveAddress)
{
	uint8_t retry = 4;
	while(retry--)
	{
		volatile uint32_t timeout = 0x0002FFFF;
		vAHI_SiMasterWriteSlaveAddr(slaveAddress , FALSE);
		bAHI_SiMasterSetCmdReg(	E_AHI_SI_START_BIT,									// Transmit a start & write sequence
								E_AHI_SI_NO_STOP_BIT,
								E_AHI_SI_NO_SLAVE_READ,
								E_AHI_SI_SLAVE_WRITE,
								E_AHI_SI_SEND_NACK,
								E_AHI_SI_NO_IRQ_ACK);
		while(bAHI_SiMasterPollTransferInProgress() && timeout--);								// Wait until the transfer is complete
		if( bCheckArbitration() && timeout) return 1;
		volatile uint32_t i = 0x0000FFFF;
		while(i--);
	}
	return 0;
}


bool bStartReadI2C(uint8_t slaveAddress)
{
	uint8_t retry = 4;

	while(retry--)
	{
		volatile uint32_t i = 0x0000FFFF;
		vAHI_SiMasterWriteSlaveAddr(slaveAddress , TRUE);									// Set the slave address to and the read bit to false (i.e. write)
		while( !bAHI_SiMasterSetCmdReg(TRUE, FALSE, FALSE, TRUE, FALSE, FALSE) && i--);
		if(i != 0)
		{
			volatile uint32_t timeout = 0x0002FFFF;
			while(bAHI_SiMasterPollTransferInProgress() && timeout--);								// Wait until the transfer is complete
			if( bCheckArbitration() && timeout) return 1;
			else
			{
				DBG_vPrintf(TRACE_CY8CMBR3,"bCheckArbitration error\r\n");
			}
		}
	}
	return 0;
}

static bool WritePacket ( uint8_t slaveAddress, uint8_t numberOfBytes, uint8_t * writeBuffer )
{

//	DBG_vPrintf(TRACE_CY8CMBR3,"Write packet size %d\r\n", numberOfBytes);

	uint8 retryCount = CY8CMBR3xxx_RETRY_TIMES;
	uint8_t byteCount = 0;

	// Start I2C
	if( !bStartWriteI2C(slaveAddress) )
	{
		DBG_vPrintf(TRACE_CY8CMBR3,"bStartWriteI2C error\r\n");
		 return 0;
	}

	while(retryCount--)
	{
		for(byteCount = 0; byteCount < numberOfBytes-1; byteCount++)
		{
			if (!bWriteSingleByteWithoutStopFlag( writeBuffer[byteCount] ) )
			{
				byteCount = 0;
				DBG_vPrintf(TRACE_CY8CMBR3, "bWriteSingleByteWithoutStopFlag error\n");
				goto next_last_byte;
			}
		}
		next_last_byte:
		if( (byteCount == (numberOfBytes-1)) )
		{
			if( bWriteLastSingleByteWithStopFlag(writeBuffer[byteCount]) )
			{
				byteCount++;
				goto quit;
			}
			else byteCount = 0;
		}
	}
	vStopI2C();
	quit:
	if(byteCount == numberOfBytes) return 1;
	return 0;
}


static bool ReadPacket (uint8_t slaveAddress, uint8_t  numberOfBytes, uint8_t * readBuffer)
{
//	DBG_vPrintf(TRACE_CY8CMBR3,"Address %d, ReadPacket size %d\r\n", slaveAddress, numberOfBytes);
	uint8 retryCount = CY8CMBR3xxx_RETRY_TIMES;
	uint8_t byteCount = 0;
	if( !bStartReadI2C(slaveAddress) ) return 0;
	while(retryCount--)
	{
		for(byteCount = 0; byteCount < numberOfBytes - 1; byteCount++)
		{
			if( !bReadSingleByteWithoutStopFlag( &readBuffer[byteCount] ) )
			{
				byteCount = 0;
				volatile uint32_t i = 0x0001FFFF;
				while(i--);
				goto next_last_byte;
			}
		}
		next_last_byte:
		if( (byteCount == (numberOfBytes-1)) )
		{
			if( bReadLastSingleByteWithStopFlag(&readBuffer[byteCount]) )
			{
				byteCount++;
				goto quit;
			}
			else
			{
					byteCount = 0;
					vStopI2C();
			}
		}
	}
	vStopI2C();
	quit:
	if(byteCount == numberOfBytes) return 1;
	return 0;
}

 
/*******************************************************************************
*   Function Code
*******************************************************************************/

/*******************************************************************************
* Function Name: Host_LowLevelWrite
********************************************************************************
*
* Summary:
*  This API writes to the register map of the CY8CMBR3xxx device using the I2C 
*  communication protocol. The implementation is host processor dependent and 
*  you may need to update the API code to suit your host.
*
* Parameters:
*  uint8 slaveAddress:
*   The I2C address of the CY8CMBR3xxx device. Valid range: 8 - 119
*
*  uint8 *writeBuffer:
*   The buffer from which data is written to the device. 
*
*   The first element should always contain the location of the register 
*   of the device to write to. This value can be within 0 – 251.
*
*   Each successive element should contain the data to be written to that 
*   register and the successive registers. These elements can have a value 
*   between 0  255. The number of data bytes can be between 0 and 128.
*
*  uint8 numberOfBytes:
*   Number of bytes to be written, equal to the number of elements in the 
*   buffer (i.e. number of data bytes + 1)
*
* Pre:
*  The I2C interface should be enabled and working before this Low Level 
*  API can be called. Also make sure that the Global Interrupts are also
*  enabled (if required)
*
* Post:
*  N/A
*
* Return:
*  status
*    Value                Description
*    TRUE                 Write process was successful
*    FALSE                Write process was not successful
*
*******************************************************************************/
bool Host_LowLevelWrite(uint8 slaveAddress, uint8 *writeBuffer, uint8 numberOfBytes)
{
	if( !WritePacket (slaveAddress, numberOfBytes, writeBuffer ) )
	{
		DBG_vPrintf(TRACE_CY8CMBR3, "Host_LowLevelWrite error\n");
		return 0;
	}
	return 1;
}

/*******************************************************************************
* Function Name: Host_LowLevelRead
********************************************************************************
*
* Summary:
*  This API reads from the register map of the CY8CMBR3xxx device using the 
*  I2C communication protocol. The implementation is host processor dependent 
*  and you may need to update the API code to suit your host.
*
* Parameters:
*  uint8 slaveAddress:
*   The I2C address of the CY8CMBR3xxx device. Valid range: 8 - 119
*
*  uint8 *readBuffer:
*   The buffer to be updated with the data read from the device.
*
*   The register location to read from should be set prior to calling 
*   this API. Each successive element to contain the data read from that 
*   register and the successive registers. These elements can have a value 
*   between 0 – 255.
*
*  uint8 numberOfBytes:
*   Number of data bytes to be read, equal to the number of elements in 
*   the buffer. Valid range: 1 – 252
*
* Pre:
*  The I2C interface should be enabled and working before this Low Level 
*  API can be called. Also make sure that the Global Interrupts are also
*  enabled (if required)
*
* Post:
*  N/A
*
* Return:
*  status
*    Value                Description
*    TRUE                 Read process was successful
*    FALSE                Read process was not successful
*
*******************************************************************************/
bool Host_LowLevelRead(uint8 slaveAddress, uint8 *readBuffer, uint8 numberOfBytes)
{
	if( !ReadPacket(slaveAddress, numberOfBytes, readBuffer) )
	{
		DBG_vPrintf(TRACE_CY8CMBR3, "Host_LowLevelRead error\n");
		return 0;
	}
	return 1;
}

/*******************************************************************************
* Function Name: Host_LowLevelDelayMs
********************************************************************************
*
* Summary:
*  This API implements a time-delay function to be used by the High-level APIs. 
*  The delay period is in milliseconds. This delay is achieved by a 
*  code-execution block for the required amount of time.
*
*  The implementation is host processor dependent and you need to update the 
*  API code to suit your host.
*
* Parameters:
*  uint16 milliseconds:
*   The amount of time in milliseconds for which a wait is required. 
*   Valid range: 0 65535
*
* Return:
*  None
*
*******************************************************************************/
void Host_LowLevelDelayMs(uint32 milliseconds)
{
    // Call the host-specific delay implementation
    // Replace this with the correct host delay routine for introducing delays in milliseconds
    //CyDelay((uint32) milliseconds);
	Host_Delayus(70*milliseconds);
}
/*******************************************************************************
* Function Name: Host_Delayus
********************************************************************************
*
* Summary:
*  This API implements Simple Delay function
*
* Parameters:
*
* Return:
*  None
*
*******************************************************************************/
void Host_Delayus(uint32 udelayus)
{
    volatile uint32 u32Delay = 0;
    volatile uint8_t i = 0;
    for(u32Delay=0; u32Delay< udelayus; u32Delay++)
    	for(i = 0; i < 1; i++);
}
/*******************************************************************************
* Function Name: Host_Init
********************************************************************************
*
* Summary:
*  This API implements initialization of I2C Port
*
* Parameters:
*
* Return:
*  None
*
*******************************************************************************/
bool bHost_Init(bool inited_before)
{
	DBG_vPrintf(TRACE_CY8CMBR3, "Host init baud = JN5169_I2C_PRESCALE_100KHZ, init before = %d \n", inited_before);
	vAHI_SiMasterConfigure(FALSE, FALSE, JN5169_I2C_PRESCALE_50KHZ);
	vAHI_SiSetLocation(TRUE);
	if( !inited_before )
	{
		Host_LowLevelDelayMs(MBR3_BOOT_DELAY);
		if (TRUE == CY8CMBR3xxx_Configure(SLAVE_ADDRESS, CY8CMBR3116_configuration))
		{
			DBG_vPrintf(TRUE, "Host configured successfully\r\n");
			return TRUE;
		}
		else
		{
			DBG_vPrintf(TRUE, "Host configuration failed\r\n");
			return FALSE;
		}
	}
	return TRUE;
}


/****************************End of File***************************************/
