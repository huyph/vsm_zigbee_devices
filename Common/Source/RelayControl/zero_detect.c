/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          zero_detect.c
 *
 * DESCRIPTION:        Detect zero crossing on AC power
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "zero_detect.h"
#include "AppHardwareApi_JN516x.h"
#include "dbg.h"
#include "ZTimer.h"
#include "hw_isr.h"
#include "my_define.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifdef DEBUG_ZERO_DETECT
    #define TRACE_ZERO_DETECT          (TRUE)
#else   /* DEBUG_ZERO_DETECT */
    #define TRACE_ZERO_DETECT          (FALSE)
#endif  /* DEBUG_ZERO_DETECT */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vCallbackZeroDetectTimeout(void* pvParams);
PRIVATE void ZERO_DETECT_vCallbackWhenDetected(void* pvParams);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE uint8_t s_u8ZeroDetectIsr;
PRIVATE fnCbWhenDetected	s_fnCallback;
PRIVATE void	        	*s_pParams;
PRIVATE uint8_t				u8TimerTimeout;
PRIVATE bool_t				bZeroDetectRunning = FALSE;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void ZERO_DETECT_vInitialize(void)
{
	s_fnCallback 	= NULL;
	s_pParams		= NULL;
	/* Set Zero detect pin as input */
	vAHI_DioSetDirection(1UL << PIN_IN_ZERO_DETECT, 0);

    /* Set the edge detection for rising edges */
    vAHI_DioInterruptEdge(1UL << PIN_IN_ZERO_DETECT, 0);

    // Register callback of interrupt
    HW_ISR_vRegisterCallback(
    		&s_u8ZeroDetectIsr,
    		1UL << PIN_IN_ZERO_DETECT,
    		ZERO_DETECT_vCallbackWhenDetected,
    		NULL
    );

    ZERO_DETECT_vDisable();

    // Open timer timeout zero detect
	(void) ZTIMER_eOpen(&u8TimerTimeout,
							vCallbackZeroDetectTimeout,
							NULL,
							ZTIMER_FLAG_PREVENT_SLEEP);
}

PUBLIC void ZERO_DETECT_vEnable(
		fnCbWhenDetected	fnCallback,
		void	        	*pParams
)
{
	DBG_vPrintf(TRACE_ZERO_DETECT, "ZERO_DETECT: Enable Zero detect timeout = %d \n", ZERO_DETECT_TIME_OUT);

	s_fnCallback 	= fnCallback;
	s_pParams		= pParams;

	/* Set the edge detection for rising edges */
	vAHI_DioInterruptEdge(1UL << PIN_IN_ZERO_DETECT, 0);
	/* Enable ZERO interrupt */
	vAHI_DioInterruptEnable(1UL << PIN_IN_ZERO_DETECT, 0);

	/* Start timer timeout */
	if(ZERO_DETECT_TIME_OUT > 0)
	{
		ZTIMER_eStart(u8TimerTimeout, ZERO_DETECT_TIME_OUT);
	}

	HW_ISR_vEnable(s_u8ZeroDetectIsr);
	bZeroDetectRunning = TRUE;
}

PUBLIC void ZERO_DETECT_vDisable(void)
{
	DBG_vPrintf(TRACE_ZERO_DETECT, "ZERO_DETECT: Disable \n");
	ZTIMER_eStop(u8TimerTimeout);
	/* Disable input interrupt */
    vAHI_DioInterruptEnable(0, 1UL << PIN_IN_ZERO_DETECT);

    HW_ISR_vDisable(s_u8ZeroDetectIsr);

    bZeroDetectRunning = FALSE;
}

PUBLIC bool_t ZERO_DETECT_bIsRunning(void)
{
	return bZeroDetectRunning;
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void ZERO_DETECT_vCallbackWhenDetected(void* pvParams)
{
	(void)u32AHI_DioInterruptStatus();
	if(bZeroDetectRunning)
	{
		DBG_vPrintf(TRACE_ZERO_DETECT, "ZERO_DETECT: ZERO_DETECT_vCallbackWhenDetected \n");
		ZTIMER_eStop(u8TimerTimeout);
		bZeroDetectRunning = FALSE;

		ZERO_DETECT_vDisable();

		if(s_fnCallback != NULL)
		{
			s_fnCallback(s_pParams);
		}
	}
	else
	{
		DBG_vPrintf(TRACE_ZERO_DETECT, "ZERO_DETECT: ZERO_DETECT_vCallbackWhenDetected noise??? \n");
	}
}

PRIVATE void vCallbackZeroDetectTimeout(void* pvParams)
{
	DBG_vPrintf(TRACE_ZERO_DETECT, "ZERO_DETECT: vCallbackZeroDetectTimeout \n");
	bZeroDetectRunning = FALSE;

	ZTIMER_eStop(u8TimerTimeout);

	ZERO_DETECT_vDisable();

	if(s_fnCallback != NULL)
	{
		s_fnCallback(s_pParams);
	}
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/

