ĐIỀU KHIỂN RELAY:
- Đang bật rơ le (đang zero detect/đang delay to turn on relay):
	+ Có lệnh bật rơ le khác thì sẽ không thực hiện Zero detect hay delay to turn on relay nữa. Mà sẽ bật cùng rơ le hiện tại.
	+ Có lệnh tắt rơ le: Nếu chỉ có rơ le này đang thực hiện bật thì disable Zero detect và Stop timer delay to turn on relay.
		Sau đó mới thực hiện tắt rơ le.
		

HARDWARE TIMER
- Các bước để cấu hình rơ le:
	vAHI_Timer0RegisterCallback();
	vAHI_TimerEnable(E_AHI_TIMER_0, 10, FALSE, FALSE, TRUE);
	vAHI_TimerConfigureOutputs(E_AHI_TIMER_0, FALSE, TRUE);
	vAHI_TimerSetLocation(E_AHI_TIMER_0, TRUE, FALSE);
 
	vAHI_TimerStartRepeat(E_AHI_TIMER_0, 15625, 31250);
- Nếu dùng lênh Start Single shot với u16Hi = 0, u16Lo = 4s > 0 thì
		Rise Int		Periode Int				Result Rise Int		Result Period Int		Note
		TRUE			TRUE					OK-0				OK-4s 					Rise trước Falling	
		TRUE			FALSE					OK-0				NO
		FALSE			TRUE					OK-4s				NO
		FALSE			FALSE					NO					NO
- Nếu dùng lênh Start Single shot với u16Hi = 4s > 0, u16Lo = 0 thì
		Rise Int		Periode Int				Result Rise Int		Result Period Int		Note
		TRUE			TRUE					OK-4s				OK-4s 					Rise trước Falling
		TRUE			FALSE					OK-4s				NO						=> SỬ DỤNG ĐỂ TẠO HÀM TRỄ
		FALSE			TRUE					OK-4s				NO
		FALSE			FALSE					NO					NO