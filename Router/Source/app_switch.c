/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_reporting.c
 *
 * DESCRIPTION:        Base Device application - reporting functionality
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "app_switch.h"
#include "zps_gen.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
PUBLIC uint8_t u8SwitchType = APP_E_SWITCH_1_GANG;
PUBLIC uint8_t u8ListEndpoint[MAX_NUMBER_SWITCH];
PUBLIC tsZLO_OnOffLightDevice sOnOffLightDevice[MAX_NUMBER_SWITCH];
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void APP_SWITCH_vZCLDeviceSpecificInit(void)
{
	if(u8SwitchType < 0 || u8SwitchType >= MAX_NUMBER_SWITCH)
	{
		return;
	}
	uint8 i;
	for(i = 0; i <= u8SwitchType; i++)
	{
		sOnOffLightDevice[i].sOnOffServerCluster.bOnOff = FALSE;
		memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ManufacturerName, "NXP", CLD_BAS_MANUF_NAME_SIZE);
		memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8ModelIdentifier, "BDB-Router", CLD_BAS_MODEL_ID_SIZE);
		memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8DateCode, "20150212", CLD_BAS_DATE_SIZE);
		memcpy(sOnOffLightDevice[i].sBasicServerCluster.au8SWBuildID, "1000-0001", CLD_BAS_SW_BUILD_SIZE);
	}
}

PUBLIC teZCL_Status APP_SWITCH_eZCLRegisterEndpoint(tfpZCL_ZCLCallBackFunction fptr)
{
	teZCL_Status eZCL_Status;
	if(u8SwitchType < 0 || u8SwitchType >= MAX_NUMBER_SWITCH)
	{
		return E_ZCL_ERR_ENUM_END;
	}
	uint8 i;
	for(i = 0; i <= u8SwitchType; i++)
	{
		  eZCL_Status =  eZLO_RegisterOnOffLightEndPoint(ROUTER_APPLICATION_ENDPOINT,
		                                                      fptr,
		                                                      &sOnOffLightDevice[i]);
		  if(eZCL_Status != E_ZCL_SUCCESS)
			  return eZCL_Status;
	}

	return E_ZCL_SUCCESS;
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
