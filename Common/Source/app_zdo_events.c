/*
 * app_zdo_events.c
 *
 *  Created on: Apr 5, 2019
 *      Author: TrungTQ9
 */

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include <dbg.h>
#include <bdb_api.h>
#include "PDM_IDs.h"
#include "app_nwk_handler.h"
#include "app_zdo_events.h"
#include "app_node.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_APP_ZDO_EVENTS
    #define TRACE_APP_ZDO_TASK              (FALSE)
#else   /* DEBUG_APP_ZDO_EVENTS */
    #define TRACE_APP_ZDO_TASK              (TRUE)
#endif  /* DEBUG_APP_ZDO_EVENTS */

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/*
 * @brief   Application handler for stack events for end point 0 (ZDO)
 */
PUBLIC void APP_ZdoEvents_vHandle(
    BDB_tsZpsAfEvent *psZpsAfEvent
) {
    ZPS_tsAfEvent *psAfEvent = &(psZpsAfEvent->sStackEvent);

    switch (psAfEvent->eType) {
    case ZPS_EVENT_NONE:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: None\n");
        break;

    case ZPS_EVENT_APS_DATA_INDICATION:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Data Indication Status 0x%02x from 0x%04x SrcEp %d "
                    "DstEp %d Profile 0x%04x Cluster 0x%04x\n",
                    psAfEvent->uEvent.sApsDataIndEvent.eStatus,
                    psAfEvent->uEvent.sApsDataIndEvent.uSrcAddress.u16Addr,
                    psAfEvent->uEvent.sApsDataIndEvent.u8SrcEndpoint,
                    psAfEvent->uEvent.sApsDataIndEvent.u8DstEndpoint,
                    psAfEvent->uEvent.sApsDataIndEvent.u16ProfileId,
                    psAfEvent->uEvent.sApsDataIndEvent.u16ClusterId);
        break;

    case ZPS_EVENT_APS_DATA_CONFIRM:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Data Confirm\n");
        break;

    case ZPS_EVENT_APS_DATA_ACK:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Data ACK\n");
        break;

    case ZPS_EVENT_NWK_STARTED:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Network started\n");
        break;

    case ZPS_EVENT_NWK_JOINED_AS_ROUTER:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Joined as Router\n");
        break;

    case ZPS_EVENT_NWK_JOINED_AS_ENDDEVICE:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Joined Network Addr 0x%04x Rejoin %d\n",
                    psAfEvent->uEvent.sNwkJoinedEvent.u16Addr,
                    psAfEvent->uEvent.sNwkJoinedEvent.bRejoin);
        APP_NwkHandler_vJoinedAsEndDevice();
        break;

    case ZPS_EVENT_NWK_FAILED_TO_START:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Failed To start\n");
        break;

    case ZPS_EVENT_NWK_FAILED_TO_JOIN:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Failed To Join 0x%02x Rejoin %d\n",
                    psAfEvent->uEvent.sNwkJoinFailedEvent.u8Status,
                    psAfEvent->uEvent.sNwkJoinFailedEvent.bRejoin);

        if (0 != ZPS_psAplAibGetAib()->u64ApsUseExtendedPanid) {
            DBG_vPrintf(TRACE_APP_ZDO_TASK,
                        "APP-ZDO: Restore epid %016llx\n",
                        ZPS_psAplAibGetAib()->u64ApsUseExtendedPanid);
            ZPS_vNwkNibSetExtPanId(ZPS_pvAplZdoGetNwkHandle(),
                                   ZPS_psAplAibGetAib()->u64ApsUseExtendedPanid);
        }
        break;

    case ZPS_EVENT_NWK_NEW_NODE_HAS_JOINED:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: New Node Has Joined\n");
        break;

    case ZPS_EVENT_NWK_DISCOVERY_COMPLETE:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Discovery Complete 0x%02x\n",
                    psAfEvent->uEvent.sNwkDiscoveryEvent.eStatus);
//        APP_NwkHandler_vPrintAPSTable();
        break;

    case ZPS_EVENT_NWK_LEAVE_INDICATION:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Leave Indication %016llx Rejoin %d\n",
                    psAfEvent->uEvent.sNwkLeaveIndicationEvent.u64ExtAddr,
                    psAfEvent->uEvent.sNwkLeaveIndicationEvent.u8Rejoin);
        APP_NwkHandler_vNetworkLeaveIndication(psAfEvent);
        break;

    case ZPS_EVENT_NWK_LEAVE_CONFIRM:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Leave Confirm status 0x%02x Addr %016llx\n",
                    psAfEvent->uEvent.sNwkLeaveConfirmEvent.eStatus,
                    psAfEvent->uEvent.sNwkLeaveConfirmEvent.u64ExtAddr);
        APP_NwkHandler_vNetworkLeaveConfirm(psAfEvent);
        break;

    case ZPS_EVENT_NWK_STATUS_INDICATION:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Network status Indication 0x%02x addr %04x\n",
                    psAfEvent->uEvent.sNwkStatusIndicationEvent.u8Status,
                    psAfEvent->uEvent.sNwkStatusIndicationEvent.u16NwkAddr);
        break;

    case ZPS_EVENT_NWK_ROUTE_DISCOVERY_CONFIRM:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Network Route Discovery Confirm 0x%02x\n",
                    psAfEvent->uEvent.sNwkRouteDiscoveryConfirmEvent.u8Status);
        if (ZPS_NWK_ENUM_ROUTE_DISCOVERY_FAILED == psAfEvent->uEvent.sNwkRouteDiscoveryConfirmEvent.u8Status)
        {
             bBdbJoinFailed = TRUE;
        }
        break;

    case ZPS_EVENT_NWK_POLL_CONFIRM:
    	DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP_ZDO: ZPS_EVENT_NWK_POLL_CONFIRM \n");
        APP_NwkHandler_vPollResponse(psAfEvent);
        break;

    case ZPS_EVENT_NWK_ED_SCAN:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Network Energy Detect (ED) Scan 0x%02x\n",
                    psAfEvent->uEvent.sNwkEdScanConfirmEvent.u8Status);
        break;

    case ZPS_EVENT_ZDO_BIND:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Zdo Bind\n");
        break;

    case ZPS_EVENT_ZDO_UNBIND:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Zdo Unbind\n");
        break;

    case ZPS_EVENT_ZDO_LINK_KEY:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Zdo Link Key Type %d Addr %016llx\n",
                    psAfEvent->uEvent.sZdoLinkKeyEvent.u8KeyType,
                    psAfEvent->uEvent.sZdoLinkKeyEvent.u64IeeeLinkAddr);
        break;

    case ZPS_EVENT_BIND_REQUEST_SERVER:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Bind Request Server\n");
        break;

    case ZPS_EVENT_ERROR:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Error %d\n",
                    psAfEvent->uEvent.sAfErrorEvent.eError);
        break;

    case ZPS_EVENT_APS_INTERPAN_DATA_INDICATION:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Interpan Data Indication\n");
        break;

    case ZPS_EVENT_APS_INTERPAN_DATA_CONFIRM:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Interpan Data Confirm\n");
        break;

    case ZPS_EVENT_TC_STATUS:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Trust Center Status %02x\n",
                    psAfEvent->uEvent.sApsTcEvent.u8Status);
        break;

    case ZPS_EVENT_NWK_DUTYCYCLE_INDICATION:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Duty Cycle Indication\n");
        break;

    case ZPS_EVENT_NWK_FAILED_TO_SELECT_AUX_CHANNEL:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Failed To Select Aux Channel\n");
        break;

    case ZPS_EVENT_NWK_ROUTE_RECORD_INDICATION:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Route Record Indication\n");
        break;

    case ZPS_EVENT_NWK_FC_OVERFLOW_INDICATION:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: FC Overflow Indication\n");
        break;

    case ZPS_ZCP_EVENT_FAILURE:
        DBG_vPrintf(TRACE_APP_ZDO_TASK, "APP-ZDO: Failed\n");
        break;

    default:
        DBG_vPrintf(TRACE_APP_ZDO_TASK,
                    "APP-ZDO: Unhandled Event %d\n",
                    psAfEvent->eType);
        break;
    }
}
