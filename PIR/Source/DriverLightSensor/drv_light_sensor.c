
/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_reporting.c
 *
 * DESCRIPTION:        Base Device application - reporting functionality
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "drv_light_sensor.h"
#include "AppHardwareApi_JN516x.h"
#include "dbg.h"
#include "my_define.h"
#include "ZTimer.h"
#include "stdlib.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_DRV_LIGHT_SENSOR
    #define TRACE_DRV_LIGHT_SENSOR           (FALSE)
#else   /* DEBUG_DRV_LIGHT_SENSOR */
    #define TRACE_DRV_LIGHT_SENSOR            (TRUE)
#endif  /* DEBUG_DRV_LIGHT_SENSOR */
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef struct
{
	uint16_t    		u16LightLevel;
	DRV_LIGHT_SENSOR_teLightLevel_t	eLightLevelState;
	uint16_t    		u16LightLevelOfLatestReport;
	drvLightSensorCb 	s_fnCallback;
	uint8_t       		u8TimerUpdateLightLevel;
	uint16_t       		u16TimeFromLatestReport;
}DRV_LIGHT_SENSOR_tsParams;
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vCallbackTimerUpdateLightLevel(void *pvParam);
PRIVATE uint16_t   u16ReadLightLevel(void);
PRIVATE bool    bIsReadyToReport(void);
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE DRV_LIGHT_SENSOR_tsParams 		sSensorParams;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void	DRV_LIGHT_SENSOR_vInitialise(
		drvLightSensorCb fptr
)
{
    // Initialise parameters
	sSensorParams.u16LightLevel					= 0;
	sSensorParams.eLightLevelState				= E_LIGHT_LEVEL_BRIGHT;
	sSensorParams.u16LightLevelOfLatestReport 	= 0;
    sSensorParams.s_fnCallback					= fptr;
    sSensorParams.u16TimeFromLatestReport		= 0;

    // Setting ADC
    if (!bAHI_APRegulatorEnabled())
    {
        vAHI_ApConfigure(E_AHI_AP_REGULATOR_ENABLE,
                E_AHI_AP_INT_DISABLE,       // Disable interrupt when conversion
                E_AHI_AP_SAMPLE_2,
                E_AHI_AP_CLOCKDIV_500KHZ, // clock
                E_AHI_AP_EXTREF);          // Vref = Internal = 1.235VDC (Refer JN5169 Datasheet page 67)

        while(!bAHI_APRegulatorEnabled());
    }

    vAHI_AdcEnable( E_AHI_ADC_SINGLE_SHOT,  // Perform a single conversion and then stop
            E_AHI_AP_INPUT_RANGE_1, // range 0-Vref = 0-> 2.47VDC
            ADC_LIGHT_SENSOR);   // ADC1 hay PIN15

    sSensorParams.u16LightLevel =  u16ReadLightLevel();
    if(sSensorParams.u16LightLevel > HIGH_THRESHOLD_LIGHT_LEVEL)
    {
    	sSensorParams.eLightLevelState = E_LIGHT_LEVEL_DARK;
    }
    else
    {
    	sSensorParams.eLightLevelState = E_LIGHT_LEVEL_BRIGHT;
    }

    // Start timer UPDATE LIGHT SENSOR
    (void) ZTIMER_eOpen(&sSensorParams.u8TimerUpdateLightLevel,
    		vCallbackTimerUpdateLightLevel,
			NULL,
			ZTIMER_FLAG_PREVENT_SLEEP);
    ZTIMER_eStart(sSensorParams.u8TimerUpdateLightLevel, TIME_UPDATE_LIGHT_LEVEL);
}

PUBLIC DRV_LIGHT_SENSOR_teLightLevel_t DRV_LIGHT_SENSOR_eGetState(void)
{
	return sSensorParams.eLightLevelState;
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
PRIVATE void vCallbackTimerUpdateLightLevel(void *pvParam)
{
    // stop timer
	ZTIMER_eStop(sSensorParams.u8TimerUpdateLightLevel);

    // Read light level
    sSensorParams.u16LightLevel =  u16ReadLightLevel();

    // Increase duration from last report
	if (((LIGHT_SENSOR_MAXIMUM_REPORTING_INTERVAL < 0xffff)
			&& (sSensorParams.u16TimeFromLatestReport< LIGHT_SENSOR_MAXIMUM_REPORTING_INTERVAL))
		|| ((LIGHT_SENSOR_MAXIMUM_REPORTING_INTERVAL < 0xffff)
			&& (sSensorParams.u16TimeFromLatestReport < LIGHT_SENSOR_MINIMUM_REPORTING_INTERVAL)))
    {
    	sSensorParams.u16TimeFromLatestReport += TIME_UPDATE_LIGHT_LEVEL;
    }

	// Check light level state
	if((sSensorParams.eLightLevelState == E_LIGHT_LEVEL_BRIGHT)
			&& (sSensorParams.u16LightLevel > HIGH_THRESHOLD_LIGHT_LEVEL))
	{
		sSensorParams.eLightLevelState = E_LIGHT_LEVEL_DARK;
		DBG_vPrintf(TRACE_DRV_LIGHT_SENSOR, "DRV_LIGHT_SENSOR: State BRIGHT => DARK \n");
	}
	else if((sSensorParams.eLightLevelState == E_LIGHT_LEVEL_DARK)
			&& (sSensorParams.u16LightLevel < LOW_THRESHOLD_LIGHT_LEVEL))
	{
		sSensorParams.eLightLevelState = E_LIGHT_LEVEL_BRIGHT;
		DBG_vPrintf(TRACE_DRV_LIGHT_SENSOR, "DRV_LIGHT_SENSOR: State DARK => BRIGHT \n");
	}

    // Report light level
    if (bIsReadyToReport())
    {
    	sSensorParams.u16LightLevelOfLatestReport = sSensorParams.u16LightLevel;
        if (NULL != sSensorParams.s_fnCallback)
        {
        	sSensorParams.s_fnCallback(sSensorParams.u16LightLevel);
        }
    }
    // Restart timer UPDATE LIGHT SENSOR
    ZTIMER_eStart(sSensorParams.u8TimerUpdateLightLevel, TIME_UPDATE_LIGHT_LEVEL);

    //DBG_vPrintf(TRACE_DRV_LIGHT_SENSOR, "DRV_LIGHT_SENSOR: CallbackTimerUpdateLightLevel: %d \n", u16LightLevel);
}

PRIVATE uint16_t u16ReadLightLevel(void)
{
    uint16 u16AdcReading;
    uint16 u16LightLevelRet;  // unit: LUX

    // Read ADC
    vAHI_AdcStartSample();
    while(bAHI_AdcPoll());             // Poll for read completion
    u16AdcReading = u16AHI_AdcRead();

    // convert to ADC reading value to Light Level  (LUX)
    float Vref = 3.3f; //V
    float R = 33.0f;   //kOhm
    uint16 u16ADCVoltage = (uint16)(Vref*1000*u16AdcReading/1024);  // mV
    u16LightLevelRet = (uint16)(R*u16AdcReading/(1024 - u16AdcReading)); //kOhm
	DBG_vPrintf(TRACE_DRV_LIGHT_SENSOR,
			"DRV_LIGHT_SENSOR: ADCReading = %d ADCVoltage = %dmV Light Resistor = %d(kOhm) \n",
			u16AdcReading, u16ADCVoltage, u16LightLevelRet);

//    return u16LightLevelRet;
	return u16AdcReading;
}

PRIVATE bool bIsReadyToReport(void)
{
    bool bRet = FALSE;
    if(((sSensorParams.u16TimeFromLatestReport >=  LIGHT_SENSOR_MINIMUM_REPORTING_INTERVAL)
    			&& (abs(sSensorParams.u16LightLevel - sSensorParams.u16LightLevelOfLatestReport) > LIGHT_SENSOR_MINIMUM_REPORTABLE_CHANGE))
    		||(sSensorParams.u16TimeFromLatestReport >=  LIGHT_SENSOR_MAXIMUM_REPORTING_INTERVAL))
    {
        bRet = TRUE;
        sSensorParams.u16TimeFromLatestReport = 0;
    }
    return bRet;
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/

