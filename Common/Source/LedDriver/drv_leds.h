/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_leds.h
 *
 * DESCRIPTION:		   API for control led
 *
 ***************************************************************************/

#ifndef DRV_LEDS_H_
#define DRV_LEDS_H_
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "jendefs.h"
#include "hw_timer.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#define MAX_NUMBER_GROUP_LED_BLINK	(5)

#ifndef DRV_LEDS_ZTIMER_STORAGE
#define DRV_LEDS_ZTIMER_STORAGE     (MAX_NUMBER_GROUP_LED_BLINK)
#endif /* DRV_LEDS_ZTIMER_STORAGE */

#define LED_BLINK_FOREVER			(-1)   // if u16NumberBlink <= -1 then led will blink forever

#define LED_LEVEL_0					(0)		//0%
#define LED_LEVEL_100				(100)	//100%
#define LED_LEVEL_MIN				(5)		//5% Min led level ensure Device running well
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef void (*fnCbAfterBlink)(void*);
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC bool_t DRV_LEDS_bInitialize(
		uint32_t				u32LedPinMask,
		uint8					u8LedLevel
);

PUBLIC void DRV_LEDS_vBlinkLedsAlternately(
		uint32_t				u32LedPinMask,
		uint32_t				u32LedPinMask2,
		int16_t 				i16NumberBlink,
		int16_t 				i16BlinkDuration,		//ms
		uint16_t 				u16TimeOn,				//ms
		uint16_t 				u16TimeOff,				//ms
		fnCbAfterBlink			fnCallback,
		void	               	*pParams
);

PUBLIC void DRV_LEDS_vStopBlinkLeds(
		bool_t					bStopBlinkAllLeds,
		uint32_t				u32LedPinMask
);

PUBLIC void DRV_LEDS_vSetLevel(
		uint8					u8LedLevel
);

PUBLIC void DRV_LEDS_vOn(
		uint32_t				u32LedPinMask
);

PUBLIC void DRV_LEDS_vOff(
		uint32_t				u32LedPinMask
);
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
#endif /* DRV_LEDS_H_ */
