/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_light_sensor.h
 *
 * DESCRIPTION:
 *
 ***************************************************************************/
#ifndef DRV_LIGHT_SENSOR_H_
#define DRV_LIGHT_SENSOR_H_
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "jendefs.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DRV_LIGHT_SENSOR_ZTIMER_STORAGE
	#define DRV_LIGHT_SENSOR_ZTIMER_STORAGE     (1)
#endif /* DRV_LIGHT_SENSOR_ZTIMER_STORAGE */

#define LIGHT_SENSOR_MINIMUM_REPORTABLE_CHANGE  3       //lux
#define LIGHT_LEVEL_THRESHOLD                   100     //lux
#define TIME_UPDATE_LIGHT_LEVEL                 2000    //ms

#define LOW_THRESHOLD_LIGHT_LEVEL				(50)
#define HIGH_THRESHOLD_LIGHT_LEVEL				(200)

#define LIGHT_SENSOR_MINIMUM_REPORTING_INTERVAL				(10) //s
#define LIGHT_SENSOR_MAXIMUM_REPORTING_INTERVAL				(20) //s	if this value equal 0xffff => disable report frequently

typedef enum
{
	E_LIGHT_LEVEL_DARK = 0,
	E_LIGHT_LEVEL_BRIGHT
}DRV_LIGHT_SENSOR_teLightLevel_t;
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef void (*drvLightSensorCb)(uint16_t);
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void	DRV_LIGHT_SENSOR_vInitialise(
		drvLightSensorCb fptr
);

PUBLIC DRV_LIGHT_SENSOR_teLightLevel_t DRV_LIGHT_SENSOR_eGetState(void);
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
#endif /* DRV_LIGHT_SENSOR_H_ */
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/



