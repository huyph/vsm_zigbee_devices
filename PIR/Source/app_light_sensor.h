/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_light_sensor.c
 *
 * DESCRIPTION:
 *
 ***************************************************************************/
#ifndef APP_LIGHT_SENSOR_H_
#define APP_LIGHT_SENSOR_H_
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "jendefs.h"
#include "light_sensor.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#define LIGHT_SENSOR_MINIMUM_MEASURED_VALUE                            0x0000
#define LIGHT_SENSOR_MAXIMUM_MEASURED_VALUE                            0x03E8 //1000lux
//#define LIGHT_SENSOR_MINIMUM_REPORTABLE_CHANGE                         0x01
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_LIGHT_SENSOR_vInitialize(void);
PUBLIC teZCL_Status APP_LIGHT_SENSOR_eZCLRegisterEndpoint(
    tfpZCL_ZCLCallBackFunction fptr
);
PUBLIC void APP_LIGHT_SENSOR_vZCLDeviceSpecificInit(void);
PUBLIC void APP_LIGHT_SENSOR_vReportState(void);

PUBLIC bool_t APP_LIGHT_SENSOR_bIsDarkState(void);
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/
extern PUBLIC tsZLO_LightSensorDevice s_LightSensor;
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
#endif /*APP_LIGHT_SENSOR_H_*/
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
