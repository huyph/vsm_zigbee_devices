/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          hw_timer.h
 *
 * DESCRIPTION:        Manage hardware and software timers
 *
 ***************************************************************************/

#ifndef HW_TIMER_H_
#define HW_TIMER_H_
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "jendefs.h"
#include "AppHardwareApi_JN516x.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef enum
{
	E_TIMER_INT_RISING_EDGE = 0,
	E_TIMER_INT_FALLING_EDGE,

	E_TIMER_INT_NUMBER_INTERRUPT
}HW_TIMER_teTimerInt_t;

typedef struct
{
	uint8_t							u8Timer;
	HW_TIMER_teTimerInt_t 			eIntType;
	void*							pvParams;
}HW_TIMER_tsTimerEvent_t;

typedef void (*fbCallbackTimerInt)(HW_TIMER_tsTimerEvent_t sEvent);

typedef enum
{
	E_HW_TIMER_0 = 0,
	E_HW_TIMER_1,
	E_HW_TIMER_2,
	E_HW_TIMER_3,
	E_HW_TIMER_4,
	NUMBER_HW_TIMER
}HW_TIMER_teTimerNumber_t;
typedef enum
{
	E_HW_TIMER_PERIOD_RANGE_1US_TO_65MS = 0,	//prescale = 4  -> f = 1Mhz  -> T = 1us
	E_HW_TIMER_PERIOD_RANGE_64US_TO_4S,			//prescale = 10  -> f = 15625hz  -> T = 64us
	E_HW_TIMER_PERIOD_RANGE_1MS_TO_65S,			//prescale = 14 -> f = 976Hz -> T ~ 1ms
	NUMBER_HW_TIMER_CYCLE
}HW_TIMER_teTimerCycle_t;

typedef enum
{
	E_TIMER_OUTPUT_NONE = 0,

	E_TIMER_0_DEFAULT_OUTPUT_DIO_4,
	E_TIMER_0_DEFAULT_OUTPUT_DIO_10,

	E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_5_6_7_8,
	E_TIMER_1_2_3_4_DEFAULT_OUTPUT_DIO_11_12_13_17,

	E_TIMER_2_3_DEFAULT_OUTPUT_DO_0_1,

	E_TIMER_OUTPUT_BY_USER,

}HW_TIMER_teOutput_t;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void HW_TIMER_vInitialize(void);

PUBLIC void HW_TIMER_vISRTimer0(uint32 u32Device, uint32 u32ItemBitmap);
PUBLIC void HW_TIMER_vISRTimer1(uint32 u32Device, uint32 u32ItemBitmap);
PUBLIC void HW_TIMER_vISRTimer2(uint32 u32Device, uint32 u32ItemBitmap);
PUBLIC void HW_TIMER_vISRTimer3(uint32 u32Device, uint32 u32ItemBitmap);
PUBLIC void HW_TIMER_vISRTimer4(uint32 u32Device, uint32 u32ItemBitmap);
PUBLIC bool_t HW_TIMER_bEnable(
		HW_TIMER_teTimerNumber_t	eHWTimer,
		bool_t						bAllowOtherAppUseThisTimer,
		HW_TIMER_teTimerCycle_t		eTimerCycle,
		bool_t 						bIntRiseEnable,
		bool_t 						bIntPeriodEnable,
		fbCallbackTimerInt			fnCallback,
		void*						pvParams,

		HW_TIMER_teOutput_t			eTimerOutput,
		uint32_t					u32UserOutputMask
);
PUBLIC void HW_TIMER_vStartRepeat(
		HW_TIMER_teTimerNumber_t	eHWTimer,
		uint16 						u16Hi_ms,
		uint16 						u16Lo_ms
);
PUBLIC void HW_TIMER_vStartSingleShot(
		HW_TIMER_teTimerNumber_t	eHWTimer,
		uint16 						u16Hi_ms,
		uint16 						u16Lo_ms
);
PUBLIC void HW_TIMER_vStop(
		HW_TIMER_teTimerNumber_t	eHWTimer
);
PUBLIC void HW_TIMER_vDisable(
		HW_TIMER_teTimerNumber_t	eHWTimer
);


PUBLIC void HW_TIMER_vStartSingleShot_us(
		HW_TIMER_teTimerNumber_t	eHWTimer,
		uint16 						u16Hi_us,
		uint16 						u16Lo_us
);
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /* HW_TIMER_H_ */
