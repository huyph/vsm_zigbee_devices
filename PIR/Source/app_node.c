/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_node.c
 *
 * DESCRIPTION:        Base Device Demo: End Device Application
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5168, JN5179].
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2017. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include <jendefs.h>
#include <string.h>
#include "dbg.h"
#include "pdum_apl.h"
#include "pdum_nwk.h"
#include "pdum_gen.h"
#include "pwrm.h"
#include "PDM.h"
#include "zps_gen.h"
#include "zps_apl.h"
#include "zps_apl_af.h"
#include "zps_apl_zdo.h"
#include "zps_apl_zdp.h"
#include "zps_apl_aib.h"
#include "zps_apl_aps.h"
#include "bdb_api.h"
#include "app_common.h"
#include "app_main.h"
#include "app_buttons.h"
#include "ZTimer.h"
#include "app_events.h"
#include <rnd_pub.h>

#include "app_zcl_task.h"
#include "app_node.h"
#include "zps_nwk_nib.h"
#include "PDM_IDs.h"
#include "zcl_options.h"
#include "zcl.h"
#include "hw_timer.h"
#include "wd_reset.h"
#include "delay.h"
#include "app_reporting.h"
#include "app_occupancy_sensor.h"
#include "app_light_sensor.h"
#include "app_led.h"
#include "app_relay.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef DEBUG_APP
    #define TRACE_APP   FALSE
#else
    #define TRACE_APP   TRUE
#endif

#ifdef DEBUG_APP_EVENT
    #define TRACE_APP_EVENT   TRUE
#else
    #define TRACE_APP_EVENT   FALSE
#endif

#ifdef DEBUG_POLL_SLEEP
    #define TRACE_POLL_SLEEP   TRUE
#else
    #define TRACE_POLL_SLEEP   FALSE
#endif

#define FIND_AND_BIND_IME (182)
#define DEEP_SLEEPTIME (10)
#define SLEEP_DURATION_MS (1000)
#define SLEEP_TIMER_TICKS_PER_MS (32)

#define NEVER_DEEP_SLEEP   FALSE
#define ZCL_TICK_TIME           ZTIMER_TIME_MSEC(100)

#define NUMBER_RETRY_STEERING_NWK	(3)
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void APP_Node_vLeaveAndReset(void *pvParams);
PRIVATE void vWakeCallBack(void);
PRIVATE void APP_vBdbInit(void);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
PUBLIC bool_t bDeepSleep;
PUBLIC teNodeState eNodeState;
PUBLIC bool_t bBdbJoinFailed = FALSE;
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
PRIVATE uint8 u8DeepSleepTime = DEEP_SLEEPTIME;
PRIVATE uint8_t	u8NumberRetrySteeringNwk = 0;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

#ifdef PDM_EEPROM
    extern uint8 u8PDM_CalculateFileSystemCapacity();
    extern uint8 u8PDM_GetFileSystemOccupancy();
#endif



/****************************************************************************
 *
 * NAME: APP_vInitialiseNode
 *
 * DESCRIPTION:
 * Initialises the application related functions
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vInitialiseNode(void)
{
    uint16 u16ByteRead;

    DBG_vPrintf(TRACE_APP,"APP_NODE:  HW_TIMER_vInitialize() \n");
    HW_TIMER_vInitialize();

    DBG_vPrintf(TRACE_APP,"APP_NODE:  WD_RESET_vInitialize() \n");
    WD_RESET_vInitialize();

    DBG_vPrintf(TRACE_APP,"APP_NODE:  DELAY_vInitialize() \n");
    DELAY_vInitialize();

    DBG_vPrintf(TRACE_APP,"APP_NODE:  APP_BUTTONS_vInitialize() \n");
    APP_BUTTONS_vInitialize();

    eNodeState = E_STARTUP;
    PDM_eReadDataFromRecord(PDM_ID_NODE_STATE,
                            &eNodeState,
                            sizeof(teNodeState),
                            &u16ByteRead);

    ZPS_u32MacSetTxBuffers  (4);

    /* Initialise ZBPro stack */
    ZPS_eAplAfInit();

    /* Initialise ZCL */
    APP_ZCL_vInitialise();
    /* Set end device age out time to 11 days 9 hours & 4 mins */
    ZPS_bAplAfSetEndDeviceTimeout(ZED_TIMEOUT_16384_MIN);
    /* Initialise other software modules
     * HERE
     */
    DBG_vPrintf(TRACE_APP,"APP_NODE:  APP_LED_vInitialize() \n");
    APP_LED_vInitialize();

    DBG_vPrintf(TRACE_APP,"APP_NODE:  APP_RELAY_vInitialize() \n");
    APP_RELAY_vInitialize();

    DBG_vPrintf(TRACE_APP,"APP_NODE:  APP_OCCUPANCY_SENSOR_vInitialize() \n");
    APP_OCCUPANCY_SENSOR_vInitialize();

//    DBG_vPrintf(TRACE_APP,"APP_NODE:  APP_LIGHT_SENSOR_vInitialize() \n");
//    APP_LIGHT_SENSOR_vInitialize();

    APP_vBdbInit();
    /* Always initialise any peripherals used by the application
     * HERE
     */

#if (defined PDM_EEPROM) && (defined TRACE_APP)
    /* The functions u8PDM_CalculateFileSystemCapacity and u8PDM_GetFileSystemOccupancy
     * may be called at any time to monitor space available in  the eeprom  */
    DBG_vPrintf(TRACE_APP, "PDM: Capacity %d\n", u8PDM_CalculateFileSystemCapacity() );
    DBG_vPrintf(TRACE_APP, "PDM: Occupancy %d\n", u8PDM_GetFileSystemOccupancy() );
#endif
}

/****************************************************************************
 *
 * NAME: APP_taskEndDevice
 *
 * DESCRIPTION:
 * Task that handles application related functions
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_taskEndDevice(void)
{
    APP_tsEvent sAppEvent;
    sAppEvent.eType = APP_E_EVENT_NONE;

    if (ZQ_bQueueReceive(&APP_msgAppEvents, &sAppEvent) == TRUE)
    {
        DBG_vPrintf(TRACE_APP_EVENT, "ZPR: App event %d, NodeState=%d \n", sAppEvent.eType, eNodeState);

        /* Network Events*/
        if(sAppEvent.eType == APP_E_EVENT_NWK_STEERING_SUCCESS)
        {
        	DBG_vPrintf(TRACE_APP_EVENT, "APP_NODE: App event: APP_E_EVENT_NWK_STEERING_SUCCESS \n");
        	APP_REPORT_bReportDeviceInfor();
        	// CTKT khong mo ta nhay led
        }
        else if(sAppEvent.eType == APP_E_EVENT_NWK_STEERING_FAILURE)
        {
        	DBG_vPrintf(TRACE_APP_EVENT, "APP_NODE: App event: APP_E_EVENT_NWK_STEERING_FAILURE \n");
        	// CTKT khong mo ta nhay led
        	if(eNodeState == E_STARTUP)
        	{
        		// Retry steering network
        		u8NumberRetrySteeringNwk ++;
        		if(u8NumberRetrySteeringNwk >= NUMBER_RETRY_STEERING_NWK)
        		{
        			u8NumberRetrySteeringNwk = 0;
        		}
        		else
        		{
        			BDB_teStatus eStatus = BDB_eNsStartNwkSteering();
        			DBG_vPrintf(TRACE_APP, "APP_NODE: Retry Steering Network time %d, status %d \n",u8NumberRetrySteeringNwk, eStatus);
        		}
        	}
        }
        else if(sAppEvent.eType == APP_E_EVENT_ZCL_APS_ACK_FAIL)
        {
			DBG_vPrintf(TRACE_APP,
					"APP_NODE: App event: APP_E_EVENT_ZCL_APS_ACK_FAIL EP: %d, clusterID: 0x%04x \n",
					sAppEvent.uEvent.sACKEvent.u8EndPoint,
					sAppEvent.uEvent.sACKEvent.u16ClusterID);
			bBdbJoinFailed = TRUE;
        }
        /* Function button event */
        if(sAppEvent.eType == APP_E_EVENT_FUNC_BUTTON_UP_SHORTER_5S)
        {
        	DBG_vPrintf(TRACE_APP_EVENT, "APP_NODE: Event APP_E_EVENT_FUNC_BUTTON_UP_SHORTER_5S \n");
        	if(eNodeState == E_RUNNING)
        	{
        		APP_REPORT_bReportDeviceInfor();
        		// Theo CTKT: den led sang 3s roi tat
        		APP_LED_vOn(3000);
        	}
        	else
        	{
            	// Theo CTKT: den led nhay trong 3s chu ky 0.2s roi tat
            	APP_LED_vStartBlink(3000, 200, NULL, NULL);
        	}
        }
        else if(sAppEvent.eType == APP_E_EVENT_FUNC_BUTTON_HOLD_5S)
        {
        	DBG_vPrintf(TRACE_APP_EVENT, "APP_NODE: Event APP_E_EVENT_FUNC_BUTTON_HOLD_5S \n");
        	// Theo CTKT: nhay trong 3s chu ky 0.2s roi tat
        	APP_LED_vStartBlink(3000, 200, APP_Node_vLeaveAndReset, NULL);
        }

        /* Occupancy sensor event */
        else if(sAppEvent.eType == APP_E_EVENT_OCCUPANCY_SENSOR_REPORT_STATUS)
        {
			DBG_vPrintf(TRACE_APP_EVENT,
					"APP_NODE: Event APP_E_EVENT_OCCUPANCY_SENSOR_REPORT_STATUS :0x%02x \n",
					s_OccupancySensor.sOccupancySensingServerCluster.u8Occupancy);
			if(eNodeState == E_RUNNING)
			{
				APP_OCCUPANCY_SENSOR_vReportState();
			}
        	APP_LED_vSet(s_OccupancySensor.sOccupancySensingServerCluster.u8Occupancy);
//        	if(s_OccupancySensor.sOccupancySensingServerCluster.u8Occupancy)
//        	{
//        		if( APP_LIGHT_SENSOR_bIsDarkState())
//        		{
//        			APP_RELAY_vSet(0, TRUE);
//        			APP_LED_vSet(TRUE);
//        		}
//        	}
//        	else
//        	{
//        		APP_RELAY_vSet(0, FALSE);
//        		APP_LED_vSet(FALSE);
//        	}
        }

        /* Light sensor event */
        else if(sAppEvent.eType == APP_E_EVENT_LIGHT_SENSOR_REPORT_STATUS)
        {
        	DBG_vPrintf(TRACE_APP_EVENT, "APP_NODE: Event APP_E_EVENT_LIGHT_SENSOR_REPORT_STATUS \n");
        	if(eNodeState == E_RUNNING)
        	{
        		APP_LIGHT_SENSOR_vReportState();
        	}
        }
    }
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

PRIVATE void APP_Node_vLeaveAndReset(void *pvParams)
{
	// Do factory reset
	if (eNodeState == E_RUNNING)
	{
		if (ZPS_eAplZdoLeaveNetwork( 0UL, FALSE, FALSE) != ZPS_E_SUCCESS )
		{
			APP_Node_vFactoryResetRecords();
			vAHI_SwReset();
		}
	}
	else
	{
		APP_Node_vFactoryResetRecords();
		vAHI_SwReset();
	}
}

/****************************************************************************
 *
 * NAME: APP_Node_vFactoryResetRecords
 *
 * DESCRIPTION:
 * Resets persisted data structures to factory new state
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_Node_vFactoryResetRecords(void)
{
    /* clear out the stack */
    ZPS_vDefaultStack();
    ZPS_eAplAibSetApsUseExtendedPanId(0);
    ZPS_vSetKeys();

    /* save everything */
    eNodeState = E_STARTUP;
    PDM_eSaveRecordData(PDM_ID_NODE_STATE, &eNodeState, sizeof(teNodeState));
    ZPS_vSaveAllZpsRecords();
}

PUBLIC void APP_Node_vRejoin(void)
{
    bBdbJoinFailed = FALSE;
    sBDB.sAttrib.bbdbNodeIsOnANetwork = TRUE;
#if (defined ZBPRO_DEVICE_TYPE_ZED)
    DBG_vPrintf(TRACE_APP, "APP_NODE: End Device Start rejoin \n");
    BDB_vStart(); // Rejoin
#elif (defined ZBPRO_DEVICE_TYPE_ZCR)
    DBG_vPrintf(TRACE_APP, "APP_NODE: u8RejoinCycles = %d", u8RejoinCycles);
    if ((0xFF == u8RejoinCycles) || (0 == u8RejoinCycles))
    {
        DBG_vPrintf(TRACE_APP, " => RejoinCycle");
        BDB_vRejoinCycle(FALSE);
    }
    DBG_vPrintf(TRACE_APP, "\n");
#endif
}
/****************************************************************************
 *
 * NAME: vWakeCallBack
 *
 * DESCRIPTION:
 * Wake up call back called upon wake up by the schedule activity event.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vWakeCallBack(void)
{
	/*Decrement the deepsleep count so that if there is no activity for
	 * DEEP_SLEEPTIME then the module is put to deep sleep.
	 * */
	if (0 < u8DeepSleepTime)
	{
#if NEVER_DEEP_SLEEP
		u8DeepSleepTime = DEEP_SLEEPTIME;
#else
		u8DeepSleepTime--;
#endif
	}
}
/****************************************************************************
 *
 * NAME: APP_vStartUpHW
 *
 * DESCRIPTION:
 * Os Task activated by the wake up event to manage sleep
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vStartUpHW(void)
{
    uint8 u8Status;
    /* Restart the keyboard scanning timer as we've come up through */
    /* warm start via the Power Manager if we get here              */

    DBG_vPrintf(TRACE_POLL_SLEEP, "\nWoken: start poll timer,");
    u8Status = ZPS_eAplZdoPoll();
    DBG_vPrintf(TRACE_POLL_SLEEP, " Wake poll %02x\n", u8Status);
    if(ZTIMER_eStart(u8TimerPoll, ZTIMER_TIME_MSEC(200)) != E_ZTIMER_OK)
    {
        DBG_vPrintf(TRACE_APP, "APP: Failed to start Poll Tick Timer\n");
    }
    if ( ZTIMER_eStart(u8TimerZCL, ZCL_TICK_TIME) != E_ZTIMER_OK )
    {
        DBG_vPrintf(TRACE_APP, "APP: Failed to start ZCL Timer\n");
    }

}

/****************************************************************************
 *
 * NAME: APP_vBdbInit
 *
 * DESCRIPTION:
 * Function to initialize BDB attributes and message queue
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void APP_vBdbInit(void)
{
	BDB_tsInitArgs sInitArgs;
	sBDB.sAttrib.bbdbNodeIsOnANetwork = ((eNodeState >= E_RUNNING) ? (TRUE) : (FALSE));
	sInitArgs.hBdbEventsMsgQ = &APP_msgBdbEvents;
	BDB_vInit(&sInitArgs);
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
