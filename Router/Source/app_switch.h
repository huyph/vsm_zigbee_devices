/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_reporting.c
 *
 * DESCRIPTION:        Base Device application - reporting functionality
 *
 ***************************************************************************/
#ifndef APP_SWITCH_H_
#define APP_SWITCH_H_

#include "on_off_light.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
enum
{
	APP_E_SWITCH_1_GANG = 0,
	APP_E_SWITCH_2_GANG,
	APP_E_SWITCH_3_GANG,
	APP_E_SWITCH_4_GANG,
	MAX_NUMBER_SWITCH,
};

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_SWITCH_vZCLDeviceSpecificInit(void);
PUBLIC teZCL_Status APP_SWITCH_eZCLRegisterEndpoint(tfpZCL_ZCLCallBackFunction fptr);
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/
extern PUBLIC uint8_t u8SwitchType;
extern PUBLIC uint8_t u8ListEndpoint[MAX_NUMBER_SWITCH];
extern PUBLIC tsZLO_OnOffLightDevice sOnOffLightDevice[MAX_NUMBER_SWITCH];

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /* APP_SWITCH_H_ */
