/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_led.h
 *
 * DESCRIPTION:        DK4 DR1175 Led interface (White Led)
 *
 ***************************************************************************/
#ifndef APP_LED_H_
#define APP_LED_H_

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "drv_leds.h"
#include "app_common.h"
#include "app_switch.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#define LED_NOT_USE			(0xff)
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef enum
{
	E_LED_RED,
	E_LED_GREEN,
	E_LED_BLUE,
	NUMBER_OF_SINGLE_LED_ON_LED_RGB
}APP_LED_teTypeLed_t;

typedef enum
{
	E_STATE_NONE,

	E_STATE_BLINK_NWK_STEERING_SUCCESS,
	E_STATE_BLINK_NWK_STEERING_FAILURE,

	E_STATE_BLINK_ACK_FAIL,

	E_STATE_BLINK_CHECK_NWK_CONECTION_GOOD,
	E_STATE_BLINK_CHECK_NWK_CONECTION_ERROR,

	E_STATE_BLINK_FACTORY_RESET,

	E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_ON_NWK,
	E_STATE_BLINK_SWITCH_CHANGE_STATUS_WHEN_DEVICE_NOT_ON_NWK,
}APP_LED_teLedState_t;

typedef struct
{
	APP_LED_teLedState_t	eLedState;
	uint8_t 				u8RGBLedNumber;
}APP_LED_tsLedEvent_t;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void APP_LED_vInitialize(void);

PUBLIC void APP_LED_vSetLevel(uint8_t u8LedLevel);

PUBLIC void APP_LED_vStart(
	APP_LED_tsLedEvent_t sLedEvent
);

PUBLIC void APP_LED_vStartEvent(
	APP_LED_teLedState_t  eLedState
);

PUBLIC void APP_LED_vSet(
	uint8_t					u8RGBLedNumber,
	APP_LED_teTypeLed_t		eTypeLed,
	bool_t					bOnOff,
	int16_t					i16TimeOn
);

PUBLIC void APP_LED_vUpdateLedStatusOfSwitch(
	APP_eSwitch 	eSwitch
);

PUBLIC void APP_LED_vUpdateLedStatusOfAllSwitch(void *pvParams);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
#endif /* APP_LED_H_ */
