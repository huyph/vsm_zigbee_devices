/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          drv_buttons.h
 *
 * DESCRIPTION:        Button events
 *
 ***************************************************************************/

#ifndef DRV_BUTTONS_H_
#define DRV_BUTTONS_H_
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "jendefs.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#if (defined TEST_SCENE_SWITCH) | (defined TEST_NORMAL_SWITCH)
	#define NUMBER_OF_BUTTONS				(5)
#else
	#define NUMBER_OF_BUTTONS				(1)
#endif

#ifndef DRV_BUTTONS_ZTIMER_STORAGE
#define DRV_BUTTONS_ZTIMER_STORAGE			NUMBER_OF_BUTTONS+1
#endif
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef enum {
	DRV_E_BUTTON_EVENT_NONE = 0,
	DRV_E_BUTTON_EVENT_DOWN,
	DRV_E_BUTTON_EVENT_UP,
	DRV_E_BUTTON_EVENT_HOLD,
} DRV_BUTTONS_teEventType_t;
typedef struct
{
	DRV_BUTTONS_teEventType_t	eEventType;
	uint8_t						u8Button;
	uint8_t						u8TimeHold;
}DRV_BUTTONS_tsEvent_t;
typedef void (*drvButtonCb)(DRV_BUTTONS_tsEvent_t sEvent);
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC bool_t DRV_BUTTONS_bInitialize(
		drvButtonCb		fptr
);
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /* DRV_BUTTONS_H_ */
