/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          delay.h
 *
 * DESCRIPTION:        This for delay
 *
 ***************************************************************************/
#ifndef DELAY_H_
#define DELAY_H_
/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include "jendefs.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#define NUMBER_DELAY_TIMER_SUPPORT	(10)

#ifndef DELAY_ZTIMER_STORAGE
#define DELAY_ZTIMER_STORAGE     (NUMBER_DELAY_TIMER_SUPPORT)
#endif /* DELAY_ZTIMER_STORAGE */

#define NUMBER_OF_HW_TIMER_DELAY	(1)
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef void (*fnCbAfterDelay)(void* pvParams);
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC void DELAY_vInitialize(void);

PUBLIC void DELAY_vStart(
		uint16_t				u16TimeDelay,		//ms
		fnCbAfterDelay			fnCallback,
		void	               	*pvParams,
		uint8_t					*u8TimerIndex
);
PUBLIC void DELAY_vStop(
		uint8_t					*u8TimerIndex
);
PUBLIC void DELAY_vWaitHere(
		uint16_t				u16TimeDelay		//ms
);

PUBLIC void DELAY_vUseHWtimerStart(
		uint8_t					*pu8HWTimerIndex,
		uint16_t				u16TimeDelay_us,		//us
		fnCbAfterDelay			fnCallback,
		void	               	*pvParams
);
PUBLIC void DELAY_vUseHWtimerStop(
		uint8_t					*u8HWTimerIndex
);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /* DELAY_H_ */

