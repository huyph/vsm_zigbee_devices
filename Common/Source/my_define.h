/*
 * my_define.h
 *
 *  Created on: Jun 19, 2019
 *      Author: huyph
 */

#ifndef MY_DEFINE_H_
#define MY_DEFINE_H_

#define DIO_0	(0)
#define DIO_1	(1)
#define DIO_2	(2)
#define DIO_3	(3)
#define DIO_4	(4)
#define DIO_5	(5)
#define DIO_6	(6)
#define DIO_7	(7)
#define DIO_8	(8)
#define DIO_9	(9)
#define DIO_10	(10)
#define DIO_11	(11)
#define DIO_12	(12)
#define DIO_13	(13)
#define DIO_14	(14)
#define DIO_15	(15)
#define DIO_16	(16)
#define DIO_17	(17)
#define DIO_18	(18)
#define DIO_19	(19)

#define DO_0	(30)
#define DO_1	(31)
/*************************PIR*******************************/
#ifdef	TEST_PIR_ON_NXP_KIT
	#define PIN_IN_OCCUPANCY_SENOR		DIO_17
	#define PIN_IN_FUNC_BUTTON			DIO_8
	#define PIN_IN_ZERO_DETECT			DIO_10

	#define PIN_OUT_COIL_SET_RELAY_SW1	DIO_0
	#define PIN_OUT_COIL_RST_RELAY_SW1	DIO_1

	#define PIN_OUT_LED_STATUS			DIO_2

#elif 	(defined TEST_PIR_ON_HUYTV_KIT)
	#define PIN_IN_OCCUPANCY_SENOR		DIO_16
	#define PIN_IN_FUNC_BUTTON			DIO_17
	#define PIN_IN_ZERO_DETECT			DIO_10

	#define PIN_OUT_COIL_SET_RELAY_SW1	DIO_0
	#define PIN_OUT_COIL_RST_RELAY_SW1	DIO_1

	#define PIN_OUT_LED_STATUS			DIO_2
/*************************SCENE SWITCH**********************/
#elif 	(defined TEST_SCENE_SWITCH_ON_NXP_KIT)

	#define PIN_IN_FUNC_BUTTON			DIO_8

	#define PIN_IN_BUTTON_TEST_1		DIO_11
	#define PIN_IN_BUTTON_TEST_2		DIO_12
	#define PIN_IN_BUTTON_TEST_3		DIO_17
	#define PIN_IN_BUTTON_TEST_4		DIO_1

	#define PIN_OUT_LED_BLUE_SW1		DIO_16
	#define PIN_OUT_LED_RED_SW1			DIO_5
	#define PIN_OUT_LED_BLUE_SW2		DIO_13
	#define PIN_OUT_LED_RED_SW2			DIO_9
	#define PIN_OUT_LED_BLUE_SW3		DIO_0
	#define PIN_OUT_LED_RED_SW3			(20)
	#define PIN_OUT_LED_BLUE_SW4		DIO_18
	#define PIN_OUT_LED_RED_SW4			DIO_19

	#define PIN_OUT_PWM_MOTOR			DIO_11

	#define PIN_OUT_WD_RST				DO_0
	#define PIN_OUT_TOUCH_POWER_RST		DO_1

#elif (defined TEST_SCENE_SWITCH_ON_HUYTV_KIT)
	#define PIN_IN_BUTTON_TEST_3	DIO_2		//= D6 on Carrier board
	#define PIN_IN_BUTTON_TEST_2	DIO_3		//= D3 on Carrier board
	#define PIN_OUT_LED_BLUE_SW1		DIO_4
	#define PIN_OUT_LED_RED_SW1			DIO_5
	#define PIN_IN_BUTTON_TEST_1	DIO_8
	#define PIN_OUT_LED_BLUE_SW2		(20)
	#define PIN_OUT_LED_RED_SW2			DIO_9
	#define PIN_IN_BUTTON_TEST_4	DIO_10	//PIN_IN_ZERO_DETECT
	#define PIN_OUT_PWM_MOTOR			DIO_11
	#define PIN_OUT_LED_BLUE_SW3		DIO_12
	#define PIN_OUT_LED_RED_SW3			DIO_13
	#define PIN_IN_FUNC_BUTTON			DIO_17
	#define PIN_OUT_WD_RST				DO_0
	#define PIN_OUT_TOUCH_POWER_RST		DO_1
	#define PIN_OUT_LED_BLUE_SW4		DIO_18
	#define PIN_OUT_LED_RED_SW4			DIO_19
/*************************NORMAL SWITCH**********************/
#elif (defined TEST_NORMAL_SWITCH_ON_NXP_KIT)
	#define PIN_IN_FUNC_BUTTON			DIO_8

	#define PIN_OUT_LED_BLUE_SW1		DIO_2
	#define PIN_OUT_LED_RED_SW1			DIO_3
	#define PIN_OUT_LED_BLUE_SW2		(20)
	#define PIN_OUT_LED_RED_SW2			DIO_9
	#define PIN_OUT_LED_BLUE_SW3		DIO_12
	#define PIN_OUT_LED_RED_SW3			DIO_13		//= D2 on Generic Expansion board

	#define PIN_OUT_COIL_RST_RELAY_SW1	DIO_0
	#define PIN_OUT_COIL_SET_RELAY_SW1	DIO_1
	#define PIN_OUT_COIL_RST_RELAY_SW2	DIO_18		//= D3 on Generic Expansion board
	#define PIN_OUT_COIL_SET_RELAY_SW2	DIO_19		//= D4 on Generic Expansion board
	#define PIN_OUT_COIL_RST_RELAY_SW3	DIO_4
	#define PIN_OUT_COIL_SET_RELAY_SW3	DIO_5

	#define UART0_TX					DIO_6		//fix
	#define UART0_RX					DIO_7		//fix

	#define PIN_IN_ZERO_DETECT			DIO_10
	#define PIN_OUT_PWM_MOTOR			DIO_11

	#define PIN_IN_SWITCH_TYPE_BIT_1	DIO_14
	#define PIN_IN_SWITCH_TYPE_BIT_0	DIO_15
	#define I2C_CLK						DIO_16		//= D1 on Generic Expansion board
	#define I2C_SDA						DIO_17

	#define PIN_OUT_WD_RST				DO_0
	#define PIN_OUT_TOUCH_POWER_RST		DO_1

	#define PIN_IN_BUTTON_TEST_1		23
	#define PIN_IN_BUTTON_TEST_2		24
	#define PIN_IN_BUTTON_TEST_3		25
	#define PIN_IN_BUTTON_TEST_4		26

	// Switch 4 Gang has some differences
	#define PIN_OUT_LED_BLUE_SW4		(27)
	#define PIN_OUT_LED_RED_SW4			(28)
	#define PIN_OUT_S0_MULTIPLEXER		DIO_0
	#define PIN_OUT_S1_MULTIPLEXER		DIO_1
	#define PIN_OUT_S2_MULTIPLEXER		DIO_2
	#define PIN_OUT_OE_MULTIPLEXER		DIO_3

#elif (defined TEST_NORMAL_SWITCH_ON_HUYTV_KIT)
	#define PIN_IN_FUNC_BUTTON			DIO_17

	#define PIN_OUT_LED_BLUE_SW1		DIO_2
	#define PIN_OUT_LED_RED_SW1			DIO_3
	#define PIN_OUT_LED_BLUE_SW2		(20)
	#define PIN_OUT_LED_RED_SW2			(27)
	#define PIN_OUT_LED_BLUE_SW3		(25)
	#define PIN_OUT_LED_RED_SW3			(26)		//= D2 on Generic Expansion board

	#define PIN_OUT_COIL_RST_RELAY_SW1	DIO_0
	#define PIN_OUT_COIL_SET_RELAY_SW1	DIO_1
	#define PIN_OUT_COIL_RST_RELAY_SW2	(21)		//= D3 on Generic Expansion board
	#define PIN_OUT_COIL_SET_RELAY_SW2	(22)		//= D4 on Generic Expansion board
	#define PIN_OUT_COIL_RST_RELAY_SW3	DIO_4
	#define PIN_OUT_COIL_SET_RELAY_SW3	DIO_5

	#define UART0_TX					DIO_6		//fix
	#define UART0_RX					DIO_7		//fix

	#define PIN_IN_ZERO_DETECT			(23)
	#define PIN_OUT_PWM_MOTOR			(24)

	#define PIN_IN_SWITCH_TYPE_BIT_1	DIO_14
	#define PIN_IN_SWITCH_TYPE_BIT_0	DIO_15
	#define I2C_CLK						DIO_16		//= D1 on Generic Expansion board
	#define I2C_SDA						DIO_17

	#define PIN_OUT_WD_RST				DO_0
	#define PIN_OUT_TOUCH_POWER_RST		DO_1

	#define PIN_IN_BUTTON_TEST_1		DIO_8
	#define PIN_IN_BUTTON_TEST_2		DIO_15
	#define PIN_IN_BUTTON_TEST_3		DIO_18
	#define PIN_IN_BUTTON_TEST_4		DIO_19

	// Switch 4 Gang has some differences
	#define PIN_OUT_LED_BLUE_SW4		(26)
	#define PIN_OUT_LED_RED_SW4			DIO_9
	#define PIN_OUT_S0_MULTIPLEXER		DIO_0
	#define PIN_OUT_S1_MULTIPLEXER		DIO_1
	#define PIN_OUT_S2_MULTIPLEXER		DIO_2
	#define PIN_OUT_OE_MULTIPLEXER		DIO_3

#endif
///////////////////////////////////////////////////////////////////////////////////
#if 	(defined PIR_DEVICE) && !(defined TEST_DEVICE)
	#define PIN_IN_OCCUPANCY_SENOR		DIO_11

	#define PIN_IN_FUNC_BUTTON			DIO_1

	#define PIN_IN_ZERO_DETECT			DIO_18

	#define PIN_OUT_COIL_SET_RELAY_SW1	DIO_19
	#define PIN_OUT_COIL_RST_RELAY_SW1	DIO_0

	#define PIN_OUT_LED_STATUS			DIO_13

	#define PIN_OUT_WD_RST				DIO_2
#elif ((defined NORMAL_SWITCH_DEVICE) || (defined SCENE_SWITCH_DEVICE)) && !(defined TEST_DEVICE)

	// common for NORMAL_SWITCH and SCENE_SWITCH
	#define PIN_OUT_LED_BLUE_SW1		DIO_4
	#define PIN_OUT_LED_RED_SW1			DIO_5
	#define PIN_OUT_LED_BLUE_SW2		DIO_8
	#define PIN_OUT_LED_RED_SW2			DIO_9
	#define PIN_OUT_LED_BLUE_SW3		DIO_12
	#define PIN_OUT_LED_RED_SW3			DIO_13		//= D2 on Generic Expansion board
	#define PIN_OUT_LED_BLUE_SW4		DIO_18
	#define PIN_OUT_LED_RED_SW4			DIO_19

	#define UART0_TX					DIO_6		//fix
	#define UART0_RX					DIO_7		//fix

	#define PIN_OUT_PWM_MOTOR			DIO_11

	#define PIN_IN_SWITCH_TYPE_BIT_1	DIO_14
	#define PIN_IN_SWITCH_TYPE_BIT_0	DIO_15

	#define I2C_CLK						DIO_16		//= D1 on Generic Expansion board
	#define I2C_SDA						DIO_17

	#define PIN_OUT_WD_RST				DO_0
	#define PIN_OUT_TOUCH_POWER_RST		DO_1

	// For NORMAL_SWITCH only
	#if (defined NORMAL_SWITCH_DEVICE)
		#define PIN_OUT_COIL_RST_RELAY_SW1	DIO_18
		#define PIN_OUT_COIL_SET_RELAY_SW1	DIO_19
		#define PIN_OUT_COIL_RST_RELAY_SW2	DIO_0		//= D3 on Generic Expansion board
		#define PIN_OUT_COIL_SET_RELAY_SW2	DIO_1		//= D4 on Generic Expansion board
		#define PIN_OUT_COIL_RST_RELAY_SW3	DIO_2		//= D6 on Carrier board
		#define PIN_OUT_COIL_SET_RELAY_SW3	DIO_3		//= D3 on Carrier board

		#define PIN_IN_ZERO_DETECT			DIO_10
		// Switch 4 Gang has some differences
		#define PIN_OUT_S0_MULTIPLEXER		DIO_0
		#define PIN_OUT_S1_MULTIPLEXER		DIO_1
		#define PIN_OUT_S2_MULTIPLEXER		DIO_2
		#define PIN_OUT_OE_MULTIPLEXER		DIO_3

	// For SCENE_SWITCH only
	#elif (defined SCENE_SWITCH_DEVICE)

	#endif
#endif

#ifdef PIR_DEVICE
	/* HARDWARE TIMER*/
//	#define NOT_USE						E_HW_TIMER_0		// We can not use this timer for ZED ????
	#define HW_TIMER_DEBOUNCE_BUTTON	E_HW_TIMER_1
	#define HW_TIMER_WD					E_HW_TIMER_2		// fix 	using DO_0	for Watchdog reset
	#define HW_TIMER_DIM_LED			E_HW_TIMER_3
	#define HW_TIMER_DELAY				E_HW_TIMER_4

	/*ADC*/
	#define ADC_LIGHT_SENSOR			E_AHI_ADC_SRC_ADC_1
	//#define NOT_USE					E_AHI_ADC_SRC_ADC_2
	//#define NOT_USE					E_AHI_ADC_SRC_ADC_3
	//#define NOT_USE					E_AHI_ADC_SRC_ADC_4
#elif (defined NORMAL_SWITCH_DEVICE) | (defined SCENE_SWITCH_DEVICE)
	/* HARDWARE TIMER*/
	#define HW_TIMER_SCAN_CAPSENSE		E_HW_TIMER_0
	#define HW_TIMER_MOTOR_CONTROL		E_HW_TIMER_1		// fix	using DIO_11 PWM motor
	#define HW_TIMER_WD					E_HW_TIMER_2		// fix 	using DO_0	for Watchdog reset
	#define HW_TIMER_DIM_LED			E_HW_TIMER_3
	#define HW_TIMER_DELAY				E_HW_TIMER_4

	/*ADC*/
	#define ADC_FUNC_BUTTON				E_AHI_ADC_SRC_ADC_1
	//#define NOT_USE					E_AHI_ADC_SRC_ADC_2
	//#define NOT_USE					E_AHI_ADC_SRC_ADC_3
	//#define NOT_USE					E_AHI_ADC_SRC_ADC_4
#endif

// define led and button for testing
#define	LED_D3_CARRIER_BOARD		DIO_3
#define	LED_D6_CARRIER_BOARD		DIO_2

#define	LED_D1_GEN_EXP_BOARD		DIO_16
#define	LED_D2_GEN_EXP_BOARD		DIO_13
#define	LED_D3_GEN_EXP_BOARD		DIO_0
#define	LED_D4_GEN_EXP_BOARD		DIO_1

#endif /* MY_DEFINE_H_ */
